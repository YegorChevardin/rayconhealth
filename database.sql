-- MySQL dump 10.13  Distrib 8.0.30, for macos12 (x86_64)
--
-- Host: 127.0.0.1    Database: rayconhealth
-- ------------------------------------------------------
-- Server version	8.0.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `appointment_types`
--

DROP TABLE IF EXISTS `appointment_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `appointment_types` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appointment_types`
--

LOCK TABLES `appointment_types` WRITE;
/*!40000 ALTER TABLE `appointment_types` DISABLE KEYS */;
INSERT INTO `appointment_types` VALUES (6,'analysis'),(4,'consultation'),(2,'medication'),(3,'operation'),(1,'procedure'),(5,'vaccination');
/*!40000 ALTER TABLE `appointment_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `appointments`
--

DROP TABLE IF EXISTS `appointments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `appointments` (
  `id` int NOT NULL AUTO_INCREMENT,
  `meeting_time` datetime NOT NULL,
  `user_doctor_id` int unsigned NOT NULL,
  `appointment_type_id` int NOT NULL,
  `hospital_card_id` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_appointments_appointment_types1_idx` (`appointment_type_id`),
  KEY `fk_appointments_hospital_cards1_idx` (`hospital_card_id`),
  KEY `fk_appointments_users_doctors1_idx` (`user_doctor_id`),
  CONSTRAINT `fk_appointments_appointment_types1` FOREIGN KEY (`appointment_type_id`) REFERENCES `appointment_types` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_appointments_hospital_cards1` FOREIGN KEY (`hospital_card_id`) REFERENCES `hospital_cards` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_appointments_users_doctors1` FOREIGN KEY (`user_doctor_id`) REFERENCES `users_doctors` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appointments`
--

LOCK TABLES `appointments` WRITE;
/*!40000 ALTER TABLE `appointments` DISABLE KEYS */;
INSERT INTO `appointments` VALUES (71,'2016-03-12 00:00:00',17,4,1);
/*!40000 ALTER TABLE `appointments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (5,'dentist'),(6,'ophthalmologist'),(1,'patient'),(2,'pediatrician'),(4,'surgeon'),(3,'traumatologist');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diagnos_statuses`
--

DROP TABLE IF EXISTS `diagnos_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `diagnos_statuses` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `status_UNIQUE` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diagnos_statuses`
--

LOCK TABLES `diagnos_statuses` WRITE;
/*!40000 ALTER TABLE `diagnos_statuses` DISABLE KEYS */;
INSERT INTO `diagnos_statuses` VALUES (2,'critical'),(1,'normal'),(3,'simple');
/*!40000 ALTER TABLE `diagnos_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diagnosis`
--

DROP TABLE IF EXISTS `diagnosis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `diagnosis` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `doctor_id` int unsigned NOT NULL,
  `hospital_card_id` int unsigned NOT NULL,
  `diagnos_status_id` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_diagnosis_hospital_cards1_idx` (`hospital_card_id`),
  KEY `fk_diagnosis_diagnos_statuses1_idx` (`diagnos_status_id`),
  KEY `fk_diagnosis_user1_idx` (`doctor_id`),
  CONSTRAINT `fk_diagnosis_diagnos_statuses1` FOREIGN KEY (`diagnos_status_id`) REFERENCES `diagnos_statuses` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_diagnosis_hospital_cards1` FOREIGN KEY (`hospital_card_id`) REFERENCES `hospital_cards` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_diagnosis_user1` FOREIGN KEY (`doctor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diagnosis`
--

LOCK TABLES `diagnosis` WRITE;
/*!40000 ALTER TABLE `diagnosis` DISABLE KEYS */;
INSERT INTO `diagnosis` VALUES (5,'Allergy',8,4,3),(6,'hipotermia',8,4,2);
/*!40000 ALTER TABLE `diagnosis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enrolls`
--

DROP TABLE IF EXISTS `enrolls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `enrolls` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `text` varchar(500) NOT NULL,
  `user_id` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_clients_users1_idx` (`user_id`),
  CONSTRAINT `fk_clients_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enrolls`
--

LOCK TABLES `enrolls` WRITE;
/*!40000 ALTER TABLE `enrolls` DISABLE KEYS */;
/*!40000 ALTER TABLE `enrolls` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hospital_cards`
--

DROP TABLE IF EXISTS `hospital_cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hospital_cards` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_hospital_cards_user1_idx` (`user_id`),
  CONSTRAINT `fk_hospital_cards_user1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hospital_cards`
--

LOCK TABLES `hospital_cards` WRITE;
/*!40000 ALTER TABLE `hospital_cards` DISABLE KEYS */;
INSERT INTO `hospital_cards` VALUES (1,'Admin\'s card','2022-09-15 12:44:14',2),(4,'Yegor\'s card','2022-09-15 12:45:26',1),(6,'Ibolit\'s card','2022-09-18 11:34:31',8),(7,'Nurse\'s card','2022-09-19 13:00:30',9),(8,'Егор\'s card','2022-09-24 16:35:04',10),(9,'Stas\'s card','2022-09-26 15:12:38',11);
/*!40000 ALTER TABLE `hospital_cards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_events`
--

DROP TABLE IF EXISTS `log_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `log_events` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `text` varchar(500) NOT NULL,
  `send_at` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_events`
--

LOCK TABLES `log_events` WRITE;
/*!40000 ALTER TABLE `log_events` DISABLE KEYS */;
INSERT INTO `log_events` VALUES (6,'egor03052004@gmail.com updated permissions of user nurse@nurse.com to role doctorcategory of ophthalmologist','2022-09-22 19:16:00'),(7,'User egor03052004@gmail.com made an enroll','2022-09-23 15:50:56'),(8,'egor03052004@gmail.com updated permissions of user nurse@nurse.com to role nursecategory of ophthalmologist','2022-09-23 17:20:10'),(9,'User egor03052004@gmail.com made an enroll','2022-09-23 17:21:50'),(10,'egor03052004@gmail.com changed his account information, new email: egor03052004@gmail.com','2022-09-24 16:19:43'),(11,'egor03052004@gmail.com changed his account information, new email: egor03052004@gmail.com','2022-09-24 16:20:28'),(12,'nurse@nurse.com changed his account information, new email: nurse@nurse.com','2022-09-24 16:26:02'),(13,'nurse@nurse.com changed his account information, new email: nurse@nurse.com','2022-09-24 16:27:07'),(14,'egor03052004@gmail.com changed his account information, new email: egor03052004@gmail.com','2022-09-24 16:29:13'),(15,'egor03052004@gmail.com changed his account information, new email: egor03052004@gmail.com','2022-09-24 16:35:11'),(16,'egor03052004@gmail.com registered new user: example@example.com','2022-09-24 16:36:54'),(17,'egor03052004@gmail.com updated permissions of user example@example.com to role doctor category of surgeon','2022-09-24 16:39:03'),(18,'egor03052004@gmail.com changed his account information, new email: egor03052004@gmail.com','2022-09-24 16:39:31'),(19,'nurse@nurse.com changed his account information, new email: nurse@nurse.com','2022-09-24 16:40:36'),(20,'nurse@nurse.com changed his account information, new email: nurse@nurse.com','2022-09-24 16:41:05'),(21,'egor03052004@gmail.com changed his account information, new email: egor03052004@gmail.com','2022-09-24 16:41:38'),(22,'egor03052004@gmail.com changed his account information, new email: egor03052004@gmail.com','2022-09-24 16:46:39'),(23,'egor03052004@gmail.com changed his account information, new email: egor03052004@gmail.com','2022-09-24 16:47:30'),(24,'User egor03052004@gmail.com made an enroll','2022-09-25 18:52:35'),(25,'egor03052004@gmail.com updated permissions of user kryzha@kryzha.com to role admin category of patient','2022-09-26 15:15:51'),(26,'egor03052004@gmail.com registered new user: stas.panchenko@gmail.com','2022-09-26 15:16:49'),(27,'User egor03052004@gmail.com made an enroll','2022-10-14 17:39:43');
/*!40000 ALTER TABLE `log_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (2,'admin'),(3,'doctor'),(4,'nurse'),(1,'user');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `second_name` varchar(45) NOT NULL,
  `birth_day` datetime NOT NULL,
  `phone` varchar(45) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(128) NOT NULL,
  `role_id` int NOT NULL,
  `category_id` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `phone_UNIQUE` (`phone`),
  KEY `fk_users_roles1_idx` (`role_id`),
  KEY `fk_users_categories1` (`category_id`),
  CONSTRAINT `fk_users_categories1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_users_roles1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Yegor','Chevardin','2004-05-03 00:00:00','+380999425174','egor03052004@gmail.com','$31$16$aU4LhC8jadu1QGhkt1HJjZLLSoeEb0WmMlWzAGDFwcc',2,1,'2022-09-13 21:22:52','2022-09-13 21:22:52'),(2,'Илья','Крыжановский','2004-05-05 00:00:00','+48574350682','kryzha@kryzha.com','$31$16$rbW-RXl35u-3lf46SvLVbDcc_hSA_3yN6DJ3bW1r5Bs',2,1,'2022-09-13 21:23:19','2022-09-13 21:23:19'),(8,'Ibolit','Doctor','1855-06-14 00:00:00','+5550505005','ibolit@doctor.com','$31$16$-D6KBthGAdiuVYOpTRZBloNQRRyHrt_9103ilp8zmhM',3,4,'2022-09-18 11:34:31','2022-09-18 11:34:31'),(9,'Nurse','Example','1990-06-16 00:00:00','+4528895454','nurse@nurse.com','$31$16$j6Ki7uCUmYsW5Fp8gq66lhopOwOO3E5N5ob3_k98NxE',4,6,'2022-09-19 13:01:49','2022-09-19 13:01:49'),(10,'Егор','Chevardin','2003-05-03 00:00:00','+48099942','example@example.com','$31$16$FV2-ImrFmo6Nu_i7Iinw1W-8DvP5GxolKG_vmqWF3qk',3,4,'2022-09-24 16:36:54','2022-09-24 16:36:54'),(11,'Stas','Panchenko','2003-05-05 00:00:00','+3504483438','stas.panchenko@gmail.com','$31$16$gV6i5bEeRLGdIdP2w3TTLBjSxTvFuDSyg6aFB3YZaX4',1,1,'2022-09-26 15:16:48','2022-09-26 15:16:48');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_doctors`
--

DROP TABLE IF EXISTS `users_doctors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_doctors` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int unsigned NOT NULL,
  `doctor_id` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_users_doctors_users1_idx` (`user_id`),
  KEY `fk_users_doctors_users2_idx` (`doctor_id`),
  CONSTRAINT `fk_users_doctors_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_users_doctors_users2` FOREIGN KEY (`doctor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_doctors`
--

LOCK TABLES `users_doctors` WRITE;
/*!40000 ALTER TABLE `users_doctors` DISABLE KEYS */;
INSERT INTO `users_doctors` VALUES (17,2,9),(18,8,9),(19,1,10),(20,11,10),(21,11,9),(22,1,9);
/*!40000 ALTER TABLE `users_doctors` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-10-14 23:09:16
