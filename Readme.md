# Raycon Health

Hospital web application. **Book** and appointment, staying **at home**.

## 1. Fuctionality

As a reqular *user*:

- Make request to enroll to a doctor;
- Reqister and controll your account. Change or delete information;
- See your hospital card and download it in `.pdf` format;
- See your doctors;
- See your appointments;
- Delete doctors and appointments;

As a *doctor*:

- Same functionality as *user*;
- View your patients and their hospital card;
- Add diagnosis to your patients;
- Make an appointment with patients;
- Delete patients and appointments after procedures;
> Notice, that if you are *nurse* - you cannot make surgeon appointments

As an *admin*:

- Same functionality as a regular *user*;
- Controll enrolls from users and enroll them to a doctor;
- Delete enrolls;
- Controll, delete, search, filter and change information of other users of all types;
- Update user permissions;
- Controll application logging (Delete and find needed items);

## 2. Set up this application on your computer

- Run `rayconhealth.sql` script to create a database;
- Download and run app on apache-tomcat 10th version;
- Set up your IDE and run it or compile it by yourself to `.war` archieve, put in `webapps` dirrectory in apache-tomcat and enjoy :)

> Java 18 required

You might change **information about database** in `src/main/webapp/META-INF/context.xml` dirrectory to your own settings:

```xml
<Context>
    <Resource name="jdbc/rayconhealth" auth="Container" type="javax.sql.DataSource"
              maxTotal="100" maxIdle="30" maxWaitMillis="10000"
              username="rayconhealth" password="rayconhealth" driverClassName="com.mysql.cj.jdbc.Driver"
              url="jdbc:mysql://localhost:3306/rayconhealth"/>

</Context>
```