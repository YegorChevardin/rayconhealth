package com.rayconhealth.rayconhealth.filters;

import com.rayconhealth.rayconhealth.facades.DataValidationFacade;
import jakarta.servlet.*;
import jakarta.servlet.annotation.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebFilter(filterName = "DataFilter", value = {"/edit-account", "/register", "/appointment", "/system", "/diagnos", "/users-list", "/doctors-list", "/admins-list"})
public class DataFilter implements Filter {
    public void init(FilterConfig config) throws ServletException {
    }

    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        if (httpRequest.getMethod().equals("POST")) {
            String url = httpRequest.getRequestURI();
            if (url.equals(httpRequest.getContextPath() + "/diagnos")) {
                if (DataValidationFacade.validateDiagnos(httpRequest)) {
                    chain.doFilter(request, response);
                } else {
                    throw new ServletException("Please, type correct text for diagnos (between 5 and 45 characters)");
                }
            } else if (url.equals(httpRequest.getContextPath() + "/system")) {
                if (DataValidationFacade.validateEnroll(httpRequest)) {
                    chain.doFilter(request, response);
                } else {
                    throw new ServletException("Please, type correct explanation of your paint (between 10 and 500 characters)!");
                }
            } else if (url.equals(httpRequest.getContextPath() + "/appointment")) {
                if (DataValidationFacade.validateMeeting(httpRequest)) {
                    chain.doFilter(request, response);
                } else {
                    throw new ServletException("Please, select a correct date for meeting");
                }
            } else if (url.equals(httpRequest.getContextPath() + "/edit-account")) {
                if (request.getParameter("birth-day") != null) {
                    if (DataValidationFacade.validateRegistration(httpRequest)) {
                        chain.doFilter(request, response);
                    } else {
                        throw new ServletException("Inserted data is not valid. Please, recheck your credentials!");
                    }
                } else {
                    chain.doFilter(request, response);
                }
            } else {
                if (DataValidationFacade.validateRegistration(httpRequest)) {
                    chain.doFilter(request, response);
                } else {
                    throw new ServletException("Inserted data is not valid. Please, recheck your credentials!");
                }
            }
        } else {
            chain.doFilter(request, response);
        }
    }
}
