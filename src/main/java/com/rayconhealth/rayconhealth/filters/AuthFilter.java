package com.rayconhealth.rayconhealth.filters;

import com.rayconhealth.rayconhealth.facades.AuthenticationFacade;
import jakarta.servlet.*;
import jakarta.servlet.annotation.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;

@WebFilter(filterName = "AuthFilter", value = {"/system", "/doctor-appointments", "/appointments", "/appointment", "/delete-appointment", "/account", "/edit-account", "/delete-pair", "/enroll", "/delete-enroll", "/enrolls", "/users-list", "/doctors-list", "/admins-list", "/patients-list", "/hospital-card", "/my-doctors", "/user", "/diagnos", "/logs", "/log", "/delete-log"})
public class AuthFilter implements Filter {
    public void init(FilterConfig config) throws ServletException {
    }

    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        if (!AuthenticationFacade.checkAuth((HttpServletRequest) request)) {
            HttpServletResponse httpResponse = (HttpServletResponse) response;
            httpResponse.sendRedirect("/login");
        } else {
            chain.doFilter(request, response);
        }
    }
}
