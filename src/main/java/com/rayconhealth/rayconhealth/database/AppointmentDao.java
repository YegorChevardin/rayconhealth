package com.rayconhealth.rayconhealth.database;

import com.rayconhealth.rayconhealth.database.interfaces.AppointmentDaoInterface;
import com.rayconhealth.rayconhealth.models.Appointment;
import com.rayconhealth.rayconhealth.models.HospitalCard;
import com.rayconhealth.rayconhealth.models.User;
import static com.rayconhealth.rayconhealth.database.UserDao.DOCTOR_PAIR_NAME;
import static com.rayconhealth.rayconhealth.database.UserDao.USER_PAIR_NAME;
import static com.rayconhealth.rayconhealth.database.pereferences.DatabasePreferences.getDataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AppointmentDao implements AppointmentDaoInterface {
    private static final String GET_ALL = "SELECT * FROM appointments";
    private static final String INSERT_DATA = "INSERT INTO appointments (meeting_time, user_doctor_id, appointment_type_id, hospital_card_id) VALUES";
    private static final String GET_DOCTORS_APPOINTMENTS = GET_ALL + " JOIN users_doctors ON users_doctors.id = appointments.user_doctor_id";
    private static AppointmentDao instance = null;
    private static final String GET_APPOINTMENT_TYPES = "SELECT * FROM appointment_types";
    private AppointmentDao() {}

    public static AppointmentDao getInstance() {
        if (instance == null) {
            instance = new AppointmentDao();
        }
        return instance;
    }

    @Override
    public List<Appointment> getDoctorAppointments(User doctor) {
        List<Appointment> appointments = new ArrayList<>();

        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(GET_DOCTORS_APPOINTMENTS + " WHERE users_doctors.doctor_id = ?")) {
            int count = 0;
            statement.setLong(++count, doctor.getId());
            ResultSet resultSet = statement.executeQuery();
            User user = null;
            HospitalCard hospitalCard = null;
            while (resultSet.next()) {
                hospitalCard = HospitalCardDao.getInstance().getById(resultSet.getInt("hospital_card_id"));
                user = UserDao.getInstance().getUserByHospitalCard(hospitalCard);
                appointments.add(new Appointment(
                        resultSet.getInt("id"),
                        resultSet.getDate("meeting_time"),
                        doctor,
                        user,
                        getAppointmentType(resultSet.getInt("appointment_type_id")),
                        hospitalCard
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get doctor Appointments: " + doctor.getId(), e);
        }

        return appointments;
    }

    @Override
    public String getAppointmentType(int appointmentTypeId) {
        String result = null;

        try (Connection connection = getDataSource().getConnection();
            PreparedStatement statement = connection.prepareStatement(GET_APPOINTMENT_TYPES + " WHERE id = ?")) {
            int k = 0;
            statement.setLong(++k, appointmentTypeId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getString("name");
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get appointment type with this id: " + appointmentTypeId, e);
        }

        return result;
    }

    @Override
    public Integer getAppointmentTypeId(String name) {
        Integer result = null;

        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(GET_APPOINTMENT_TYPES + " WHERE name = ?")) {
            int k = 0;
            statement.setString(++k, name);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getInt("id");
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get appointment type with this name: " + name, e);
        }

        return result;
    }

    @Override
    public List<Appointment> getUserAppointments(User user) {
        List<Appointment> appointments = new ArrayList<>();

        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(GET_DOCTORS_APPOINTMENTS + " WHERE users_doctors.user_id = ?")) {
            int count = 0;
            statement.setLong(++count, user.getId());
            ResultSet resultSet = statement.executeQuery();
            User doctor = null;
            HospitalCard hospitalCard = null;
            Map<String, User> userDoctorPair = null;
            while (resultSet.next()) {
                hospitalCard = HospitalCardDao.getInstance().getById(resultSet.getInt("hospital_card_id"));
                userDoctorPair = UserDao.getInstance().getUserDoctorPair(resultSet.getInt("user_doctor_id"));
                doctor = userDoctorPair.get(DOCTOR_PAIR_NAME);
                appointments.add(new Appointment(
                        resultSet.getInt("id"),
                        resultSet.getDate("meeting_time"),
                        doctor,
                        user,
                        getAppointmentType(resultSet.getInt("appointment_type_id")),
                        hospitalCard
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get user Appointments: " + user.getId(), e);
        }

        return appointments;
    }

    @Override
    public List<Appointment> getAppointmentsByHospitalCard(HospitalCard hospitalCard) {
        List<Appointment> appointments = new ArrayList<>();

        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(GET_ALL + " WHERE hospital_card_id = ?")) {
            int count = 0;
            statement.setLong(++count, hospitalCard.getId());
            ResultSet resultSet = statement.executeQuery();
            User user = null;
            User doctor = null;
            Map<String, User> userDoctorPair = null;
            while (resultSet.next()) {
                userDoctorPair = UserDao.getInstance().getUserDoctorPair(resultSet.getInt("user_doctor_id"));
                user = userDoctorPair.get(USER_PAIR_NAME);
                doctor = userDoctorPair.get(DOCTOR_PAIR_NAME);
                appointments.add(new Appointment(
                        resultSet.getInt("id"),
                        resultSet.getDate("meeting_time"),
                        doctor,
                        user,
                        getAppointmentType(resultSet.getInt("appointment_type_id")),
                        hospitalCard
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get appointments with current hospital card: " + hospitalCard.getName(), e);
        }

        return appointments;
    }

    @Override
    public List<Appointment> getAppointmentsByType(String type) {
        List<Appointment> appointments = new ArrayList<>();
        Integer typeId = getAppointmentTypeId(type);

        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(GET_ALL + " WHERE appointment_type_id = ?")) {
            if (typeId == null) throw new NullPointerException("this type is not existing.");
            int count = 0;
            statement.setLong(++count, typeId);
            ResultSet resultSet = statement.executeQuery();
            User user = null;
            User doctor = null;
            HospitalCard hospitalCard = null;
            Map<String, User> userDoctorPair = null;
            while (resultSet.next()) {
                userDoctorPair = UserDao.getInstance().getUserDoctorPair(resultSet.getInt("user_doctor_id"));
                user = userDoctorPair.get(USER_PAIR_NAME);
                doctor = userDoctorPair.get(DOCTOR_PAIR_NAME);
                hospitalCard = HospitalCardDao.getInstance().getUsersHospitalCard(user);
                appointments.add(new Appointment(
                        resultSet.getInt("id"),
                        resultSet.getDate("meeting_time"),
                        doctor,
                        user,
                        getAppointmentType(resultSet.getInt("appointment_type_id")),
                        hospitalCard
                ));
            }
        } catch (SQLException | NullPointerException e) {
            throw new RuntimeException("Cannot get appointments with this name: " + type, e);
        }

        return appointments;
    }

    @Override
    public boolean insert(Appointment object) {
        boolean result = false;

        try (Connection connection = getDataSource().getConnection();
            PreparedStatement statement = connection.prepareStatement(INSERT_DATA + " (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS)) {
            int count = 0;
            statement.setDate(++count, object.getMeetingDate());
            statement.setInt(++count, UserDao.getInstance().getUserDoctorIdByPair(object.getUser().getId(), object.getDoctor().getId()));
            statement.setInt(++count, getAppointmentTypeId(object.getAppointmentType()));
            statement.setLong(++count, object.getHospitalCard().getId());
            int updateCount = statement.executeUpdate();
            if (updateCount > 0) {
                ResultSet resultSet = statement.getGeneratedKeys();
                if (resultSet.next()) {
                    object.setId(resultSet.getInt(1));
                    result = true;
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot insert this data to database", e);
        }

        return result;
    }

    @Override
    public List<Appointment> getAll() {
        List<Appointment> appointments = new ArrayList<>();

        try (Connection connection = getDataSource().getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(GET_ALL)) {
            User doctor = null;
            User user = null;
            HospitalCard hospitalCard = null;
            Map<String, User> userDoctorPair = null;
            while (resultSet.next()) {
                userDoctorPair = UserDao.getInstance().getUserDoctorPair(resultSet.getInt("user_doctor_id"));
                user = userDoctorPair.get(USER_PAIR_NAME);
                doctor = userDoctorPair.get(DOCTOR_PAIR_NAME);
                hospitalCard = HospitalCardDao.getInstance().getUsersHospitalCard(user);
                appointments.add(new Appointment(
                        resultSet.getInt("id"),
                        resultSet.getDate("meeting_time"),
                        doctor,
                        user,
                        getAppointmentType(resultSet.getInt("appointment_type_id")),
                        hospitalCard
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get all appointments from database", e);
        }

        return appointments;
    }

    @Override
    public Appointment getById(int id) {
        Appointment appointment = null;

        try (Connection connection = getDataSource().getConnection();
            PreparedStatement statement = connection.prepareStatement(GET_ALL + " WHERE id = ?")) {
            int count = 0;
            statement.setLong(++count, id);
            ResultSet resultSet = statement.executeQuery();
            Map<String, User> userDoctorMap = null;
            if (resultSet.next()) {
                userDoctorMap = UserDao.getInstance().getUserDoctorPair(resultSet.getInt("user_doctor_id"));
                appointment = new Appointment(
                        resultSet.getInt("id"),
                        resultSet.getDate("meeting_time"),
                        userDoctorMap.get(DOCTOR_PAIR_NAME),
                        userDoctorMap.get(USER_PAIR_NAME),
                        getAppointmentType(resultSet.getInt("appointment_type_id")),
                        HospitalCardDao.getInstance().getById(resultSet.getInt("hospital_card_id"))
                );
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot find appointment with this id: " + id, e);
        }

        return appointment;
    }

    @Override
    public boolean update(Appointment entity) {
        boolean result = false;

        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement("UPDATE appointment SET meeting_time = ?, user_doctor_id = ?, appointment_type_id = ?, hospital_card_id = ? WHERE id = ?")) {
            int count = 0;
            statement.setDate(++count, entity.getMeetingDate());
            statement.setInt(++count, UserDao.getInstance().getUserDoctorIdByPair(entity.getUser().getId(), entity.getDoctor().getId()));
            statement.setLong(++count, getAppointmentTypeId(entity.getAppointmentType()));
            statement.setLong(++count, entity.getHospitalCard().getId());
            statement.setLong(++count, entity.getId());
            int resultUpdate = statement.executeUpdate();
            if (resultUpdate > 0) {
                result = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot update appointment with this id: " + entity.getId(), e);
        }

        return result;
    }

    @Override
    public boolean delete(Appointment entity) {
        boolean result = false;

        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement("DELETE FROM appointments WHERE id = ?")) {
            int k = 0;
            statement.setLong(++k, entity.getId());
            int deleteCount = statement.executeUpdate();
            if (deleteCount > 0) {
                result = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not delete appointment with this id:" + entity.getId(), e);
        }

        return result;
    }

    @Override
    public List<String> getAllAppointmentTypes() {
        List<String> result = new ArrayList<>();

        try (Connection connection = getDataSource().getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM appointment_types")) {
            while (resultSet.next()) {
                result.add(resultSet.getString("name"));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get all appointment types");
        }

        return result;
    }

    @Override
    public List<Appointment> getDoctorAppointments(User doctor, int start, int total) {
        List<Appointment> appointments = new ArrayList<>();
        try (Connection connection = getDataSource().getConnection();
            PreparedStatement statement = connection.prepareStatement(GET_DOCTORS_APPOINTMENTS + " WHERE users_doctors.doctor_id = ? LIMIT ?, ?")) {
            int count = 0;
            statement.setLong(++count, doctor.getId());
            statement.setLong(++count, start - 1);
            statement.setLong(++count, total);
            ResultSet resultSet = statement.executeQuery();
            User user = null;
            HospitalCard hospitalCard = null;
            while (resultSet.next()) {
                hospitalCard = HospitalCardDao.getInstance().getById(resultSet.getInt("hospital_card_id"));
                user = UserDao.getInstance().getUserByHospitalCard(hospitalCard);
                appointments.add(new Appointment(
                        resultSet.getInt("id"),
                        resultSet.getDate("meeting_time"),
                        doctor,
                        user,
                        getAppointmentType(resultSet.getInt("appointment_type_id")),
                        hospitalCard
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get appointments from this doctor: " + doctor.getId() + " with pagination", e);
        }
        return appointments;
    }

    @Override
    public List<Appointment> getUserAppointments(User user, int start, int total) {
        List<Appointment> appointments = new ArrayList<>();

        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(GET_DOCTORS_APPOINTMENTS + " WHERE users_doctors.user_id = ? LIMIT ?, ?")) {
            int count = 0;
            statement.setLong(++count, user.getId());
            statement.setLong(++count, start - 1);
            statement.setLong(++count, total);
            ResultSet resultSet = statement.executeQuery();
            User doctor = null;
            HospitalCard hospitalCard = null;
            Map<String, User> userDoctorPair = null;
            while (resultSet.next()) {
                hospitalCard = HospitalCardDao.getInstance().getById(resultSet.getInt("hospital_card_id"));
                userDoctorPair = UserDao.getInstance().getUserDoctorPair(resultSet.getInt("user_doctor_id"));
                doctor = userDoctorPair.get(DOCTOR_PAIR_NAME);
                appointments.add(new Appointment(
                        resultSet.getInt("id"),
                        resultSet.getDate("meeting_time"),
                        doctor,
                        user,
                        getAppointmentType(resultSet.getInt("appointment_type_id")),
                        hospitalCard
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get user Appointments: " + user.getId(), e);
        }

        return appointments;
    }

    @Override
    public int getSizeOfDoctorAppointments(User doctor) {
        int result = 0;
        try (Connection connection = getDataSource().getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT COUNT(*) AS total FROM appointments JOIN users_doctors ON users_doctors.id = appointments.user_doctor_id WHERE users_doctors.doctor_id = ?")) {
            int count = 0;
            statement.setLong(++count, doctor.getId());
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getInt("total");
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get sum of appointments of this doctor: " + doctor.getId());
        }

        return result;
    }

    @Override
    public int getSizeOfUsersAppointments(User user) {
        int result = 0;
        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT COUNT(*) AS total FROM appointments JOIN users_doctors ON users_doctors.id = appointments.user_doctor_id WHERE users_doctors.user_id = ?")) {
            int count = 0;
            statement.setLong(++count, user.getId());
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getInt("total");
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get sum of appointments of this user: " + user.getId());
        }

        return result;
    }

    @Override
    public List<Appointment> getDoctorAppointments(User doctor, Connection connection) {
        List<Appointment> appointments = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(GET_DOCTORS_APPOINTMENTS + " WHERE users_doctors.doctor_id = ?")) {
            int count = 0;
            statement.setLong(++count, doctor.getId());
            ResultSet resultSet = statement.executeQuery();
            User user = null;
            HospitalCard hospitalCard = null;
            while (resultSet.next()) {
                hospitalCard = HospitalCardDao.getInstance().getById(resultSet.getInt("hospital_card_id"));
                user = UserDao.getInstance().getUserByHospitalCard(hospitalCard);
                appointments.add(new Appointment(
                        resultSet.getInt("id"),
                        resultSet.getDate("meeting_time"),
                        doctor,
                        user,
                        getAppointmentType(resultSet.getInt("appointment_type_id")),
                        hospitalCard
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get doctor Appointments: " + doctor.getId(), e);
        }

        return appointments;
    }

    @Override
    public String getAppointmentType(int appointmentTypeId, Connection connection) {
        String result = null;

        try (PreparedStatement statement = connection.prepareStatement(GET_APPOINTMENT_TYPES + " WHERE id = ?")) {
            int k = 0;
            statement.setLong(++k, appointmentTypeId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getString("name");
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get appointment type with this id: " + appointmentTypeId, e);
        }

        return result;
    }

    @Override
    public Integer getAppointmentTypeId(String name, Connection connection) {
        Integer result = null;

        try (PreparedStatement statement = connection.prepareStatement(GET_APPOINTMENT_TYPES + " WHERE name = ?")) {
            int k = 0;
            statement.setString(++k, name);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getInt("id");
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get appointment type with this name: " + name, e);
        }

        return result;
    }

    @Override
    public List<Appointment> getUserAppointments(User user, Connection connection) {
        List<Appointment> appointments = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(GET_DOCTORS_APPOINTMENTS + " WHERE users_doctors.user_id = ?")) {
            int count = 0;
            statement.setLong(++count, user.getId());
            ResultSet resultSet = statement.executeQuery();
            User doctor = null;
            HospitalCard hospitalCard = null;
            Map<String, User> userDoctorPair = null;
            while (resultSet.next()) {
                hospitalCard = HospitalCardDao.getInstance().getById(resultSet.getInt("hospital_card_id"));
                userDoctorPair = UserDao.getInstance().getUserDoctorPair(resultSet.getInt("user_doctor_id"));
                doctor = userDoctorPair.get(DOCTOR_PAIR_NAME);
                appointments.add(new Appointment(
                        resultSet.getInt("id"),
                        resultSet.getDate("meeting_time"),
                        doctor,
                        user,
                        getAppointmentType(resultSet.getInt("appointment_type_id")),
                        hospitalCard
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get user Appointments: " + user.getId(), e);
        }

        return appointments;
    }

    @Override
    public List<Appointment> getAppointmentsByHospitalCard(HospitalCard hospitalCard, Connection connection) {
        List<Appointment> appointments = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(GET_ALL + " WHERE hospital_card_id = ?")) {
            int count = 0;
            statement.setLong(++count, hospitalCard.getId());
            ResultSet resultSet = statement.executeQuery();
            User user = null;
            User doctor = null;
            Map<String, User> userDoctorPair = null;
            while (resultSet.next()) {
                userDoctorPair = UserDao.getInstance().getUserDoctorPair(resultSet.getInt("user_doctor_id"));
                user = userDoctorPair.get(USER_PAIR_NAME);
                doctor = userDoctorPair.get(DOCTOR_PAIR_NAME);
                appointments.add(new Appointment(
                        resultSet.getInt("id"),
                        resultSet.getDate("meeting_time"),
                        doctor,
                        user,
                        getAppointmentType(resultSet.getInt("appointment_type_id")),
                        hospitalCard
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get appointments with current hospital card: " + hospitalCard.getName(), e);
        }

        return appointments;
    }

    @Override
    public List<Appointment> getAppointmentsByType(String type, Connection connection) {
        List<Appointment> appointments = new ArrayList<>();
        Integer typeId = getAppointmentTypeId(type);

        try (PreparedStatement statement = connection.prepareStatement(GET_ALL + " WHERE appointment_type_id = ?")) {
            if (typeId == null) throw new NullPointerException("this type is not existing.");
            int count = 0;
            statement.setLong(++count, typeId);
            ResultSet resultSet = statement.executeQuery();
            User user = null;
            User doctor = null;
            HospitalCard hospitalCard = null;
            Map<String, User> userDoctorPair = null;
            while (resultSet.next()) {
                userDoctorPair = UserDao.getInstance().getUserDoctorPair(resultSet.getInt("user_doctor_id"));
                user = userDoctorPair.get(USER_PAIR_NAME);
                doctor = userDoctorPair.get(DOCTOR_PAIR_NAME);
                hospitalCard = HospitalCardDao.getInstance().getUsersHospitalCard(user);
                appointments.add(new Appointment(
                        resultSet.getInt("id"),
                        resultSet.getDate("meeting_time"),
                        doctor,
                        user,
                        getAppointmentType(resultSet.getInt("appointment_type_id")),
                        hospitalCard
                ));
            }
        } catch (SQLException | NullPointerException e) {
            throw new RuntimeException("Cannot get appointments with this name: " + type, e);
        }

        return appointments;
    }

    @Override
    public boolean insert(Appointment object, Connection connection) {
        boolean result = false;

        try (PreparedStatement statement = connection.prepareStatement(INSERT_DATA + " (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS)) {
            int count = 0;
            statement.setDate(++count, object.getMeetingDate());
            statement.setInt(++count, UserDao.getInstance().getUserDoctorIdByPair(object.getUser().getId(), object.getDoctor().getId()));
            statement.setInt(++count, getAppointmentTypeId(object.getAppointmentType()));
            statement.setLong(++count, object.getHospitalCard().getId());
            int updateCount = statement.executeUpdate();
            if (updateCount > 0) {
                ResultSet resultSet = statement.getGeneratedKeys();
                if (resultSet.next()) {
                    object.setId(resultSet.getInt(1));
                    result = true;
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot insert this data to database", e);
        }

        return result;
    }

    @Override
    public List<Appointment> getAll(Connection connection) {
        List<Appointment> appointments = new ArrayList<>();

        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(GET_ALL)) {
            User doctor = null;
            User user = null;
            HospitalCard hospitalCard = null;
            Map<String, User> userDoctorPair = null;
            while (resultSet.next()) {
                userDoctorPair = UserDao.getInstance().getUserDoctorPair(resultSet.getInt("user_doctor_id"));
                user = userDoctorPair.get(USER_PAIR_NAME);
                doctor = userDoctorPair.get(DOCTOR_PAIR_NAME);
                hospitalCard = HospitalCardDao.getInstance().getUsersHospitalCard(user);
                appointments.add(new Appointment(
                        resultSet.getInt("id"),
                        resultSet.getDate("meeting_time"),
                        doctor,
                        user,
                        getAppointmentType(resultSet.getInt("appointment_type_id")),
                        hospitalCard
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get all appointments from database", e);
        }

        return appointments;
    }

    @Override
    public Appointment getById(int id, Connection connection) {
        Appointment appointment = null;

        try (PreparedStatement statement = connection.prepareStatement(GET_ALL + " WHERE id = ?")) {
            int count = 0;
            statement.setLong(++count, id);
            ResultSet resultSet = statement.executeQuery();
            Map<String, User> userDoctorMap = null;
            if (resultSet.next()) {
                userDoctorMap = UserDao.getInstance().getUserDoctorPair(resultSet.getInt("user_doctor_id"));
                appointment = new Appointment(
                        resultSet.getInt("id"),
                        resultSet.getDate("meeting_time"),
                        userDoctorMap.get(DOCTOR_PAIR_NAME),
                        userDoctorMap.get(USER_PAIR_NAME),
                        getAppointmentType(resultSet.getInt("appointment_type_id")),
                        HospitalCardDao.getInstance().getById(resultSet.getInt("hospital_card_id"))
                );
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot find appointment with this id: " + id, e);
        }

        return appointment;
    }

    @Override
    public boolean update(Appointment entity, Connection connection) {
        boolean result = false;

        try (PreparedStatement statement = connection.prepareStatement("UPDATE appointment SET meeting_time = ?, user_doctor_id = ?, appointment_type_id = ?, hospital_card_id = ? WHERE id = ?")) {
            int count = 0;
            statement.setDate(++count, entity.getMeetingDate());
            statement.setInt(++count, UserDao.getInstance().getUserDoctorIdByPair(entity.getUser().getId(), entity.getDoctor().getId()));
            statement.setLong(++count, getAppointmentTypeId(entity.getAppointmentType()));
            statement.setLong(++count, entity.getHospitalCard().getId());
            statement.setLong(++count, entity.getId());
            int resultUpdate = statement.executeUpdate();
            if (resultUpdate > 0) {
                result = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot update appointment with this id: " + entity.getId(), e);
        }

        return result;
    }

    @Override
    public boolean delete(Appointment entity, Connection connection) {
        boolean result = false;

        try (PreparedStatement statement = connection.prepareStatement("DELETE FROM appointments WHERE id = ?")) {
            int k = 0;
            statement.setLong(++k, entity.getId());
            int deleteCount = statement.executeUpdate();
            if (deleteCount > 0) {
                result = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not delete appointment with this id:" + entity.getId(), e);
        }

        return result;
    }

    @Override
    public List<String> getAllAppointmentTypes(Connection connection) {
        List<String> result = new ArrayList<>();

        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM appointment_types")) {
            while (resultSet.next()) {
                result.add(resultSet.getString("name"));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get all appointment types");
        }

        return result;
    }

    @Override
    public List<Appointment> getDoctorAppointments(User doctor, int start, int total, Connection connection) {
        List<Appointment> appointments = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(GET_DOCTORS_APPOINTMENTS + " WHERE users_doctors.doctor_id = ? LIMIT ?, ?")) {
            int count = 0;
            statement.setLong(++count, doctor.getId());
            statement.setLong(++count, start - 1);
            statement.setLong(++count, total);
            ResultSet resultSet = statement.executeQuery();
            User user = null;
            HospitalCard hospitalCard = null;
            while (resultSet.next()) {
                hospitalCard = HospitalCardDao.getInstance().getById(resultSet.getInt("hospital_card_id"));
                user = UserDao.getInstance().getUserByHospitalCard(hospitalCard);
                appointments.add(new Appointment(
                        resultSet.getInt("id"),
                        resultSet.getDate("meeting_time"),
                        doctor,
                        user,
                        getAppointmentType(resultSet.getInt("appointment_type_id")),
                        hospitalCard
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get appointments from this doctor: " + doctor.getId() + " with pagination", e);
        }
        return appointments;
    }

    @Override
    public List<Appointment> getUserAppointments(User user, int start, int total, Connection connection) {
        List<Appointment> appointments = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(GET_DOCTORS_APPOINTMENTS + " WHERE users_doctors.user_id = ? LIMIT ?, ?")) {
            int count = 0;
            statement.setLong(++count, user.getId());
            statement.setLong(++count, start - 1);
            statement.setLong(++count, total);
            ResultSet resultSet = statement.executeQuery();
            User doctor = null;
            HospitalCard hospitalCard = null;
            Map<String, User> userDoctorPair = null;
            while (resultSet.next()) {
                hospitalCard = HospitalCardDao.getInstance().getById(resultSet.getInt("hospital_card_id"));
                userDoctorPair = UserDao.getInstance().getUserDoctorPair(resultSet.getInt("user_doctor_id"));
                doctor = userDoctorPair.get(DOCTOR_PAIR_NAME);
                appointments.add(new Appointment(
                        resultSet.getInt("id"),
                        resultSet.getDate("meeting_time"),
                        doctor,
                        user,
                        getAppointmentType(resultSet.getInt("appointment_type_id")),
                        hospitalCard
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get user Appointments: " + user.getId(), e);
        }

        return appointments;
    }

    @Override
    public int getSizeOfDoctorAppointments(User doctor, Connection connection) {
        int result = 0;
        try (PreparedStatement statement = connection.prepareStatement("SELECT COUNT(*) AS total FROM appointments JOIN users_doctors ON users_doctors.id = appointments.user_doctor_id WHERE users_doctors.doctor_id = ?")) {
            int count = 0;
            statement.setLong(++count, doctor.getId());
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getInt("total");
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get sum of appointments of this doctor: " + doctor.getId());
        }

        return result;
    }

    @Override
    public int getSizeOfUsersAppointments(User user, Connection connection) {
        int result = 0;
        try (PreparedStatement statement = connection.prepareStatement("SELECT COUNT(*) AS total FROM appointments JOIN users_doctors ON users_doctors.id = appointments.user_doctor_id WHERE users_doctors.user_id = ?")) {
            int count = 0;
            statement.setLong(++count, user.getId());
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getInt("total");
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get sum of appointments of this user: " + user.getId());
        }

        return result;
    }
}
