package com.rayconhealth.rayconhealth.database.interfaces;

import com.rayconhealth.rayconhealth.models.HospitalCard;
import com.rayconhealth.rayconhealth.models.User;

import java.sql.Connection;

public interface HospitalCardDaoInterface extends DaoInterface<HospitalCard> {
    public HospitalCard getUsersHospitalCard(User user);

    public HospitalCard getUsersHospitalCard(User user, Connection connection);
}
