package com.rayconhealth.rayconhealth.database.interfaces;

import com.rayconhealth.rayconhealth.models.Appointment;
import com.rayconhealth.rayconhealth.models.HospitalCard;
import com.rayconhealth.rayconhealth.models.User;

import java.sql.Connection;
import java.util.List;

public interface AppointmentDaoInterface extends DaoInterface<Appointment>{
    public List<Appointment> getDoctorAppointments(User doctor);
    public List<Appointment> getUserAppointments(User user);
    public List<Appointment> getAppointmentsByHospitalCard(HospitalCard hospitalCard);
    public List<Appointment> getAppointmentsByType(String type);
    public String getAppointmentType(int appointmentTypeId);
    public Integer getAppointmentTypeId(String name);
    public List<String> getAllAppointmentTypes();
    public List<Appointment> getDoctorAppointments(User doctor, int start, int total);
    public List<Appointment> getUserAppointments(User user, int start, int total);
    public int getSizeOfDoctorAppointments(User doctor);
    public int getSizeOfUsersAppointments(User user);

    public List<Appointment> getDoctorAppointments(User doctor, Connection connection);
    public List<Appointment> getUserAppointments(User user, Connection connection);
    public List<Appointment> getAppointmentsByHospitalCard(HospitalCard hospitalCard, Connection connection);
    public List<Appointment> getAppointmentsByType(String type, Connection connection);
    public String getAppointmentType(int appointmentTypeId, Connection connection);
    public Integer getAppointmentTypeId(String name, Connection connection);
    public List<String> getAllAppointmentTypes(Connection connection);
    public List<Appointment> getDoctorAppointments(User doctor, int start, int total, Connection connection);
    public List<Appointment> getUserAppointments(User user, int start, int total, Connection connection);
    public int getSizeOfDoctorAppointments(User doctor, Connection connection);
    public int getSizeOfUsersAppointments(User user, Connection connection);
}
