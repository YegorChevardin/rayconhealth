package com.rayconhealth.rayconhealth.database.interfaces;

import com.rayconhealth.rayconhealth.models.Diagnos;
import com.rayconhealth.rayconhealth.models.HospitalCard;
import com.rayconhealth.rayconhealth.models.User;

import java.sql.Connection;
import java.util.List;

public interface DiagnosDaoInterface extends DaoInterface<Diagnos> {
    public List<Diagnos> getDiagnosisByName(String name);
    public List<Diagnos> getDiagnosByDoctor(User doctor);
    public List<Diagnos> getDiagnosByHospitalCard(HospitalCard hospitalCard);
    public Integer getDiagnosStatusId(String name);
    public List<Diagnos> getDiagnosByStatus(String status);
    public String getDiagnosStatusName(int id);
    public List<String> getAllStatuses();

    public List<Diagnos> getDiagnosisByName(String name, Connection connection);
    public List<Diagnos> getDiagnosByDoctor(User doctor, Connection connection);
    public List<Diagnos> getDiagnosByHospitalCard(HospitalCard hospitalCard, Connection connection);
    public Integer getDiagnosStatusId(String name, Connection connection);
    public List<Diagnos> getDiagnosByStatus(String status, Connection connection);
    public String getDiagnosStatusName(int id, Connection connection);
    public List<String> getAllStatuses(Connection connection);
}
