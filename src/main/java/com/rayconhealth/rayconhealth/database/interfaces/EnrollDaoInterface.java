package com.rayconhealth.rayconhealth.database.interfaces;

import com.rayconhealth.rayconhealth.models.Enroll;
import com.rayconhealth.rayconhealth.models.User;

import java.sql.Connection;
import java.util.List;

public interface EnrollDaoInterface extends DaoInterface<Enroll> {
    public List<Enroll> getUserEnrolls(User user);
    public List<Enroll> getAll(int start, int total);
    public int getSize();

    public List<Enroll> getUserEnrolls(User user, Connection connection);
    public List<Enroll> getAll(int start, int total, Connection connection);
    public int getSize(Connection connection);
}
