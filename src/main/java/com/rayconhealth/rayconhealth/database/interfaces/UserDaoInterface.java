package com.rayconhealth.rayconhealth.database.interfaces;

import com.rayconhealth.rayconhealth.models.HospitalCard;
import com.rayconhealth.rayconhealth.models.User;
import com.rayconhealth.rayconhealth.models.classes.Email;

import java.sql.Connection;
import java.util.List;
import java.util.Map;

public interface UserDaoInterface extends DaoInterface<User> {
    public String getCategory(int categoryId);
    public String getRole(int roleId);
    public List<User> getUsersByRole(String role);
    public List<User> getUsersByCategory(String category);
    public List<User> getDoctorUsers(User doctor);
    public List<User> getUserDoctors(User user);
    public User getUserByEmail(Email email);
    public boolean updatePassword(User user);
    public User getUserByHospitalCard(HospitalCard hospitalCard);
    public Map<String, User> getUserDoctorPair(int id);
    public Integer getUserDoctorIdByPair(int usersId, int doctorId);
    public Integer getRoleId(String role);
    public Integer getCategoryId(String category);
    public Integer insertUserDoctorPair(User user, User doctor);
    public List<String> getAllCategories();
    public List<String> getAllRoles();
    public boolean updateRoleAndCategory(User user);
    public boolean deleteAllDoctorPatients(User doctor);
    public boolean deleteDoctorPair(User doctor, User user);

    public String getCategory(int categoryId, Connection connection);
    public String getRole(int roleId, Connection connection);
    public List<User> getUsersByRole(String role, Connection connection);
    public List<User> getUsersByCategory(String category, Connection connection);
    public List<User> getDoctorUsers(User doctor, Connection connection);
    public List<User> getUserDoctors(User user, Connection connection);
    public User getUserByEmail(Email email, Connection connection);
    public boolean updatePassword(User user, Connection connection);
    public User getUserByHospitalCard(HospitalCard hospitalCard, Connection connection);
    public Map<String, User> getUserDoctorPair(int id, Connection connection);
    public Integer getUserDoctorIdByPair(int usersId, int doctorId, Connection connection);
    public Integer getRoleId(String role, Connection connection);
    public Integer getCategoryId(String category, Connection connection);
    public Integer insertUserDoctorPair(User user, User doctor, Connection connection);
    public List<String> getAllCategories(Connection connection);
    public List<String> getAllRoles(Connection connection);
    public boolean updateRoleAndCategory(User user, Connection connection);
    public boolean deleteAllDoctorPatients(User doctor, Connection connection);
    public boolean deleteDoctorPair(User doctor, User user, Connection connection);
}
