package com.rayconhealth.rayconhealth.database.interfaces;

import com.rayconhealth.rayconhealth.models.classes.Log;

import java.sql.Connection;
import java.util.List;

public interface LogDaoInterface extends DaoInterface<Log> {
    public List<Log> getAllPaginated(int start, int total);
    public int getSize();

    public List<Log> getAllPaginated(int start, int total, Connection connection);
    public int getSize(Connection connection);
}
