package com.rayconhealth.rayconhealth.database.interfaces;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.util.List;

public interface DaoInterface<T> {
    public boolean insert(T object);
    public List<T> getAll();
    public T getById(int id);
    public boolean update(T entity);
    public boolean delete(T entity);

    public boolean insert(T object, Connection connection);
    public List<T> getAll(Connection connection);
    public T getById(int id, Connection connection);
    public boolean update(T entity, Connection connection);
    public boolean delete(T entity, Connection connection);
}
