package com.rayconhealth.rayconhealth.database;

import com.rayconhealth.rayconhealth.database.interfaces.HospitalCardDaoInterface;
import com.rayconhealth.rayconhealth.models.HospitalCard;
import com.rayconhealth.rayconhealth.models.User;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import static com.rayconhealth.rayconhealth.database.pereferences.DatabasePreferences.getDataSource;

public class HospitalCardDao implements HospitalCardDaoInterface {
    public static final String GET_ALL_STATEMENT = "SELECT * FROM hospital_cards";
    public static final String INSERT_STATEMENT = "INSERT INTO hospital_cards (name, created_at, user_id) VALUES";
    public static HospitalCardDao instance = null;

    private HospitalCardDao() {}

    public static HospitalCardDao getInstance() {
        if (instance == null) {
            instance = new HospitalCardDao();
        }
        return instance;
    }

    @Override
    public boolean insert(HospitalCard object) {
        boolean result = false;

        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(INSERT_STATEMENT + " (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);) {
            int count = 0;
            statement.setString(++count, object.getName());
            statement.setTimestamp(++count, object.getCreatedAt());
            statement.setLong(++count, object.getUser().getId());

            int updateCount = statement.executeUpdate();
            if (updateCount > 0) {
                ResultSet resultSet = statement.getGeneratedKeys();
                if (resultSet.next()) {
                    object.setId(resultSet.getInt(1));
                    result = true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Inserted data is not valid", e);
        }

        return result;
    }

    @Override
    public List<HospitalCard> getAll() {
        List<HospitalCard> hospitalCards = new ArrayList<>();
        try (Connection connection = getDataSource().getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(GET_ALL_STATEMENT)) {
            while(resultSet.next()) {
                hospitalCards.add(new HospitalCard(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getTimestamp("created_at"),
                        UserDao.getInstance().getById(resultSet.getInt("user_id"))
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get all hospital cards from database", e);
        }
        return hospitalCards;
    }

    @Override
    public HospitalCard getById(int id) {
        HospitalCard hospitalCard = null;

        try (Connection connection = getDataSource().getConnection();
            PreparedStatement statement = connection.prepareStatement(GET_ALL_STATEMENT + " WHERE id = ?")) {
            int count = 0;
            statement.setLong(++count, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                hospitalCard = new HospitalCard(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getTimestamp("created_at"),
                        UserDao.getInstance().getById(resultSet.getInt("user_id"))
                );
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get hospital card by this user id:" + id, e);
        }

        return hospitalCard;
    }

    @Override
    public boolean update(HospitalCard entity) {
        boolean result = false;

        try (Connection connection = getDataSource().getConnection();
            PreparedStatement statement = connection.prepareStatement("UPDATE hospital_cards SET name = ? WHERE id = ?")) {
            int count = 0;
            statement.setString(++count, entity.getName());
            statement.setInt(++count, entity.getId());
            int resultUpdate = statement.executeUpdate();
            if (resultUpdate > 0) {
                result = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot update hospital card with this id: " + entity.getId(), e);
        }

        return result;
    }

    @Override
    public boolean delete(HospitalCard entity) {
        boolean result = false;

        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement("DELETE FROM hosptal_cards WHERE id = ?")) {
            int k = 0;
            statement.setLong(++k, entity.getId());
            int deleteCount = statement.executeUpdate();
            if (deleteCount > 0) {
                result = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not delete hospital card id:" + entity.getId(), e);
        }

        return result;
    }

    @Override
    public HospitalCard getUsersHospitalCard(User user) {
        HospitalCard hospitalCard = null;

        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(GET_ALL_STATEMENT + " WHERE user_id = ?")) {
            int count = 0;
            statement.setLong(++count, user.getId());
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                hospitalCard = new HospitalCard(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getTimestamp("created_at"),
                        user
                );
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get hospital card by this user id:" + user.getId(), e);
        }

        return hospitalCard;
    }

    @Override
    public boolean insert(HospitalCard object, Connection connection) {
        boolean result = false;

        try (PreparedStatement statement = connection.prepareStatement(INSERT_STATEMENT + " (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);) {
            int count = 0;
            statement.setString(++count, object.getName());
            statement.setTimestamp(++count, object.getCreatedAt());
            statement.setLong(++count, object.getUser().getId());

            int updateCount = statement.executeUpdate();
            if (updateCount > 0) {
                ResultSet resultSet = statement.getGeneratedKeys();
                if (resultSet.next()) {
                    object.setId(resultSet.getInt(1));
                    result = true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Inserted data is not valid", e);
        }

        return result;
    }

    @Override
    public List<HospitalCard> getAll(Connection connection) {
        List<HospitalCard> hospitalCards = new ArrayList<>();
        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(GET_ALL_STATEMENT)) {
            while(resultSet.next()) {
                hospitalCards.add(new HospitalCard(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getTimestamp("created_at"),
                        UserDao.getInstance().getById(resultSet.getInt("user_id"))
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get all hospital cards from database", e);
        }
        return hospitalCards;
    }

    @Override
    public HospitalCard getById(int id, Connection connection) {
        HospitalCard hospitalCard = null;

        try (PreparedStatement statement = connection.prepareStatement(GET_ALL_STATEMENT + " WHERE id = ?")) {
            int count = 0;
            statement.setLong(++count, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                hospitalCard = new HospitalCard(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getTimestamp("created_at"),
                        UserDao.getInstance().getById(resultSet.getInt("user_id"))
                );
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get hospital card by this user id:" + id, e);
        }

        return hospitalCard;
    }

    @Override
    public boolean update(HospitalCard entity, Connection connection) {
        boolean result = false;

        try (PreparedStatement statement = connection.prepareStatement("UPDATE hospital_cards SET name = ? WHERE id = ?")) {
            int count = 0;
            statement.setString(++count, entity.getName());
            statement.setInt(++count, entity.getId());
            int resultUpdate = statement.executeUpdate();
            if (resultUpdate > 0) {
                result = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot update hospital card with this id: " + entity.getId(), e);
        }

        return result;
    }

    @Override
    public boolean delete(HospitalCard entity, Connection connection) {
        boolean result = false;

        try (PreparedStatement statement = connection.prepareStatement("DELETE FROM hosptal_cards WHERE id = ?")) {
            int k = 0;
            statement.setLong(++k, entity.getId());
            int deleteCount = statement.executeUpdate();
            if (deleteCount > 0) {
                result = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not delete hospital card id:" + entity.getId(), e);
        }

        return result;
    }

    @Override
    public HospitalCard getUsersHospitalCard(User user, Connection connection) {
        HospitalCard hospitalCard = null;

        try (PreparedStatement statement = connection.prepareStatement(GET_ALL_STATEMENT + " WHERE user_id = ?")) {
            int count = 0;
            statement.setLong(++count, user.getId());
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                hospitalCard = new HospitalCard(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getTimestamp("created_at"),
                        user
                );
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get hospital card by this user id:" + user.getId(), e);
        }

        return hospitalCard;
    }
}
