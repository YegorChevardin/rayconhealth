package com.rayconhealth.rayconhealth.database;

import com.rayconhealth.rayconhealth.database.interfaces.EnrollDaoInterface;
import com.rayconhealth.rayconhealth.models.Enroll;
import com.rayconhealth.rayconhealth.models.User;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.rayconhealth.rayconhealth.database.pereferences.DatabasePreferences.getDataSource;

public class EnrollDao implements EnrollDaoInterface {
    private static final String GET_ALL = "SELECT * FROM enrolls";
    private static final String INSERT_STATEMENT = "INSERT INTO enrolls (user_id, text) VALUES";
    private static EnrollDao instance = null;

    private EnrollDao() {}

    public static EnrollDao getInstance() {
        if (instance == null) {
            instance = new EnrollDao();
        }

        return instance;
    }

    @Override
    public boolean insert(Enroll object) {
        boolean result = false;

        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(INSERT_STATEMENT + " (?, ?)", Statement.RETURN_GENERATED_KEYS)) {
            int count = 0;
            statement.setInt(++count, object.getUser().getId());
            statement.setString(++count, object.getText());

            int updateCount = statement.executeUpdate();
            if (updateCount > 0) {
                ResultSet resultSet = statement.getGeneratedKeys();
                if (resultSet.next()) {
                    object.setId(resultSet.getInt(1));
                    result = true;
                }
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException("Inserted data is not valid", e);
        }

        return result;
    }

    @Override
    public List<Enroll> getAll() {
        List<Enroll> enrolls = new ArrayList<>();

        try (Connection connection = getDataSource().getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(GET_ALL)) {
            while (resultSet.next()) {
                enrolls.add(new Enroll(
                        resultSet.getInt("id"),
                        UserDao.getInstance().getById(resultSet.getInt("user_id")),
                        resultSet.getString("text")
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get any enrolls from database", e);
        }

        return enrolls;
    }

    @Override
    public Enroll getById(int id) {
        Enroll result = null;

        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(GET_ALL + " WHERE id = ?")) {
            int count = 0;
            statement.setLong(++count, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = new Enroll(
                        resultSet.getInt("id"),
                        UserDao.getInstance().getById(resultSet.getInt("user_id")),
                        resultSet.getString("text")
                );
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException("Cannot get enroll by this id: " + id, e);
        }

        return result;
    }

    @Override
    public boolean update(Enroll entity) {
        boolean result = false;

        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement("UPDATE enrolls SET user_id = ?, text = ? WHERE id = ?")) {
            int count = 0;
            statement.setInt(++count, entity.getUser().getId());
            statement.setString(++count, entity.getText());
            int resultUpdate = statement.executeUpdate();
            if (resultUpdate > 0) {
                result = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot update enroll with this id: " + entity.getId(), e);
        }

        return result;
    }

    @Override
    public boolean delete(Enroll entity) {
        boolean result = false;

        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement("DELETE FROM enrolls WHERE id = ?")) {
            int k = 0;
            statement.setLong(++k, entity.getId());
            int deleteCount = statement.executeUpdate();
            if (deleteCount > 0) {
                result = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not delete enrolls with this id:" + entity.getId(), e);
        }

        return result;
    }

    @Override
    public List<Enroll> getUserEnrolls(User user) {
        List<Enroll> enrolls = new ArrayList<>();

        try (Connection connection = getDataSource().getConnection();
            PreparedStatement statement = connection.prepareStatement(GET_ALL + " WHERE user_id = ?")) {
            int count = 0;
            statement.setLong(++count, user.getId());
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                enrolls.add(new Enroll(
                        resultSet.getInt("id"),
                        UserDao.getInstance().getById(resultSet.getInt("user_id")),
                        resultSet.getString("text"))
                );
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot find enrolls with this user id " + user.getId(), e);
        }

        return enrolls;
    }

    @Override
    public List<Enroll> getAll(int start, int total) {
        List<Enroll> enrolls = new ArrayList<>();

        try (Connection connection = getDataSource().getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM enrolls LIMIT ?, ?")) {
            int count = 0;
            statement.setLong(++count, start - 1);
            statement.setLong(++count, total);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                enrolls.add(new Enroll(
                        resultSet.getInt("id"),
                        UserDao.getInstance().getById(resultSet.getInt("user_id")),
                        resultSet.getString("text")));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get paginated enrolls");
        }

        return enrolls;
    }

    @Override
    public int getSize() {
        int result = 0;

        try (Connection connection = getDataSource().getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT COUNT(*) AS enrollsSize FROM enrolls")) {
            if (resultSet.next()) {
                result = resultSet.getInt("enrollsSize");
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get paginated enrolls");
        }

        return result;
    }

    @Override
    public boolean insert(Enroll object, Connection connection) {
        boolean result = false;

        try (PreparedStatement statement = connection.prepareStatement(INSERT_STATEMENT + " (?, ?)", Statement.RETURN_GENERATED_KEYS)) {
            int count = 0;
            statement.setInt(++count, object.getUser().getId());
            statement.setString(++count, object.getText());

            int updateCount = statement.executeUpdate();
            if (updateCount > 0) {
                ResultSet resultSet = statement.getGeneratedKeys();
                if (resultSet.next()) {
                    object.setId(resultSet.getInt(1));
                    result = true;
                }
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException("Inserted data is not valid", e);
        }

        return result;
    }

    @Override
    public List<Enroll> getAll(Connection connection) {
        List<Enroll> enrolls = new ArrayList<>();

        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(GET_ALL)) {
            while (resultSet.next()) {
                enrolls.add(new Enroll(
                        resultSet.getInt("id"),
                        UserDao.getInstance().getById(resultSet.getInt("user_id")),
                        resultSet.getString("text")
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get any enrolls from database", e);
        }

        return enrolls;
    }

    @Override
    public Enroll getById(int id, Connection connection) {
        Enroll result = null;

        try (PreparedStatement statement = connection.prepareStatement(GET_ALL + " WHERE id = ?")) {
            int count = 0;
            statement.setLong(++count, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = new Enroll(
                        resultSet.getInt("id"),
                        UserDao.getInstance().getById(resultSet.getInt("user_id")),
                        resultSet.getString("text")
                );
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException("Cannot get enroll by this id: " + id, e);
        }

        return result;
    }

    @Override
    public boolean update(Enroll entity, Connection connection) {
        boolean result = false;

        try (PreparedStatement statement = connection.prepareStatement("UPDATE enrolls SET user_id = ?, text = ? WHERE id = ?")) {
            int count = 0;
            statement.setInt(++count, entity.getUser().getId());
            statement.setString(++count, entity.getText());
            int resultUpdate = statement.executeUpdate();
            if (resultUpdate > 0) {
                result = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot update enroll with this id: " + entity.getId(), e);
        }

        return result;
    }

    @Override
    public boolean delete(Enroll entity, Connection connection) {
        boolean result = false;

        try (PreparedStatement statement = connection.prepareStatement("DELETE FROM enrolls WHERE id = ?")) {
            int k = 0;
            statement.setLong(++k, entity.getId());
            int deleteCount = statement.executeUpdate();
            if (deleteCount > 0) {
                result = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not delete enrolls with this id:" + entity.getId(), e);
        }

        return result;
    }

    @Override
    public List<Enroll> getUserEnrolls(User user, Connection connection) {
        List<Enroll> enrolls = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(GET_ALL + " WHERE user_id = ?")) {
            int count = 0;
            statement.setLong(++count, user.getId());
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                enrolls.add(new Enroll(
                        resultSet.getInt("id"),
                        UserDao.getInstance().getById(resultSet.getInt("user_id")),
                        resultSet.getString("text"))
                );
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot find enrolls with this user id " + user.getId(), e);
        }

        return enrolls;
    }

    @Override
    public List<Enroll> getAll(int start, int total, Connection connection) {
        List<Enroll> enrolls = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM enrolls LIMIT ?, ?")) {
            int count = 0;
            statement.setLong(++count, start - 1);
            statement.setLong(++count, total);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                enrolls.add(new Enroll(
                        resultSet.getInt("id"),
                        UserDao.getInstance().getById(resultSet.getInt("user_id")),
                        resultSet.getString("text")));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get paginated enrolls");
        }

        return enrolls;
    }

    @Override
    public int getSize(Connection connection) {
        int result = 0;

        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT COUNT(*) AS enrollsSize FROM enrolls")) {
            if (resultSet.next()) {
                result = resultSet.getInt("enrollsSize");
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get paginated enrolls");
        }

        return result;
    }
}
