package com.rayconhealth.rayconhealth.database;

import com.rayconhealth.rayconhealth.database.interfaces.LogDaoInterface;
import com.rayconhealth.rayconhealth.models.classes.Log;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import static com.rayconhealth.rayconhealth.database.pereferences.DatabasePreferences.getDataSource;

public class LogDao implements LogDaoInterface {
    private static LogDao instance = null;

    private LogDao() {}
    public static LogDao getInstance() {
        if (instance == null) {
            instance = new LogDao();
        }
        return instance;
    }
    @Override
    public boolean insert(Log object) {
        boolean result = false;

        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement("INSERT INTO log_events (text, send_at) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS)) {
            int count = 0;
            statement.setString(++count, object.getText());
            statement.setTimestamp(++count, object.getSendAt());
            int generatedCount = statement.executeUpdate();
            if (generatedCount > 0) {
                ResultSet resultSet = statement.getGeneratedKeys();
                if (resultSet.next()) {
                    object.setId(resultSet.getInt(1));
                    result = true;
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot insert this log into database");
        }

        return result;
    }

    @Override
    public List<Log> getAll() {
        List<Log> result = new ArrayList<>();

        try (Connection connection = getDataSource().getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM log_events")) {
            while (resultSet.next()) {
                result.add(new Log(
                        resultSet.getInt("id"),
                        resultSet.getString("text"),
                        resultSet.getTimestamp("send_at")
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get all events");
        }

        return result;
    }

    @Override
    public Log getById(int id) {
        Log result = null;

        try (Connection connection = getDataSource().getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM log_events WHERE id = ?")) {
            int count = 0;
            statement.setLong(++count, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = new Log(
                        resultSet.getInt("id"),
                        resultSet.getString("text"),
                        resultSet.getTimestamp("send_at")
                );
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get log by this id " + id);
        }

        return result;
    }

    @Override
    public boolean update(Log entity) {
        boolean result = false;

        try (Connection connection = getDataSource().getConnection();
            PreparedStatement statement = connection.prepareStatement("UPDATE log_events SET text = ?, send_at = ? WHERE id = ?")) {
            int count = 0;
            statement.setString(++count, entity.getText());
            statement.setTimestamp(++count, entity.getSendAt());
            statement.setLong(++count, entity.getId());
            int countUpdate = statement.executeUpdate();
            if (countUpdate > 0) {
                result = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot update log with this id: " + entity.getId());
        }

        return result;
    }

    @Override
    public boolean delete(Log entity) {
        boolean result = false;

        try (Connection connection = getDataSource().getConnection();
            PreparedStatement statement = connection.prepareStatement("DELETE FROM log_events WHERE id = ?")) {
            int count = 0;
            statement.setLong(++count, entity.getId());
            int countDelete = statement.executeUpdate();
            if (countDelete > 0) {
                result = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot delete log with this id:" + entity.getId());
        }

        return result;
    }

    @Override
    public List<Log> getAllPaginated(int start, int total) {
        List<Log> result = new ArrayList<>();

        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM log_events LIMIT ?, ?")) {
            int count = 0;
            statement.setLong(++count, start - 1);
            statement.setLong(++count, total);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(new Log(
                        resultSet.getInt("id"),
                        resultSet.getString("text"),
                        resultSet.getTimestamp("send_at")
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get all events with pagination");
        }

        return result;
    }

    @Override
    public int getSize() {
        int result = 0;

        try (Connection connection = getDataSource().getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT COUNT(*) AS logs_sum FROM log_events")) {
            if (resultSet.next()) {
                result = resultSet.getInt("logs_sum");
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get sum of all logs form database");
        }

        return result;
    }

    @Override
    public boolean insert(Log object, Connection connection) {
        boolean result = false;

        try (PreparedStatement statement = connection.prepareStatement("INSERT INTO log_events (text, send_at) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS)) {
            int count = 0;
            statement.setString(++count, object.getText());
            statement.setTimestamp(++count, object.getSendAt());
            int generatedCount = statement.executeUpdate();
            if (generatedCount > 0) {
                ResultSet resultSet = statement.getGeneratedKeys();
                if (resultSet.next()) {
                    object.setId(resultSet.getInt(1));
                    result = true;
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot insert this log into database");
        }

        return result;
    }

    @Override
    public List<Log> getAll(Connection connection) {
        List<Log> result = new ArrayList<>();

        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM log_events")) {
            while (resultSet.next()) {
                result.add(new Log(
                        resultSet.getInt("id"),
                        resultSet.getString("text"),
                        resultSet.getTimestamp("send_at")
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get all events");
        }

        return result;
    }

    @Override
    public Log getById(int id, Connection connection) {
        Log result = null;

        try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM log_events WHERE id = ?")) {
            int count = 0;
            statement.setLong(++count, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = new Log(
                        resultSet.getInt("id"),
                        resultSet.getString("text"),
                        resultSet.getTimestamp("send_at")
                );
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get log by this id " + id);
        }

        return result;
    }

    @Override
    public boolean update(Log entity, Connection connection) {
        boolean result = false;

        try (PreparedStatement statement = connection.prepareStatement("UPDATE log_events SET text = ?, send_at = ? WHERE id = ?")) {
            int count = 0;
            statement.setString(++count, entity.getText());
            statement.setTimestamp(++count, entity.getSendAt());
            statement.setLong(++count, entity.getId());
            int countUpdate = statement.executeUpdate();
            if (countUpdate > 0) {
                result = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot update log with this id: " + entity.getId());
        }

        return result;
    }

    @Override
    public boolean delete(Log entity, Connection connection) {
        boolean result = false;

        try (PreparedStatement statement = connection.prepareStatement("DELETE FROM log_events WHERE id = ?")) {
            int count = 0;
            statement.setLong(++count, entity.getId());
            int countDelete = statement.executeUpdate();
            if (countDelete > 0) {
                result = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot delete log with this id:" + entity.getId());
        }

        return result;
    }

    @Override
    public List<Log> getAllPaginated(int start, int total, Connection connection) {
        List<Log> result = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM log_events LIMIT ?, ?")) {
            int count = 0;
            statement.setLong(++count, start - 1);
            statement.setLong(++count, total);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(new Log(
                        resultSet.getInt("id"),
                        resultSet.getString("text"),
                        resultSet.getTimestamp("send_at")
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get all events with pagination");
        }

        return result;
    }

    @Override
    public int getSize(Connection connection) {
        int result = 0;

        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT COUNT(*) AS logs_sum FROM log_events")) {
            if (resultSet.next()) {
                result = resultSet.getInt("logs_sum");
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get sum of all logs form database");
        }

        return result;
    }
}
