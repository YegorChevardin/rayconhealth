package com.rayconhealth.rayconhealth.database.pereferences;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class DatabasePreferences {
    public static final int PAGINATION_MAX_PAGE = 10;
    public static DataSource getDataSource() {
        DataSource ds = null;
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");
            ds = (DataSource) envContext.lookup("jdbc/rayconhealth");
        } catch (NamingException e) {
            throw new RuntimeException("Could not connect to database", e);
        }
        return ds;
    }
}
