package com.rayconhealth.rayconhealth.database;

import com.rayconhealth.rayconhealth.database.interfaces.UserDaoInterface;
import com.rayconhealth.rayconhealth.models.HospitalCard;
import com.rayconhealth.rayconhealth.models.User;
import com.rayconhealth.rayconhealth.models.classes.Email;
import com.rayconhealth.rayconhealth.models.classes.Password;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static com.rayconhealth.rayconhealth.database.pereferences.DatabasePreferences.getDataSource;

public class UserDao implements UserDaoInterface {
    private static final String GET_ALL_STATEMENT = "SELECT * FROM users";
    private static final String INSERT_STATEMENT = "INSERT INTO users (name, second_name, birth_day, phone, email, password, role_id, category_id) VALUES";
    private static final String GET_CATEGORY = "SELECT * FROM categories";
    private static final String GET_ROLE = "SELECT * FROM roles";
    private static final String GET_USERS_BY_CATEGORY = GET_ALL_STATEMENT + " JOIN categories ON users.category_id = categories.id";
    private static final String GET_USERS_BY_ROLE = GET_ALL_STATEMENT + " JOIN roles ON users.role_id = roles.id";
    private static final String GET_USER_BY_HOSPITAL_CARD = GET_ALL_STATEMENT + " JOIN hospital_cards ON hospital_cards.user_id = users.id";
    private static final String GET_USER_DOCTOR_PAIR = "SELECT * FROM users_doctors";
    public static final String DOCTOR_PAIR_NAME = "doctor";
    public static final String USER_PAIR_NAME = "user";
    public static final String DEFAULT_USER_ROLE = "user";
    public static final String ADMIN_ROLE = "admin";
    public static final String DOCTOR_ROLE = "doctor";
    public static final String DEFAULT_USER_CATEGORY = "patient";
    private static UserDao instance = null;

    private UserDao() {}

    public static UserDao getInstance() {
        if (instance == null) {
            instance = new UserDao();
        }
        return instance;
    }

    @Override
    public Integer getRoleId(String role) {
        Integer result = null;

        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM roles WHERE name = ?")) {
            int k = 0;
            statement.setString(++k, role);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getInt("id");
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get role with this name: " + role, e);
        }

        return result;
    }

    @Override
    public Integer getCategoryId(String category) {
        Integer result = null;

        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM categories WHERE name = ?")) {
            int k = 0;
            statement.setString(++k, category);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getInt("id");
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get category with this name: " + category, e);
        }

        return result;
    }

    @Override
    public boolean insert(User object) {
        boolean result = false;

        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(INSERT_STATEMENT + " (?, ?, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS)) {
            int count = 0;
            statement.setString(++count, object.getName());
            statement.setString(++count, object.getSecondName());
            statement.setDate(++count, object.getBirthDay());
            statement.setString(++count, object.getPhone());
            statement.setString(++count, object.getEmail().getValue());
            statement.setString(++count, object.getPassword().getValue());
            statement.setLong(++count, getRoleId(object.getRole()));
            statement.setLong(++count, getCategoryId(object.getCategory()));

            int updateCount = statement.executeUpdate();
            if (updateCount > 0) {
                ResultSet resultSet = statement.getGeneratedKeys();
                if (resultSet.next()) {
                    object.setId(resultSet.getInt(1));
                    result = true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("The phone or email already exists", e);
        }

        return result;
    }

    @Override
    public List<User> getAll() {
        List<User> usersList = new ArrayList<>();

        try (Connection connection = getDataSource().getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(GET_ALL_STATEMENT)) {
            while (resultSet.next()) {
                usersList.add(new User(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("second_name"),
                        resultSet.getDate("birth_day"),
                        Email.generateEmail(resultSet.getString("email")),
                        resultSet.getString("phone"),
                        Password.generatePassword(resultSet.getString("password"), true),
                        getRole(resultSet.getInt("role_id")),
                        getCategory(resultSet.getInt("category_id")),
                        resultSet.getTimestamp("created_at"),
                        resultSet.getTimestamp("updated_at")
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not get result from database");
        }

        return usersList;
    }

    @Override
    public User getById(int id) {
        User user = null;
        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(GET_ALL_STATEMENT + " WHERE id = ?;")) {
            int k = 0;
            statement.setLong(++k, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                user = new User(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("second_name"),
                        resultSet.getDate("birth_day"),
                        Email.generateEmail(resultSet.getString("email")),
                        resultSet.getString("phone"),
                        Password.generatePassword(resultSet.getString("password"), true),
                        getRole(resultSet.getInt("role_id")),
                        getCategory(resultSet.getInt("category_id")),
                        resultSet.getTimestamp("created_at"),
                        resultSet.getTimestamp("updated_at")
                );
            }
        } catch (SQLException e) {
            throw new RuntimeException("Something went wrong when trying to find a user with specific id", e);
        }

        return user;
    }

    @Override
    public boolean update(User entity) {
        boolean result = false;
        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement("UPDATE users SET name = ?, second_name = ?, birth_day = ?, phone = ?, email = ? WHERE id = ?")) {
            int k = 0;
            statement.setString(++k, entity.getName());
            statement.setString(++k, entity.getSecondName());
            statement.setDate(++k, entity.getBirthDay());
            statement.setString(++k, entity.getPhone());
            statement.setString(++k, entity.getEmail().getValue());
            statement.setLong(++k, entity.getId());
            int updateResult = statement.executeUpdate();
            if (updateResult > 0) {
                result = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not update user with id: " + entity.getId(), e);
        }

        return result;
    }

    @Override
    public boolean updatePassword(User user) {
        boolean result = false;
        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement("UPDATE users SET password = ? WHERE id = ?")) {
            int k = 0;
            statement.setString(++k, user.getPassword().getValue());
            statement.setLong(++k, user.getId());
            int updateCount = statement.executeUpdate();
            if (updateCount > 0) {
                result = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not update user with id: " + user.getId(), e);
        }

        return result;
    }

    @Override
    public boolean delete(User entity) {
        boolean result = false;

        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement("DELETE FROM users WHERE id = ?")) {
            int k = 0;
            statement.setLong(++k, entity.getId());
            int deleteCount = statement.executeUpdate();
            if (deleteCount > 0) {
                result = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not delete user", e);
        }

        return result;
    }

    @Override
    public String getCategory(int categoryId) {
        String result = null;

        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(GET_CATEGORY + " WHERE id = ?")) {
            int k = 0;
            statement.setLong(++k, categoryId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getString("name");
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not get user category", e);
        }

        return result;
    }

    @Override
    public String getRole(int roleId) {
        String result = null;

        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(GET_ROLE + " WHERE id = ?")) {
            int k = 0;
            statement.setLong(++k, roleId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getString("name");
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not get user role", e);
        }

        return result;
    }

    @Override
    public List<User> getUsersByRole(String role) {
        List<User> users = new ArrayList<>();
        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(GET_USERS_BY_ROLE + " WHERE roles.name = ?")) {
            int k = 0;
            statement.setString(++k, role);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                users.add(new User(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("second_name"),
                        resultSet.getDate("birth_day"),
                        Email.generateEmail(resultSet.getString("email")),
                        resultSet.getString("phone"),
                        Password.generatePassword(resultSet.getString("password"), true),
                        getRole(resultSet.getInt("role_id")),
                        getCategory(resultSet.getInt("category_id")),
                        resultSet.getTimestamp("created_at"),
                        resultSet.getTimestamp("updated_at")
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not get users by roles", e);
        }

        return users;
    }

    @Override
    public List<User> getUsersByCategory(String category) {
        List<User> users = new ArrayList<>();
        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(GET_USERS_BY_CATEGORY + " WHERE categories.name = ?")) {
            int k = 0;
            statement.setString(++k, category);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                users.add(new User(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("second_name"),
                        resultSet.getDate("birth_day"),
                        Email.generateEmail(resultSet.getString("email")),
                        resultSet.getString("phone"),
                        Password.generatePassword(resultSet.getString("password"), true),
                        getRole(resultSet.getInt("role_id")),
                        getCategory(resultSet.getInt("category_id")),
                        resultSet.getTimestamp("created_at"),
                        resultSet.getTimestamp("updated_at")
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not get users by categories", e);
        }

        return users;
    }

    @Override
    public List<User> getDoctorUsers(User doctor) {
        List<User> users = new ArrayList<>();
        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(GET_ALL_STATEMENT + " JOIN users_doctors ON users_doctors.user_id = users.id WHERE users_doctors.doctor_id = ?")) {
            int k = 0;
            statement.setLong(++k, doctor.getId());
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                users.add(new User(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("second_name"),
                        resultSet.getDate("birth_day"),
                        Email.generateEmail(resultSet.getString("email")),
                        resultSet.getString("phone"),
                        Password.generatePassword(resultSet.getString("password"), true),
                        getRole(resultSet.getInt("role_id")),
                        getCategory(resultSet.getInt("category_id")),
                        resultSet.getTimestamp("created_at"),
                        resultSet.getTimestamp("updated_at")
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not get users by this doctor " + doctor.getId(), e);
        }

        return users;
    }

    @Override
    public List<User> getUserDoctors(User user) {
        List<User> users = new ArrayList<>();
        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(GET_ALL_STATEMENT + " JOIN users_doctors ON users_doctors.doctor_id = users.id WHERE users_doctors.user_id = ?")) {
            int k = 0;
            statement.setLong(++k, user.getId());
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                users.add(new User(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("second_name"),
                        resultSet.getDate("birth_day"),
                        Email.generateEmail(resultSet.getString("email")),
                        resultSet.getString("phone"),
                        Password.generatePassword(resultSet.getString("password"), true),
                        getRole(resultSet.getInt("role_id")),
                        getCategory(resultSet.getInt("category_id")),
                        resultSet.getTimestamp("created_at"),
                        resultSet.getTimestamp("updated_at")
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not get doctors by this user " + user.getId(), e);
        }

        return users;
    }

    @Override
    public User getUserByEmail(Email email) {
        User user = null;
        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(GET_ALL_STATEMENT + " WHERE email = ?;")) {
            int k = 0;
            statement.setString(++k, email.getValue());
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                user = new User(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("second_name"),
                        resultSet.getDate("birth_day"),
                        Email.generateEmail(resultSet.getString("email")),
                        resultSet.getString("phone"),
                        Password.generatePassword(resultSet.getString("password"), true),
                        getRole(resultSet.getInt("role_id")),
                        getCategory(resultSet.getInt("category_id")),
                        resultSet.getTimestamp("created_at"),
                        resultSet.getTimestamp("updated_at")
                );
            }
        } catch (SQLException e) {
            throw new RuntimeException("This email does not exist: " + email.getValue(), e);
        }

        return user;
    }

    @Override
    public User getUserByHospitalCard(HospitalCard hospitalCard) {
        User user = null;
        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(GET_USER_BY_HOSPITAL_CARD + " WHERE hospital_cards.id = ?")) {
            int k = 0;
            statement.setLong(++k, hospitalCard.getId());
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                user = new User(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("second_name"),
                        resultSet.getDate("birth_day"),
                        Email.generateEmail(resultSet.getString("email")),
                        resultSet.getString("phone"),
                        Password.generatePassword(resultSet.getString("password"), true),
                        getRole(resultSet.getInt("role_id")),
                        getCategory(resultSet.getInt("category_id")),
                        resultSet.getTimestamp("created_at"),
                        resultSet.getTimestamp("updated_at")
                );
            }
        } catch (SQLException e) {
            throw new RuntimeException("Something went wrong when trying to find a user with this hospital_card: " + hospitalCard.getName(), e);
        }

        return user;
    }

    @Override
    public Map<String, User> getUserDoctorPair(int id) {
        Map<String, User> userDoctorPair = new HashMap<>();

        try (Connection connection = getDataSource().getConnection();
            PreparedStatement statement = connection.prepareStatement(GET_USER_DOCTOR_PAIR + " WHERE id = ?")) {
            int k = 0;
            statement.setLong(++k, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                userDoctorPair.put(USER_PAIR_NAME, UserDao.getInstance().getById(resultSet.getInt("user_id")));
                userDoctorPair.put(DOCTOR_PAIR_NAME, UserDao.getInstance().getById(resultSet.getInt("doctor_id")));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get this users_doctors pair: " + id, e);
        }

        return userDoctorPair;
    }

    @Override
    public Integer getUserDoctorIdByPair(int usersId, int doctorId) {
        Integer result = null;

        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(GET_USER_DOCTOR_PAIR + " WHERE user_id = ? AND doctor_id = ?")) {
            int k = 0;
            statement.setLong(++k, usersId);
            statement.setLong(++k, doctorId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getInt("id");
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get this users_doctors pair by entered data: user_id = " + usersId + ", doctor_id = " + doctorId, e);
        }

        return result;
    }

    public Integer insertUserDoctorPair(User user, User doctor) {
        Integer result = null;
        try (Connection connection = getDataSource().getConnection();
            PreparedStatement statement = connection.prepareStatement("INSERT INTO users_doctors (user_id, doctor_id) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS)) {
            int count = 0;
            statement.setLong(++count, user.getId());
            statement.setLong(++count, doctor.getId());
            int updateCount = statement.executeUpdate();
            if (updateCount > 0) {
                ResultSet resultSet = statement.getGeneratedKeys();
                if (resultSet.next()) {
                    result = resultSet.getInt(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Cannot insert this user to this doctor: " + user.getId() + " => " + doctor.getId(), e);
        }

        return result;
    }

    @Override
    public List<String> getAllCategories() {
        List<String> categories = new ArrayList<>();

        try (Connection connection = getDataSource().getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM categories")) {
            while (resultSet.next()) {
                categories.add(resultSet.getString("name"));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get all categories", e);
        }

        return categories;
    }

    @Override
    public List<String> getAllRoles() {
        List<String> roles = new ArrayList<>();

        try (Connection connection = getDataSource().getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM roles")) {
            while (resultSet.next()) {
                roles.add(resultSet.getString("name"));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get all roles", e);
        }

        return roles;
    }

    @Override
    public boolean updateRoleAndCategory(User user) {
        boolean result = false;

        try (Connection connection = getDataSource().getConnection();
            PreparedStatement statement = connection.prepareStatement("UPDATE users SET role_id = ?, category_id = ? WHERE id = ?")) {
            int count = 0;
            statement.setLong(++count, UserDao.getInstance().getRoleId(user.getRole()));
            statement.setLong(++count, UserDao.getInstance().getCategoryId(user.getCategory()));
            statement.setLong(++count, user.getId());
            int countUpdates = statement.executeUpdate();
            if (countUpdates > 0) {
                result = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot update role and category in user: " + user.getId(), e);
        }

        return result;
    }

    @Override
    public boolean deleteAllDoctorPatients(User doctor) {
        boolean result = false;

        try (Connection connection = getDataSource().getConnection();
            PreparedStatement statement = connection.prepareStatement("DELETE FROM users_doctors WHERE doctor_id = ?")) {
            int count = 0;
            statement.setLong(++count, doctor.getId());
            int deletesCount = statement.executeUpdate();
            if (deletesCount > 0) {
                result = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot delete this doctor user pair with this doctor: " + doctor.getId(), e);
        }

        return result;
    }

    @Override
    public boolean deleteDoctorPair(User doctor, User user) {
        boolean result = false;

        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement("DELETE FROM users_doctors WHERE doctor_id = ? AND user_id = ?")) {
            int count = 0;
            statement.setLong(++count, doctor.getId());
            statement.setLong(++count, user.getId());
            int deletesCount = statement.executeUpdate();
            if (deletesCount > 0) {
                result = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot delete this doctor user pair: " + doctor.getId() + " => " + user.getId(), e);
        }

        return result;
    }

    @Override
    public Integer getRoleId(String role, Connection connection) {
        Integer result = null;

        try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM roles WHERE name = ?")) {
            int k = 0;
            statement.setString(++k, role);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getInt("id");
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get role with this name: " + role, e);
        }

        return result;
    }

    @Override
    public Integer getCategoryId(String category, Connection connection) {
        Integer result = null;

        try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM categories WHERE name = ?")) {
            int k = 0;
            statement.setString(++k, category);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getInt("id");
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get category with this name: " + category, e);
        }

        return result;
    }

    @Override
    public boolean insert(User object, Connection connection) {
        boolean result = false;

        try (PreparedStatement statement = connection.prepareStatement(INSERT_STATEMENT + " (?, ?, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS)) {
            int count = 0;
            statement.setString(++count, object.getName());
            statement.setString(++count, object.getSecondName());
            statement.setDate(++count, object.getBirthDay());
            statement.setString(++count, object.getPhone());
            statement.setString(++count, object.getEmail().getValue());
            statement.setString(++count, object.getPassword().getValue());
            statement.setLong(++count, getRoleId(object.getRole(), connection));
            statement.setLong(++count, getCategoryId(object.getCategory(), connection));

            int updateCount = statement.executeUpdate();
            if (updateCount > 0) {
                ResultSet resultSet = statement.getGeneratedKeys();
                if (resultSet.next()) {
                    object.setId(resultSet.getInt(1));
                    result = true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("The phone or email already exists", e);
        }

        return result;
    }

    @Override
    public List<User> getAll(Connection connection) {
        List<User> usersList = new ArrayList<>();

        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(GET_ALL_STATEMENT)) {
            while (resultSet.next()) {
                usersList.add(new User(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("second_name"),
                        resultSet.getDate("birth_day"),
                        Email.generateEmail(resultSet.getString("email")),
                        resultSet.getString("phone"),
                        Password.generatePassword(resultSet.getString("password"), true),
                        getRole(resultSet.getInt("role_id"), connection),
                        getCategory(resultSet.getInt("category_id"), connection),
                        resultSet.getTimestamp("created_at"),
                        resultSet.getTimestamp("updated_at")
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not get result from database");
        }

        return usersList;
    }

    @Override
    public User getById(int id, Connection connection) {
        User user = null;
        try (PreparedStatement statement = connection.prepareStatement(GET_ALL_STATEMENT + " WHERE id = ?")) {
            int k = 0;
            statement.setLong(++k, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                user = new User(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("second_name"),
                        resultSet.getDate("birth_day"),
                        Email.generateEmail(resultSet.getString("email")),
                        resultSet.getString("phone"),
                        Password.generatePassword(resultSet.getString("password"), true),
                        getRole(resultSet.getInt("role_id"), connection),
                        getCategory(resultSet.getInt("category_id"), connection),
                        resultSet.getTimestamp("created_at"),
                        resultSet.getTimestamp("updated_at")
                );
            }
        } catch (SQLException e) {
            throw new RuntimeException("Something went wrong when trying to find a user with specific id", e);
        }

        return user;
    }

    @Override
    public boolean update(User entity, Connection connection) {
        boolean result = false;
        try (PreparedStatement statement = connection.prepareStatement("UPDATE users SET name = ?, second_name = ?, birth_day = ?, phone = ?, email = ? WHERE id = ?")) {
            int k = 0;
            statement.setString(++k, entity.getName());
            statement.setString(++k, entity.getSecondName());
            statement.setDate(++k, entity.getBirthDay());
            statement.setString(++k, entity.getPhone());
            statement.setString(++k, entity.getEmail().getValue());
            statement.setLong(++k, entity.getId());
            int updateResult = statement.executeUpdate();
            if (updateResult > 0) {
                result = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not update user with id: " + entity.getId(), e);
        }

        return result;
    }

    @Override
    public boolean updatePassword(User user, Connection connection) {
        boolean result = false;
        try (PreparedStatement statement = connection.prepareStatement("UPDATE users SET password = ? WHERE id = ?")) {
            int k = 0;
            statement.setString(++k, user.getPassword().getValue());
            statement.setLong(++k, user.getId());
            int updateCount = statement.executeUpdate();
            if (updateCount > 0) {
                result = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not update user with id: " + user.getId(), e);
        }

        return result;
    }

    @Override
    public boolean delete(User entity, Connection connection) {
        boolean result = false;

        try (PreparedStatement statement = connection.prepareStatement("DELETE FROM users WHERE id = ?")) {
            int k = 0;
            statement.setLong(++k, entity.getId());
            int deleteCount = statement.executeUpdate();
            if (deleteCount > 0) {
                result = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not delete user", e);
        }

        return result;
    }

    @Override
    public String getCategory(int categoryId, Connection connection) {
        String result = null;

        try (PreparedStatement statement = connection.prepareStatement(GET_CATEGORY + " WHERE id = ?")) {
            int k = 0;
            statement.setLong(++k, categoryId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getString("name");
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not get user category", e);
        }

        return result;
    }

    @Override
    public String getRole(int roleId, Connection connection) {
        String result = null;

        try (PreparedStatement statement = connection.prepareStatement(GET_ROLE + " WHERE id = ?")) {
            int k = 0;
            statement.setLong(++k, roleId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getString("name");
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not get user role", e);
        }

        return result;
    }

    @Override
    public List<User> getUsersByRole(String role, Connection connection) {
        List<User> users = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(GET_USERS_BY_ROLE + " WHERE roles.name = ?")) {
            int k = 0;
            statement.setString(++k, role);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                users.add(new User(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("second_name"),
                        resultSet.getDate("birth_day"),
                        Email.generateEmail(resultSet.getString("email")),
                        resultSet.getString("phone"),
                        Password.generatePassword(resultSet.getString("password"), true),
                        getRole(resultSet.getInt("role_id")),
                        getCategory(resultSet.getInt("category_id")),
                        resultSet.getTimestamp("created_at"),
                        resultSet.getTimestamp("updated_at")
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not get users by roles", e);
        }

        return users;
    }

    @Override
    public List<User> getUsersByCategory(String category, Connection connection) {
        List<User> users = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(GET_USERS_BY_CATEGORY + " WHERE categories.name = ?")) {
            int k = 0;
            statement.setString(++k, category);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                users.add(new User(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("second_name"),
                        resultSet.getDate("birth_day"),
                        Email.generateEmail(resultSet.getString("email")),
                        resultSet.getString("phone"),
                        Password.generatePassword(resultSet.getString("password"), true),
                        getRole(resultSet.getInt("role_id")),
                        getCategory(resultSet.getInt("category_id")),
                        resultSet.getTimestamp("created_at"),
                        resultSet.getTimestamp("updated_at")
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not get users by categories", e);
        }

        return users;
    }

    @Override
    public List<User> getDoctorUsers(User doctor, Connection connection) {
        List<User> users = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(GET_ALL_STATEMENT + " JOIN users_doctors ON users_doctors.user_id = users.id WHERE users_doctors.doctor_id = ?")) {
            int k = 0;
            statement.setLong(++k, doctor.getId());
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                users.add(new User(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("second_name"),
                        resultSet.getDate("birth_day"),
                        Email.generateEmail(resultSet.getString("email")),
                        resultSet.getString("phone"),
                        Password.generatePassword(resultSet.getString("password"), true),
                        getRole(resultSet.getInt("role_id")),
                        getCategory(resultSet.getInt("category_id")),
                        resultSet.getTimestamp("created_at"),
                        resultSet.getTimestamp("updated_at")
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not get users by this doctor " + doctor.getId(), e);
        }

        return users;
    }

    @Override
    public List<User> getUserDoctors(User user, Connection connection) {
        List<User> users = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(GET_ALL_STATEMENT + " JOIN users_doctors ON users_doctors.doctor_id = users.id WHERE users_doctors.user_id = ?")) {
            int k = 0;
            statement.setLong(++k, user.getId());
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                users.add(new User(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("second_name"),
                        resultSet.getDate("birth_day"),
                        Email.generateEmail(resultSet.getString("email")),
                        resultSet.getString("phone"),
                        Password.generatePassword(resultSet.getString("password"), true),
                        getRole(resultSet.getInt("role_id"), connection),
                        getCategory(resultSet.getInt("category_id"), connection),
                        resultSet.getTimestamp("created_at"),
                        resultSet.getTimestamp("updated_at")
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not get doctors by this user " + user.getId(), e);
        }

        return users;
    }

    @Override
    public User getUserByEmail(Email email, Connection connection) {
        User user = null;
        try (PreparedStatement statement = connection.prepareStatement(GET_ALL_STATEMENT + " WHERE email = ?;")) {
            int k = 0;
            statement.setString(++k, email.getValue());
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                user = new User(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("second_name"),
                        resultSet.getDate("birth_day"),
                        Email.generateEmail(resultSet.getString("email")),
                        resultSet.getString("phone"),
                        Password.generatePassword(resultSet.getString("password"), true),
                        getRole(resultSet.getInt("role_id")),
                        getCategory(resultSet.getInt("category_id")),
                        resultSet.getTimestamp("created_at"),
                        resultSet.getTimestamp("updated_at")
                );
            }
        } catch (SQLException e) {
            throw new RuntimeException("This email does not exist: " + email.getValue(), e);
        }

        return user;
    }

    @Override
    public User getUserByHospitalCard(HospitalCard hospitalCard, Connection connection) {
        User user = null;
        try (PreparedStatement statement = connection.prepareStatement(GET_USER_BY_HOSPITAL_CARD + " WHERE hospital_cards.id = ?")) {
            int k = 0;
            statement.setLong(++k, hospitalCard.getId());
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                user = new User(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("second_name"),
                        resultSet.getDate("birth_day"),
                        Email.generateEmail(resultSet.getString("email")),
                        resultSet.getString("phone"),
                        Password.generatePassword(resultSet.getString("password"), true),
                        getRole(resultSet.getInt("role_id")),
                        getCategory(resultSet.getInt("category_id")),
                        resultSet.getTimestamp("created_at"),
                        resultSet.getTimestamp("updated_at")
                );
            }
        } catch (SQLException e) {
            throw new RuntimeException("Something went wrong when trying to find a user with this hospital_card: " + hospitalCard.getName(), e);
        }

        return user;
    }

    @Override
    public Map<String, User> getUserDoctorPair(int id, Connection connection) {
        Map<String, User> userDoctorPair = new HashMap<>();

        try (PreparedStatement statement = connection.prepareStatement(GET_USER_DOCTOR_PAIR + " WHERE id = ?")) {
            int k = 0;
            statement.setLong(++k, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                userDoctorPair.put(USER_PAIR_NAME, UserDao.getInstance().getById(resultSet.getInt("user_id")));
                userDoctorPair.put(DOCTOR_PAIR_NAME, UserDao.getInstance().getById(resultSet.getInt("doctor_id")));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get this users_doctors pair: " + id, e);
        }

        return userDoctorPair;
    }

    @Override
    public Integer getUserDoctorIdByPair(int usersId, int doctorId, Connection connection) {
        Integer result = null;

        try (PreparedStatement statement = connection.prepareStatement(GET_USER_DOCTOR_PAIR + " WHERE user_id = ? AND doctor_id = ?")) {
            int k = 0;
            statement.setLong(++k, usersId);
            statement.setLong(++k, doctorId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getInt("id");
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get this users_doctors pair by entered data: user_id = " + usersId + ", doctor_id = " + doctorId, e);
        }

        return result;
    }

    public Integer insertUserDoctorPair(User user, User doctor, Connection connection) {
        Integer result = null;
        try (PreparedStatement statement = connection.prepareStatement("INSERT INTO users_doctors (user_id, doctor_id) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS)) {
            int count = 0;
            statement.setLong(++count, user.getId());
            statement.setLong(++count, doctor.getId());
            int updateCount = statement.executeUpdate();
            if (updateCount > 0) {
                ResultSet resultSet = statement.getGeneratedKeys();
                if (resultSet.next()) {
                    result = resultSet.getInt(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Cannot insert this user to this doctor: " + user.getId() + " => " + doctor.getId(), e);
        }

        return result;
    }

    @Override
    public List<String> getAllCategories(Connection connection) {
        List<String> categories = new ArrayList<>();

        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM categories")) {
            while (resultSet.next()) {
                categories.add(resultSet.getString("name"));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get all categories", e);
        }

        return categories;
    }

    @Override
    public List<String> getAllRoles(Connection connection) {
        List<String> roles = new ArrayList<>();

        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM roles")) {
            while (resultSet.next()) {
                roles.add(resultSet.getString("name"));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get all roles", e);
        }

        return roles;
    }

    @Override
    public boolean updateRoleAndCategory(User user, Connection connection) {
        boolean result = false;

        try (PreparedStatement statement = connection.prepareStatement("UPDATE users SET role_id = ?, category_id = ? WHERE id = ?")) {
            int count = 0;
            statement.setLong(++count, UserDao.getInstance().getRoleId(user.getRole(), connection));
            statement.setLong(++count, UserDao.getInstance().getCategoryId(user.getCategory()));
            statement.setLong(++count, user.getId());
            int countUpdates = statement.executeUpdate();
            if (countUpdates > 0) {
                result = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot update role and category in user: " + user.getId(), e);
        }

        return result;
    }

    @Override
    public boolean deleteAllDoctorPatients(User doctor, Connection connection) {
        boolean result = false;

        try (PreparedStatement statement = connection.prepareStatement("DELETE FROM users_doctors WHERE doctor_id = ?")) {
            int count = 0;
            statement.setLong(++count, doctor.getId());
            int deletesCount = statement.executeUpdate();
            if (deletesCount > 0) {
                result = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot delete this doctor user pair with this doctor: " + doctor.getId(), e);
        }

        return result;
    }

    @Override
    public boolean deleteDoctorPair(User doctor, User user, Connection connection) {
        boolean result = false;

        try (PreparedStatement statement = connection.prepareStatement("DELETE FROM users_doctors WHERE doctor_id = ? AND user_id = ?")) {
            int count = 0;
            statement.setLong(++count, doctor.getId());
            statement.setLong(++count, user.getId());
            int deletesCount = statement.executeUpdate();
            if (deletesCount > 0) {
                result = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot delete this doctor user pair: " + doctor.getId() + " => " + user.getId(), e);
        }

        return result;
    }
}
