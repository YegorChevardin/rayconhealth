package com.rayconhealth.rayconhealth.database;

import com.rayconhealth.rayconhealth.database.interfaces.DiagnosDaoInterface;
import com.rayconhealth.rayconhealth.models.Diagnos;
import com.rayconhealth.rayconhealth.models.HospitalCard;
import com.rayconhealth.rayconhealth.models.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.rayconhealth.rayconhealth.database.pereferences.DatabasePreferences.getDataSource;

public class DiagnosDao implements DiagnosDaoInterface {
    private static final String GET_ALL = "SELECT * FROM diagnosis";
    private static final String GET_DIAGNOS_STATUSES = "SELECT * FROM diagnos_statuses";
    public static final String INSERT_STATEMENT = "INSERT INTO diagnosis (name, diagnos_status_id, doctor_id, hospital_card_id) VALUES";
    private static DiagnosDao instance = null;

    private DiagnosDao() {}
    public static DiagnosDao getInstance() {
        if (instance == null) {
            instance = new DiagnosDao();
        }
        return instance;
    }
    @Override
    public boolean insert(Diagnos object) {
        boolean result = false;

        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(INSERT_STATEMENT + " (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);) {
            int count = 0;
            statement.setString(++count, object.getName());
            statement.setInt(++count, getDiagnosStatusId(object.getDiagnosStatus()));
            statement.setInt(++count, object.getDoctor().getId());
            statement.setInt(++count, object.getHospitalCard().getId());

            int updateCount = statement.executeUpdate();
            if (updateCount > 0) {
                ResultSet resultSet = statement.getGeneratedKeys();
                if (resultSet.next()) {
                    object.setId(resultSet.getInt(1));
                    result = true;
                }
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException("Inserted data is not valid", e);
        }

        return result;
    }

    @Override
    public List<Diagnos> getAll() {
        List<Diagnos> diagnosis = new ArrayList<>();

        try (Connection connection = getDataSource().getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(GET_ALL)) {
            while (resultSet.next()) {
                diagnosis.add(new Diagnos(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        getDiagnosStatusName(resultSet.getInt("diagnos_status_id")),
                        UserDao.getInstance().getById(resultSet.getInt("doctor_id")),
                        HospitalCardDao.instance.getById(resultSet.getInt("hospital_card_id"))
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get all statements from database", e);
        }

        return diagnosis;
    }

    @Override
    public Diagnos getById(int id) {
        Diagnos result = null;

        try (Connection connection = getDataSource().getConnection();
            PreparedStatement statement = connection.prepareStatement(GET_ALL + " WHERE id = ?")) {
            int count = 0;
            statement.setLong(++count, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = new Diagnos(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        getDiagnosStatusName(resultSet.getInt("diagnos_status_id")),
                        UserDao.getInstance().getById(resultSet.getInt("doctor_id")),
                        HospitalCardDao.instance.getById(resultSet.getInt("hospital_card_id"))
                );
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException("Cannot get diagnos by this id: " + id, e);
        }

        return result;
    }

    @Override
    public boolean update(Diagnos entity) {
        boolean result = false;

        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement("UPDATE diagnosis SET name = ?, diagnos_status_id = ?, doctor_id = ?, hospital_card_id = ? WHERE id = ?")) {
            int count = 0;
            statement.setString(++count, entity.getName());
            statement.setLong(++count, getDiagnosStatusId(entity.getDiagnosStatus()));
            statement.setLong(++count, entity.getDoctor().getId());
            statement.setLong(++count, entity.getHospitalCard().getId());
            statement.setLong(++count, entity.getId());
            int resultUpdate = statement.executeUpdate();
            if (resultUpdate > 0) {
                result = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot update diagnos with this id: " + entity.getId(), e);
        }

        return result;
    }

    @Override
    public boolean delete(Diagnos entity) {
        boolean result = false;

        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement("DELETE FROM diagnosis WHERE id = ?")) {
            int k = 0;
            statement.setLong(++k, entity.getId());
            int deleteCount = statement.executeUpdate();
            if (deleteCount > 0) {
                result = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not delete diagnos with this id:" + entity.getId(), e);
        }

        return result;
    }

    @Override
    public List<Diagnos> getDiagnosisByName(String name) {
        List<Diagnos> diagnosis = new ArrayList<>();

        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(GET_ALL + " WHERE name = ?")) {
            int count = 0;
            statement.setString(++count, name);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                diagnosis.add(new Diagnos(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        getDiagnosStatusName(resultSet.getInt("diagnos_status_id")),
                        UserDao.getInstance().getById(resultSet.getInt("doctor_id")),
                        HospitalCardDao.instance.getById(resultSet.getInt("hospital_card_id"))
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get diagnosis by this name from database " + name, e);
        }

        return diagnosis;
    }

    @Override
    public List<Diagnos> getDiagnosByDoctor(User doctor) {
        List<Diagnos> diagnosis = new ArrayList<>();

        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(GET_ALL + " WHERE doctor_id = ?")) {
            int count = 0;
            statement.setInt(++count, doctor.getId());
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                diagnosis.add(new Diagnos(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        getDiagnosStatusName(resultSet.getInt("diagnos_status_id")),
                        UserDao.getInstance().getById(resultSet.getInt("doctor_id")),
                        HospitalCardDao.instance.getById(resultSet.getInt("hospital_card_id"))
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get diagnosis by this doctor from database " + doctor.getId(), e);
        }

        return diagnosis;
    }

    @Override
    public List<Diagnos> getDiagnosByHospitalCard(HospitalCard hospitalCard) {
        List<Diagnos> diagnosis = new ArrayList<>();

        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(GET_ALL + " WHERE hospital_card_id = ?")) {
            int count = 0;
            statement.setInt(++count, hospitalCard.getId());
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                diagnosis.add(new Diagnos(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        getDiagnosStatusName(resultSet.getInt("diagnos_status_id")),
                        UserDao.getInstance().getById(resultSet.getInt("doctor_id")),
                        HospitalCardDao.instance.getById(resultSet.getInt("hospital_card_id"))
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get diagnosis by this hospital card from database " + hospitalCard.getId(), e);
        }

        return diagnosis;
    }

    @Override
    public Integer getDiagnosStatusId(String name) {
        Integer result = null;

        try (Connection connection = getDataSource().getConnection();
            PreparedStatement statement = connection.prepareStatement(GET_DIAGNOS_STATUSES + " WHERE status = ?")) {
            int count = 0;
            statement.setString(++count, name);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getInt("id");
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get status id with this name: " + name, e);
        }

        return result;
    }

    @Override
    public List<Diagnos> getDiagnosByStatus(String status) {
        List<Diagnos> diagnosis = new ArrayList<>();

        try (Connection connection = getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(GET_ALL + " WHERE diagnos_status_id = ?")) {
            int count = 0;
            statement.setInt(++count, getDiagnosStatusId(status));
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                diagnosis.add(new Diagnos(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        getDiagnosStatusName(resultSet.getInt("diagnos_status_id")),
                        UserDao.getInstance().getById(resultSet.getInt("doctor_id")),
                        HospitalCardDao.instance.getById(resultSet.getInt("hospital_card_id"))
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get diagnosis by this status from database " + status, e);
        }

        return diagnosis;
    }

    @Override
    public String getDiagnosStatusName(int id) {
        String result = null;

        try (Connection connection = getDataSource().getConnection();
            PreparedStatement statement = connection.prepareStatement(GET_DIAGNOS_STATUSES + " WHERE id = ?")) {
            int count = 0;
            statement.setLong(++count, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getString("status");
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get Status Name with this id: " + id, e);
        }

        return result;
    }

    @Override
    public List<String> getAllStatuses() {
        List<String> statuses = new ArrayList<>();

        try (Connection connection = getDataSource().getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM diagnos_statuses")) {
            while (resultSet.next()) {
                statuses.add(resultSet.getString("status"));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get all statuses");
        }

        return statuses;
    }

    @Override
    public boolean insert(Diagnos object, Connection connection) {
        boolean result = false;

        try (PreparedStatement statement = connection.prepareStatement(INSERT_STATEMENT + " (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);) {
            int count = 0;
            statement.setString(++count, object.getName());
            statement.setInt(++count, getDiagnosStatusId(object.getDiagnosStatus()));
            statement.setInt(++count, object.getDoctor().getId());
            statement.setInt(++count, object.getHospitalCard().getId());

            int updateCount = statement.executeUpdate();
            if (updateCount > 0) {
                ResultSet resultSet = statement.getGeneratedKeys();
                if (resultSet.next()) {
                    object.setId(resultSet.getInt(1));
                    result = true;
                }
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException("Inserted data is not valid", e);
        }

        return result;
    }

    @Override
    public List<Diagnos> getAll(Connection connection) {
        List<Diagnos> diagnosis = new ArrayList<>();

        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(GET_ALL)) {
            while (resultSet.next()) {
                diagnosis.add(new Diagnos(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        getDiagnosStatusName(resultSet.getInt("diagnos_status_id")),
                        UserDao.getInstance().getById(resultSet.getInt("doctor_id")),
                        HospitalCardDao.instance.getById(resultSet.getInt("hospital_card_id"))
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get all statements from database", e);
        }

        return diagnosis;
    }

    @Override
    public Diagnos getById(int id, Connection connection) {
        Diagnos result = null;

        try (PreparedStatement statement = connection.prepareStatement(GET_ALL + " WHERE id = ?")) {
            int count = 0;
            statement.setLong(++count, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = new Diagnos(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        getDiagnosStatusName(resultSet.getInt("diagnos_status_id")),
                        UserDao.getInstance().getById(resultSet.getInt("doctor_id")),
                        HospitalCardDao.instance.getById(resultSet.getInt("hospital_card_id"))
                );
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException("Cannot get diagnos by this id: " + id, e);
        }

        return result;
    }

    @Override
    public boolean update(Diagnos entity, Connection connection) {
        boolean result = false;

        try (PreparedStatement statement = connection.prepareStatement("UPDATE diagnosis SET name = ?, diagnos_status_id = ?, doctor_id = ?, hospital_card_id = ? WHERE id = ?")) {
            int count = 0;
            statement.setString(++count, entity.getName());
            statement.setLong(++count, getDiagnosStatusId(entity.getDiagnosStatus()));
            statement.setLong(++count, entity.getDoctor().getId());
            statement.setLong(++count, entity.getHospitalCard().getId());
            statement.setLong(++count, entity.getId());
            int resultUpdate = statement.executeUpdate();
            if (resultUpdate > 0) {
                result = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot update diagnos with this id: " + entity.getId(), e);
        }

        return result;
    }

    @Override
    public boolean delete(Diagnos entity, Connection connection) {
        boolean result = false;

        try (PreparedStatement statement = connection.prepareStatement("DELETE FROM diagnosis WHERE id = ?")) {
            int k = 0;
            statement.setLong(++k, entity.getId());
            int deleteCount = statement.executeUpdate();
            if (deleteCount > 0) {
                result = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not delete diagnos with this id:" + entity.getId(), e);
        }

        return result;
    }

    @Override
    public List<Diagnos> getDiagnosisByName(String name, Connection connection) {
        List<Diagnos> diagnosis = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(GET_ALL + " WHERE name = ?")) {
            int count = 0;
            statement.setString(++count, name);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                diagnosis.add(new Diagnos(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        getDiagnosStatusName(resultSet.getInt("diagnos_status_id")),
                        UserDao.getInstance().getById(resultSet.getInt("doctor_id")),
                        HospitalCardDao.instance.getById(resultSet.getInt("hospital_card_id"))
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get diagnosis by this name from database " + name, e);
        }

        return diagnosis;
    }

    @Override
    public List<Diagnos> getDiagnosByDoctor(User doctor, Connection connection) {
        List<Diagnos> diagnosis = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(GET_ALL + " WHERE doctor_id = ?")) {
            int count = 0;
            statement.setInt(++count, doctor.getId());
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                diagnosis.add(new Diagnos(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        getDiagnosStatusName(resultSet.getInt("diagnos_status_id")),
                        UserDao.getInstance().getById(resultSet.getInt("doctor_id")),
                        HospitalCardDao.instance.getById(resultSet.getInt("hospital_card_id"))
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get diagnosis by this doctor from database " + doctor.getId(), e);
        }

        return diagnosis;
    }

    @Override
    public List<Diagnos> getDiagnosByHospitalCard(HospitalCard hospitalCard, Connection connection) {
        List<Diagnos> diagnosis = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(GET_ALL + " WHERE hospital_card_id = ?")) {
            int count = 0;
            statement.setInt(++count, hospitalCard.getId());
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                diagnosis.add(new Diagnos(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        getDiagnosStatusName(resultSet.getInt("diagnos_status_id")),
                        UserDao.getInstance().getById(resultSet.getInt("doctor_id")),
                        HospitalCardDao.instance.getById(resultSet.getInt("hospital_card_id"))
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get diagnosis by this hospital card from database " + hospitalCard.getId(), e);
        }

        return diagnosis;
    }

    @Override
    public Integer getDiagnosStatusId(String name, Connection connection) {
        Integer result = null;

        try (PreparedStatement statement = connection.prepareStatement(GET_DIAGNOS_STATUSES + " WHERE status = ?")) {
            int count = 0;
            statement.setString(++count, name);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getInt("id");
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get status id with this name: " + name, e);
        }

        return result;
    }

    @Override
    public List<Diagnos> getDiagnosByStatus(String status, Connection connection) {
        List<Diagnos> diagnosis = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(GET_ALL + " WHERE diagnos_status_id = ?")) {
            int count = 0;
            statement.setInt(++count, getDiagnosStatusId(status));
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                diagnosis.add(new Diagnos(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        getDiagnosStatusName(resultSet.getInt("diagnos_status_id")),
                        UserDao.getInstance().getById(resultSet.getInt("doctor_id")),
                        HospitalCardDao.instance.getById(resultSet.getInt("hospital_card_id"))
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get diagnosis by this status from database " + status, e);
        }

        return diagnosis;
    }

    @Override
    public String getDiagnosStatusName(int id, Connection connection) {
        String result = null;

        try (PreparedStatement statement = connection.prepareStatement(GET_DIAGNOS_STATUSES + " WHERE id = ?")) {
            int count = 0;
            statement.setLong(++count, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getString("status");
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get Status Name with this id: " + id, e);
        }

        return result;
    }

    @Override
    public List<String> getAllStatuses(Connection connection) {
        List<String> statuses = new ArrayList<>();

        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM diagnos_statuses")) {
            while (resultSet.next()) {
                statuses.add(resultSet.getString("status"));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get all statuses");
        }

        return statuses;
    }
}
