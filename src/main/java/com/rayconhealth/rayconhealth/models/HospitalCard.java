package com.rayconhealth.rayconhealth.models;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class HospitalCard {
    private static final int DEFAULT_ID = 0;
    private static final Timestamp DEFAULT_CREATED_AT = Timestamp.valueOf(LocalDateTime.now());
    private int id;
    private String name;
    private Timestamp createdAt;
    private final User user;

    public HospitalCard(String name, User user) {
        this(DEFAULT_ID, name, DEFAULT_CREATED_AT, user);
    }

    public HospitalCard(int id, String name, Timestamp createdAt, User user) {
        this.id = id;
        this.name = name;
        this.createdAt = createdAt;
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public User getUser() {
        return user;
    }
}
