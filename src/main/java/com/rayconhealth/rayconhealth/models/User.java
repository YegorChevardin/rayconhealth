package com.rayconhealth.rayconhealth.models;

import com.rayconhealth.rayconhealth.models.classes.Email;
import com.rayconhealth.rayconhealth.models.classes.Password;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDateTime;

public class User {
    private static final int DEFAULT_ID = 0;
    private int id;
    private String name;
    private String secondName;
    private Date birthDay;
    private Email email;
    private String phone;
    private Password password;
    private String role;
    private String category;
    private final Timestamp createdAt;
    private Timestamp updatedAt;
    private Integer numberOfPatients = null;

    public User(String name, String secondName, Date birthDay, Email email, String phone, Password password, String role, String category) {
        this(DEFAULT_ID, name, secondName, birthDay, email, phone, password, role, category, Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));
    }

    public User(int id, String name, String secondName, Date birthDay, Email email, String phone, Password password, String role, String category, Timestamp createdAt, Timestamp updatedAt) {
        this.id = id;
        this.name = name;
        this.secondName = secondName;
        this.birthDay = birthDay;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.role = role;
        this.category = category;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }

    public Email getEmail() {
        return email;
    }

    public void setEmail(Email email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setPassword(Password password) {
        this.password = password;
    }

    public Password getPassword() {
        return password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getNumberOfPatients() {
        return numberOfPatients;
    }

    public void setNumberOfPatients(Integer numberOfPatients) {
        this.numberOfPatients = numberOfPatients;
    }
    public boolean equalsUser(User user) {
        return this.getEmail().getValue().equals(user.getEmail().getValue());
    }
}
