package com.rayconhealth.rayconhealth.models.classes;

public class Password {
    private String value;

    private Password(String value) {
        this.value = value;
    }

    public static Password generatePassword(String value, boolean hashed) {
        if (!hashed) {
            value = new PasswordHashing().hash(value);
        }
        return new Password(value);
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
