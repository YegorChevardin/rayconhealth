package com.rayconhealth.rayconhealth.models.classes;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Email {
    private static final String regexPattern = "^(.+)@(.+)$";
    private String value;

    private Email(String value) {
        this.value = value;
    }

    public static boolean emailMatcher(String value) {
        Pattern pattern = Pattern.compile(regexPattern);
        Matcher matcher = pattern.matcher(value);
        return matcher.matches();
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        if (emailMatcher(value)) {
            this.value = value;
        } else {
            throw new IllegalArgumentException("Inserted value is not email!");
        }
    }

    public static Email generateEmail(String value) {
        if (emailMatcher(value)) {
            return new Email(value);
        } else {
            throw new IllegalArgumentException("Inserted value is not email!");
        }
    }
}
