package com.rayconhealth.rayconhealth.models.classes;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class Log {
    private static final int DEFAULT_ID = 0;
    private int id;
    private String text;
    private Timestamp sendAt;

    public Log(int id, String text, Timestamp sendAt) {
        this.id = id;
        this.text = text;
        this.sendAt = sendAt;
    }

    public Log(String text) {
        this(DEFAULT_ID, text, Timestamp.valueOf(LocalDateTime.now()));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Timestamp getSendAt() {
        return sendAt;
    }

    public void setSendAt(Timestamp sendAt) {
        this.sendAt = sendAt;
    }
}
