package com.rayconhealth.rayconhealth.models;

public class Enroll {
    private static final int DEFAULT_ID = 0;
    private int id;
    private User user;
    private String text;

    public Enroll(User user, String text) {
        this(DEFAULT_ID, user, text);
    }

    public Enroll(int id, User user, String text) {
        this.id = id;
        this.user = user;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
