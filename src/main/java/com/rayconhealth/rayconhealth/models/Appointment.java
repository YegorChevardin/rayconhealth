package com.rayconhealth.rayconhealth.models;

import java.sql.Date;

public class Appointment {
    private static final int DEFAULT_ID = 0;
    private int id;
    private Date meetingDate;
    private User doctor;
    private User user;
    private String appointmentType;
    private HospitalCard hospitalCard;

    public Appointment(Date meetingDate, User doctor, User user, String appointmentType, HospitalCard hospitalCard) {
        this(DEFAULT_ID, meetingDate, doctor, user, appointmentType, hospitalCard);
    }

    public Appointment(int id, Date meetingDate, User doctor, User user, String appointmentType, HospitalCard hospitalCard) {
        this.id = id;
        this.meetingDate = meetingDate;
        this.doctor = doctor;
        this.user = user;
        this.appointmentType = appointmentType;
        this.hospitalCard = hospitalCard;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getMeetingDate() {
        return meetingDate;
    }

    public void setMeetingDate(Date meetingDate) {
        this.meetingDate = meetingDate;
    }

    public User getDoctor() {
        return doctor;
    }

    public void setDoctor(User doctor) {
        this.doctor = doctor;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getAppointmentType() {
        return appointmentType;
    }

    public void setAppointmentType(String appointmentType) {
        this.appointmentType = appointmentType;
    }

    public HospitalCard getHospitalCard() {
        return hospitalCard;
    }

    public void setHospitalCard(HospitalCard hospitalCard) {
        this.hospitalCard = hospitalCard;
    }
}
