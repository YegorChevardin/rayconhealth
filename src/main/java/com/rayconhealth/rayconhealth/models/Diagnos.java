package com.rayconhealth.rayconhealth.models;

public class Diagnos {
    private static final int DEFAULT_ID = 0;
    private int id;
    private String name;
    private String diagnosStatus;
    private final User doctor;
    private HospitalCard hospitalCard;

    public Diagnos(String name, String diagnosStatus, User doctor, HospitalCard hospitalCard) {
        this(DEFAULT_ID, name, diagnosStatus, doctor, hospitalCard);
    }

    public Diagnos(int id, String name, String diagnosStatus, User doctor, HospitalCard hospitalCard) {
        this.id = id;
        this.name = name;
        this.diagnosStatus = diagnosStatus;
        this.doctor = doctor;
        this.hospitalCard = hospitalCard;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDiagnosStatus() {
        return diagnosStatus;
    }

    public void setDiagnosStatus(String diagnosStatus) {
        this.diagnosStatus = diagnosStatus;
    }

    public User getDoctor() {
        return doctor;
    }

    public HospitalCard getHospitalCard() {
        return hospitalCard;
    }

    public void setHospitalCard(HospitalCard hospitalCard) {
        this.hospitalCard = hospitalCard;
    }
}
