package com.rayconhealth.rayconhealth.facades;

import com.rayconhealth.rayconhealth.models.classes.Email;
import com.rayconhealth.rayconhealth.models.classes.Password;
import jakarta.servlet.http.HttpServletRequest;

import java.sql.Date;
import java.time.LocalDate;

public class DataValidationFacade {
    private DataValidationFacade() {}

    public static boolean validateRegistration(HttpServletRequest request) {
        String name = request.getParameter("name");
        String secondName = request.getParameter("second-name");
        String phone = request.getParameter("phone");
        String[] birthDayDate = request.getParameter("birth-day").split("-");
        Date birthDay = new Date(
                Integer.parseInt(birthDayDate[0]) - 1900,
                Integer.parseInt(birthDayDate[1]) - 1,
                Integer.parseInt(birthDayDate[2])
        );
        return ((name.length() <= 20 && name.length() > 1) && (secondName.length() <= 45) && (phone.length() < 45) && Date.valueOf(LocalDate.now()).after(birthDay));
    }

    public static boolean validateDiagnos(HttpServletRequest request) {
        String diagnos = request.getParameter("name");
        return diagnos.length() <= 45 && diagnos.length() > 5;
    }

    public static boolean validateEnroll(HttpServletRequest request) {
        String text = request.getParameter("pain-description");
        return text.length() <= 500 && text.length() > 10;
    }

    public static boolean validateMeeting(HttpServletRequest request) {
        String[] dateString = request.getParameter("date").split("-");
        Date date = new Date(
                Integer.parseInt(dateString[0]) - 1900,
                Integer.parseInt(dateString[1]) - 1,
                Integer.parseInt(dateString[2])
        );
        return Date.valueOf(LocalDate.now()).before(date);
    }
}
