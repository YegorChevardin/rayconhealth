package com.rayconhealth.rayconhealth.facades;

import com.rayconhealth.rayconhealth.database.LogDao;
import com.rayconhealth.rayconhealth.models.classes.Log;
import org.apache.logging.log4j.*;

public class LoggingFacade {
    private static final Logger logger = LogManager.getLogger(LoggingFacade.class.getName());
    private LoggingFacade() {}

    public static void makeLogInfo(String message) {
        logger.info(message);
    }

    public static void makeLogError(String message) {
        logger.error(message);
    }

    public static void makeLogFatal(String message) {
        logger.fatal(message);
    }

    public static void makeLogEvent(String message) {
        makeLogInfo(message);
        LogDao.getInstance().insert(new Log(message));
    }
}
