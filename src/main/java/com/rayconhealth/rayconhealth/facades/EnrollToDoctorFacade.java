package com.rayconhealth.rayconhealth.facades;

import com.rayconhealth.rayconhealth.database.UserDao;
import com.rayconhealth.rayconhealth.models.User;

public class EnrollToDoctorFacade {
    private EnrollToDoctorFacade() {}

    public static boolean isPatientExists(Integer doctorId, Integer userId) {
        boolean result = false;

        Integer pairId = UserDao.getInstance().getUserDoctorIdByPair(userId, doctorId);

        if (pairId != null) {
            result = true;
        }

        return result;
    }

    public static boolean enrollUserToDoctor(User user, User doctor) {
        Integer generated = null;

        if (!isPatientExists(doctor.getId(), user.getId())) {
            generated = UserDao.getInstance().insertUserDoctorPair(user, doctor);
        }

        return generated != null;
    }

    public static boolean deleteUserDoctorPairIfExists(User doctor, User user) {
        return EnrollToDoctorFacade.isPatientExists(doctor.getId(), user.getId()) && UserDao.getInstance().deleteDoctorPair(doctor, user);
    }
}
