package com.rayconhealth.rayconhealth.facades;

import com.rayconhealth.rayconhealth.database.HospitalCardDao;
import com.rayconhealth.rayconhealth.database.UserDao;
import com.rayconhealth.rayconhealth.models.HospitalCard;
import com.rayconhealth.rayconhealth.models.User;
import com.rayconhealth.rayconhealth.models.classes.PasswordHashing;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.sql.SQLException;

public class AuthenticationFacade {
    private static final String ADMIN_CHECKED_VALUE = "admin-checked";
    private AuthenticationFacade() {}

    public static void register(HttpServletRequest request, User user) throws SQLException {
        if (UserDao.getInstance().insert(user)) {
            if (createHospitalCard(user)) {
                request.getSession().setAttribute("user", user);
            } else {
                throw new SQLException("Cannot create your hospital card. Please, call administrator to fix this problem");
            }
        } else {
            throw new SQLException("User with this email or phone is already exists");
        }
    }

    public static void adminRegister(User user) throws IOException {
        try {
            if (UserDao.getInstance().insert(user)) {
                if (!createHospitalCard(user)) {
                    throw new SQLException("Cannot create user's hospital card.");
                }
            } else {
                throw new SQLException("User with this email or phone is already exists");
            }
        } catch (SQLException e) {
            throw new IOException(e.getMessage());
        }
    }

    public static void login(HttpServletRequest request, User user) throws SQLException {
        String password = request.getParameter("password");
        if (passwordCheck(password, user)) {
            request.getSession().setAttribute("user", user);
        } else {
            throw new SQLException("Wrong password");
        }
    }

    public static boolean passwordCheck(String password, User user) {
        return new PasswordHashing().authenticate(password, user.getPassword().getValue());
    }

    public static void logout(HttpServletRequest request) throws ServletException {
        HttpSession session = request.getSession();
        if (session.getAttribute("user") != null) {
            session.removeAttribute("user");
        } else {
            throw new ServletException();
        }
    }

    private static boolean createHospitalCard(User user) {
        return HospitalCardDao.getInstance().insert(new HospitalCard(user.getName() + "'s card", user));
    }

    public static boolean checkAuth(HttpServletRequest request) {
        return request.getSession().getAttribute("user") != null;
    }
}
