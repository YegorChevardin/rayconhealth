package com.rayconhealth.rayconhealth.servlets;

import com.rayconhealth.rayconhealth.controllers.EnrollController;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;

@WebServlet(name = "EnrollServlet", value = "/enroll")
public class EnrollServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        EnrollController.getInstance().index(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        EnrollController.getInstance().enrollUser(request, response);
    }
}
