package com.rayconhealth.rayconhealth.servlets;

import com.rayconhealth.rayconhealth.controllers.LogController;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;

@WebServlet(name = "LogEventServlet", value = {"/log", "/delete-log"})
public class LogEventServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String uri = request.getRequestURI();
        if (uri.equals(request.getContextPath() + "/log")) {
            LogController.getInstance().index(request, response);
        } else {
            LogController.getInstance().deleteLog(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
