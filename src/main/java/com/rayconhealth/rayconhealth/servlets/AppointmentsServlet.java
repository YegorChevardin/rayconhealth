package com.rayconhealth.rayconhealth.servlets;

import com.rayconhealth.rayconhealth.controllers.AppointmentsController;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;

@WebServlet(name = "AppointmentsServlet", value = {"/appointments", "/doctor-appointments"})
public class AppointmentsServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doAction(request, response);
    }

    private void doAction(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String uri = request.getRequestURI();

        if (uri.equals(request.getContextPath() + "/appointments")) {
            AppointmentsController.getInstance().index(request, response);
        } else if (uri.equals(request.getContextPath() + "/doctor-appointments")) {
            AppointmentsController.getInstance().doctorAppointments(request, response);
        } else {
            response.sendError(404);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doAction(req, resp);
    }
}
