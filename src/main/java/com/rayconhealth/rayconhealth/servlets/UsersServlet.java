package com.rayconhealth.rayconhealth.servlets;

import com.rayconhealth.rayconhealth.controllers.UsersListController;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;

@WebServlet(name = "UsersServlet", value = {"/users-list", "/delete-patient", "/delete-doctor", "/admins-list", "/doctors-list", "/my-doctors", "/patients-list"})
public class UsersServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String uri = request.getRequestURI();
        if (uri.equals(request.getContextPath() + "/users-list")) {
            UsersListController.getInstance().users(request, response);
        } else if (uri.equals(request.getContextPath() + "/delete-patient")){
            UsersListController.getInstance().deletePatient(request, response);
        } else if (uri.equals(request.getContextPath() + "/delete-doctor")) {
            UsersListController.getInstance().deleteDoctor(request, response);
        } else if (uri.equals(request.getContextPath() + "/admins-list")) {
            UsersListController.getInstance().admins(request, response);
        } else if (uri.equals(request.getContextPath() + "/doctors-list")) {
            UsersListController.getInstance().doctors(request, response);
        } else if (uri.equals(request.getContextPath() + "/my-doctors")) {
            UsersListController.getInstance().myDoctors(request, response);
        } else if (uri.equals(request.getContextPath() + "/patients-list")) {
            UsersListController.getInstance().patients(request, response);
        } else {
            response.sendError(404);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String uri = request.getRequestURI();
        if (uri.equals(request.getContextPath() + "/users-list")) {
            UsersListController.getInstance().adminRegister(request, response);
        } else {
            response.sendError(404);
        }
    }
}
