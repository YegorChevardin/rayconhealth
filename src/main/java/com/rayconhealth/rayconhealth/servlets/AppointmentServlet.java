package com.rayconhealth.rayconhealth.servlets;

import com.rayconhealth.rayconhealth.controllers.AppointmentController;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;

@WebServlet(name = "AppointmentServlet", value = {"/appointment", "/delete-appointment"})
public class AppointmentServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String uri = request.getRequestURI();
        if (uri.equals(request.getContextPath() + "/appointment")) {
            AppointmentController.getInstance().index(request, response);
        } else {
            AppointmentController.getInstance().deleteAppointment(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String uri = request.getRequestURI();
        if (uri.equals(request.getContextPath() + "/appointment")) {
            AppointmentController.getInstance().createAppointment(request, response);
        } else {
            response.sendError(404);
        }
    }
}
