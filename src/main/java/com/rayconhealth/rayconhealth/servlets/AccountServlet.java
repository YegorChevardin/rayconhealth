package com.rayconhealth.rayconhealth.servlets;

import com.rayconhealth.rayconhealth.controllers.AccountController;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;

@WebServlet(name = "AccountServlet", value = {"/account", "/edit-account"})
public class AccountServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String uri = request.getRequestURI();
        if (uri.equals(request.getContextPath() + "/account")) {
            AccountController.getInstance().index(request, response);
        } else {
            response.sendError(404);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String uri = request.getRequestURI();
        if (uri.equals(request.getContextPath() + "/account")) {
            AccountController.getInstance().updatePermissions(request, response);
        } else if (uri.equals(request.getContextPath() + "/edit-account")) {
            AccountController.getInstance().editAccount(request, response);
        } else {
            response.sendError(404);
        }
    }
}
