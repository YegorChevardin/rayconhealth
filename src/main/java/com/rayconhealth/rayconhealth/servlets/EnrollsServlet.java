package com.rayconhealth.rayconhealth.servlets;

import com.rayconhealth.rayconhealth.controllers.EnrollsController;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;

@WebServlet(name = "EnrollsServlet", value = {"/enrolls", "/delete-enroll"})
public class EnrollsServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String uri = request.getRequestURI();
        if (uri.equals(request.getContextPath() + "/enrolls")) {
            EnrollsController.getInstance().index(request, response);
        } else {
            EnrollsController.getInstance().deleteEnroll(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
