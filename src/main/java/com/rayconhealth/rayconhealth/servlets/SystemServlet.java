package com.rayconhealth.rayconhealth.servlets;

import com.rayconhealth.rayconhealth.controllers.SystemController;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;

@WebServlet(name = "SystemServlet", value = "/system")
public class SystemServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        SystemController.getInstance().index(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        SystemController.getInstance().enroll(request, response);
    }
}
