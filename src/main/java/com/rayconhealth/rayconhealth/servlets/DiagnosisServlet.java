package com.rayconhealth.rayconhealth.servlets;

import com.rayconhealth.rayconhealth.controllers.DiagnosisController;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;

@WebServlet(name = "DiagnosisServlet", value = "/diagnos")
public class DiagnosisServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DiagnosisController.getInstance().deleteDiagnos(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DiagnosisController.getInstance().addDiagnos(request, response);
    }
}
