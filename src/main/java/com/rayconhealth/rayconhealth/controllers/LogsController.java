package com.rayconhealth.rayconhealth.controllers;

import com.rayconhealth.rayconhealth.controllers.interfaces.Paginatable;
import com.rayconhealth.rayconhealth.database.LogDao;
import com.rayconhealth.rayconhealth.facades.AuthenticationFacade;
import com.rayconhealth.rayconhealth.models.User;
import com.rayconhealth.rayconhealth.models.classes.Log;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static com.rayconhealth.rayconhealth.database.UserDao.ADMIN_ROLE;
import static com.rayconhealth.rayconhealth.database.pereferences.DatabasePreferences.PAGINATION_MAX_PAGE;

public class LogsController {
    private static final String ADMIN_COOKIE = "adminChecked";
    private static final String PAGE_URL = "/logs.jsp";
    private static final String SECONDARY_PAGE_URL = "/password-check.jsp";
    private static LogsController instance = null;

    private LogsController() {}
    public static LogsController getInstance() {
        if (instance == null) {
            instance = new LogsController();
        }
        return instance;
    }

    public void index(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            User user = (User) request.getSession().getAttribute("user");
            if (!user.getRole().equals(ADMIN_ROLE)) {
                throw new IOException("You must be admin to enter this page!");
            }
            if (request.getMethod().equals("GET")) {
                if (isCookieExists(request)) {
                    showLogs(request, response);
                } else {
                    request.getRequestDispatcher(SECONDARY_PAGE_URL).forward(request, response);
                }
            } else if (request.getMethod().equals("POST")){
                String password = request.getParameter("password");
                if (!AuthenticationFacade.passwordCheck(password, user)) {
                    throw new IOException("You typed incorrect password from your account!");
                }
                createCookie(response);
                response.sendRedirect("/logs");
            } else {
                throw new ServletException();
            }
        } catch (ServletException e) {
            response.sendError(404);
        }
    }

    private boolean isCookieExists(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals(ADMIN_COOKIE)) {
                return true;
            }
        }
        return false;
    }
    private void createCookie(HttpServletResponse response) {
        Cookie cookie = new Cookie(ADMIN_COOKIE, String.valueOf(true));
        cookie.setMaxAge(120);
        response.addCookie(cookie);
    }

    public void showLogs(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        User user = (User) request.getSession().getAttribute("user");
        int page = Paginatable.getPaginationPage(request);
        List<Log> logs = LogDao.getInstance().getAllPaginated(page, PAGINATION_MAX_PAGE);
        int totalLogs = LogDao.getInstance().getSize();
        request.setAttribute("totalLogs", totalLogs);
        request.setAttribute("totalPages", (int)Math.ceil((float)totalLogs / PAGINATION_MAX_PAGE));
        request.setAttribute("logs", logs);
        request.getRequestDispatcher(PAGE_URL).forward(request, response);
    }
}
