package com.rayconhealth.rayconhealth.controllers;

import com.rayconhealth.rayconhealth.controllers.interfaces.Paginatable;
import com.rayconhealth.rayconhealth.database.AppointmentDao;
import com.rayconhealth.rayconhealth.database.EnrollDao;
import com.rayconhealth.rayconhealth.models.Appointment;
import com.rayconhealth.rayconhealth.models.User;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.rayconhealth.rayconhealth.database.UserDao.DOCTOR_ROLE;
import static com.rayconhealth.rayconhealth.database.pereferences.DatabasePreferences.PAGINATION_MAX_PAGE;

public class AppointmentsController {
    private static final String PAGE_URL = "/appointments.jsp";
    private static AppointmentsController instance = null;

    private AppointmentsController() {}

    public static AppointmentsController getInstance() {
        if (instance == null) {
            instance = new AppointmentsController();
        }

        return instance;
    }

    public void index(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            User user = (User) request.getSession().getAttribute("user");
            if (request.getMethod().equals("GET")) {
                int page = Paginatable.getPaginationPage(request);
                List<Appointment> appointments = AppointmentDao.getInstance().getUserAppointments(user, page, PAGINATION_MAX_PAGE);
                appointments.sort((o1, o2) -> o1.getMeetingDate().compareTo(o2.getMeetingDate()));
                int totalAppointments = AppointmentDao.getInstance().getSizeOfUsersAppointments(user);
                request.setAttribute("totalPages", (int)Math.ceil((float)totalAppointments / PAGINATION_MAX_PAGE));
                request.setAttribute("appointments", appointments);
                request.setAttribute("AppointmentsRelies", "appointments");
                request.setAttribute("totalAppointments", totalAppointments);
                request.getRequestDispatcher(PAGE_URL).forward(request, response);
            } else {
                throw new ServletException();
            }
        } catch (ServletException | IOException e) {
            e.printStackTrace();
            response.sendError(404, "This page does not exists");
        }
    }

    public void doctorAppointments(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            User user = (User) request.getSession().getAttribute("user");
            if (request.getMethod().equals("GET") && (user.getRole().equals(DOCTOR_ROLE) || user.getRole().equals("nurse"))) {
                int page = Paginatable.getPaginationPage(request);
                List<Appointment> appointments = AppointmentDao.getInstance().getDoctorAppointments(user, page, PAGINATION_MAX_PAGE);
                appointments.sort((o1, o2) -> o1.getMeetingDate().compareTo(o2.getMeetingDate()));
                int totalAppointments = AppointmentDao.getInstance().getSizeOfDoctorAppointments(user);
                request.setAttribute("totalPages", (int)Math.ceil((float)totalAppointments / PAGINATION_MAX_PAGE));
                request.setAttribute("totalAppointments", totalAppointments);
                request.setAttribute("AppointmentsRelies", "doctor-appointments");
                request.setAttribute("appointments", appointments);
                request.getRequestDispatcher(PAGE_URL).forward(request, response);
            } else {
                throw new ServletException();
            }
        } catch (ServletException | IOException e) {
            response.sendError(404, "This page does not exists");
        }
    }
}
