package com.rayconhealth.rayconhealth.controllers;

import com.rayconhealth.rayconhealth.database.DiagnosDao;
import com.rayconhealth.rayconhealth.database.HospitalCardDao;
import com.rayconhealth.rayconhealth.database.UserDao;
import com.rayconhealth.rayconhealth.facades.LoggingFacade;
import com.rayconhealth.rayconhealth.models.Diagnos;
import com.rayconhealth.rayconhealth.models.HospitalCard;
import com.rayconhealth.rayconhealth.models.User;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

import static com.rayconhealth.rayconhealth.database.UserDao.DOCTOR_ROLE;

public class DiagnosisController {
    private static final String PAGE_URL = "/hospital-card";
    private static DiagnosisController instance = null;

    private DiagnosisController() {}

    public static DiagnosisController getInstance() {
        if (instance == null) {
            instance = new DiagnosisController();
        }
        return instance;
    }

    public void addDiagnos(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            User user = (User) request.getSession().getAttribute("user");
            if (request.getMethod().equals("POST") && user.getRole().equals(DOCTOR_ROLE)) {
                int patientId = Integer.parseInt(request.getParameter("userId"));
                String diagnosName = request.getParameter("name");
                String diagnosStatus = request.getParameter("status");
                HospitalCard hospitalCard = HospitalCardDao.getInstance().getUsersHospitalCard(UserDao.getInstance().getById(patientId));
                Diagnos diagnos = new Diagnos(
                        diagnosName,
                        diagnosStatus,
                        user,
                        hospitalCard
                );
                DiagnosDao.getInstance().insert(diagnos);
                response.sendRedirect(PAGE_URL + "?id=" + hospitalCard.getId());
            } else {
                throw new IOException("Wrong method operation");
            }
        } catch (RuntimeException e) {
            LoggingFacade.makeLogFatal(e.getMessage());
            response.sendError(404);
        }
    }

    public void deleteDiagnos(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            User user = (User) request.getSession().getAttribute("user");
            if (request.getMethod().equals("GET") && user.getRole().equals(DOCTOR_ROLE)) {
                try {
                    int diagnosId = Integer.parseInt(request.getParameter("id"));
                    int userId = Integer.parseInt(request.getParameter("user_id"));
                    Diagnos diagnos = DiagnosDao.getInstance().getById(diagnosId);
                    User patient = UserDao.getInstance().getById(userId);
                    if (diagnos.getHospitalCard().getUser().equalsUser(patient)) {
                        if (!DiagnosDao.getInstance().delete(diagnos)) throw new NumberFormatException();
                    } else {
                        throw new IOException("This diagnos do not belongs to this user, you cannot delete it");
                    }
                    response.sendRedirect(PAGE_URL + "?id=" + diagnos.getHospitalCard().getId());
                } catch (NumberFormatException e) {
                    throw new IOException("Cannot delete this diagnos");
                }
            } else {
                throw new IOException("Wrong method operation");
            }
        } catch (RuntimeException e) {
            LoggingFacade.makeLogFatal(e.getMessage());
            response.sendError(404);
        }
    }
}
