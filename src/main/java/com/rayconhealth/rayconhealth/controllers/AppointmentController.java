package com.rayconhealth.rayconhealth.controllers;

import com.rayconhealth.rayconhealth.database.AppointmentDao;
import com.rayconhealth.rayconhealth.database.HospitalCardDao;
import com.rayconhealth.rayconhealth.database.UserDao;
import com.rayconhealth.rayconhealth.facades.LoggingFacade;
import com.rayconhealth.rayconhealth.models.Appointment;
import com.rayconhealth.rayconhealth.models.User;
import com.rayconhealth.rayconhealth.models.classes.Email;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;

import static com.rayconhealth.rayconhealth.database.UserDao.DOCTOR_ROLE;

public class AppointmentController {
    private static final String PAGE_URL = "/appointment.jsp";
    private static AppointmentController instance = null;

    private AppointmentController() {}
    public static AppointmentController getInstance() {
        if (instance == null) {
            instance = new AppointmentController();
        }
        return instance;
    }

    public void index(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            User user = (User) request.getSession().getAttribute("user");
            if (request.getMethod().equals("GET")) {
                Integer id = null;
                try {
                    id = Integer.valueOf(request.getParameter("id"));
                } catch (NumberFormatException e) {
                    throw new ServletException("No id was given");
                }
                Appointment appointment = AppointmentDao.getInstance().getById(id);
                request.setAttribute("appointment", appointment);
                request.getRequestDispatcher(PAGE_URL).forward(request, response);
            } else {
                throw new ServletException("wrong method operation");
            }
        } catch (ServletException | IOException e) {
            response.sendError(404, "This page does not exists");
        } catch (RuntimeException e) {
            LoggingFacade.makeLogFatal(e.getMessage());
            response.sendError(404);
        }
    }

    public void createAppointment(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            User user = (User) request.getSession().getAttribute("user");
            if (request.getMethod().equals("POST") && (user.getRole().equals(DOCTOR_ROLE) || user.getRole().equals("nurse"))) {
                String[] dateString = request.getParameter("date").split("-");
                Date date = new Date(
                        Integer.parseInt(dateString[0]) - 1900,
                        Integer.parseInt(dateString[1]) - 1,
                        Integer.parseInt(dateString[2])
                );
                User patient = UserDao.getInstance().getUserByEmail(Email.generateEmail(request.getParameter("user-email")));
                String appointmentType = request.getParameter("appointment-type");
                Appointment appointment = new Appointment(
                        date,
                        user,
                        patient,
                        appointmentType,
                        HospitalCardDao.instance.getUsersHospitalCard(patient)
                );

                if (!AppointmentDao.getInstance().insert(appointment)) throw new IOException("Cannot make an appointment with this user");
                response.sendRedirect("/appointment?id=" + appointment.getId());
                LoggingFacade.makeLogEvent(user.getEmail().getValue() + " this doctor made appointment with this user: " + patient.getEmail().getValue());
            } else {
                throw new IOException("Wrong method operation");
            }
        } catch (RuntimeException e) {
            LoggingFacade.makeLogFatal(e.getMessage());
            response.sendError(404);
        }
    }

    public void deleteAppointment(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            User doctor = (User) request.getSession().getAttribute("user");
            if (request.getMethod().equals("GET") && (doctor.getRole().equals(DOCTOR_ROLE) || doctor.getRole().equals("nurse"))) {
                try {
                    int appointmentId = Integer.parseInt(request.getParameter("id"));
                    Appointment appointment = AppointmentDao.getInstance().getById(appointmentId);
                    if (!doctor.equalsUser(appointment.getDoctor())) throw new IOException("Cannot delete this appointment, because its not yours");
                    if (!AppointmentDao.getInstance().delete(appointment)) throw new IOException("Cannot delete appointment");
                    response.sendRedirect("/doctor-appointments");
                } catch (NumberFormatException e) {
                    throw new IOException("Cannot delete this appointment");
                }
            } else {
                throw new IOException("Wrong method operation");
            }
        } catch (RuntimeException e) {
            LoggingFacade.makeLogFatal(e.getMessage());
            response.sendError(404);
        }
    }
}
