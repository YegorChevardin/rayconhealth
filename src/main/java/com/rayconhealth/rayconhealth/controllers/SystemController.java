package com.rayconhealth.rayconhealth.controllers;

import com.rayconhealth.rayconhealth.database.AppointmentDao;
import com.rayconhealth.rayconhealth.database.EnrollDao;
import com.rayconhealth.rayconhealth.database.UserDao;
import com.rayconhealth.rayconhealth.facades.LoggingFacade;
import com.rayconhealth.rayconhealth.models.Enroll;
import com.rayconhealth.rayconhealth.models.User;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.sql.SQLException;

public class SystemController {
    private static final String PAGE_URL = "/system.jsp";
    private static SystemController instance = null;
    private SystemController() {}

    public static SystemController getInstance() {
        if (instance == null) {
            instance = new SystemController();
        }
        return instance;
    }

    public void index(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            if (request.getMethod().equals("GET")) {
                User user = (User)request.getSession().getAttribute("user");

                switch (user.getRole()) {
                    case "admin":
                        Integer upcomingPatients = EnrollDao.getInstance().getAll().size();
                        Integer totalDoctors = UserDao.getInstance().getUsersByRole("doctor").size() + UserDao.getInstance().getUsersByRole("nurse").size();
                        Integer totalAdmins = UserDao.getInstance().getUsersByRole("admin").size();
                        request.setAttribute("upcomingPatients", upcomingPatients);
                        request.setAttribute("totalDoctors", totalDoctors);
                        request.setAttribute("totalAdmins", totalAdmins);
                        break;
                    case "doctor":
                    case "nurse":
                        Integer doctorPatients = UserDao.getInstance().getDoctorUsers(user).size();
                        Integer appointmentsCount = AppointmentDao.getInstance().getDoctorAppointments(user).size();
                        request.setAttribute("doctorPatients", doctorPatients);
                        request.setAttribute("appointmentsCount", appointmentsCount);
                        break;
                    default:
                        Integer userDoctors = UserDao.getInstance().getUserDoctors(user).size();
                        Integer userAppointmentsCount = AppointmentDao.getInstance().getUserAppointments(user).size();
                        request.setAttribute("userDoctors", userDoctors);
                        request.setAttribute("userAppointmentsCount", userAppointmentsCount);
                }

                request.getRequestDispatcher(PAGE_URL).forward(request, response);
            } else {
                throw new ServletException();
            }
        } catch (ServletException e) {
            response.sendError(405, "Wrong method operation");
        } catch (IOException e) {
            response.sendError(404, "Page not found");
        }
    }

    public void enroll(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            if (request.getMethod().equals("POST")) {
                User user = (User) request.getSession().getAttribute("user");
                String text = request.getParameter("pain-description");
                Enroll enroll = new Enroll(user, text);
                EnrollDao.getInstance().insert(enroll);
                response.sendRedirect("success");
                LoggingFacade.makeLogEvent("User " + user.getEmail().getValue() + " made an enroll");
            } else {
                throw new ServletException();
            }
        } catch (ServletException e) {
            response.sendError(405, "Wrong method operation");
        } catch (IOException e) {
            response.sendError(404, "Page not found");
        } catch (RuntimeException e) {
            LoggingFacade.makeLogFatal(e.getMessage());
            response.sendError(404);
        }
    }
}
