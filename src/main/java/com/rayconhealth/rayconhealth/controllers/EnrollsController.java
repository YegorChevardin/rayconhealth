package com.rayconhealth.rayconhealth.controllers;

import com.rayconhealth.rayconhealth.controllers.interfaces.Paginatable;
import com.rayconhealth.rayconhealth.database.EnrollDao;
import com.rayconhealth.rayconhealth.models.Enroll;
import com.rayconhealth.rayconhealth.models.User;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;

import static com.rayconhealth.rayconhealth.database.UserDao.ADMIN_ROLE;
import static com.rayconhealth.rayconhealth.database.pereferences.DatabasePreferences.PAGINATION_MAX_PAGE;

public class EnrollsController {
    private static EnrollsController instance = null;
    private static final String PAGE_URL = "/enrolls.jsp";

    private EnrollsController() {}

    public static EnrollsController getInstance() {
        if (instance == null) {
            instance = new EnrollsController();
        }

        return instance;
    }

    public void index(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            User user = (User) request.getSession().getAttribute("user");
            if (request.getMethod().equals("GET") && user.getRole().equals("admin")) {
                int page = Paginatable.getPaginationPage(request);
                List<Enroll> enrolls = EnrollDao.getInstance().getAll(page, PAGINATION_MAX_PAGE);
                int totalEnrolls = EnrollDao.getInstance().getSize();
                request.setAttribute("totalEnrolls", totalEnrolls);
                request.setAttribute("totalPages", (int)Math.ceil((float)totalEnrolls / PAGINATION_MAX_PAGE));
                request.setAttribute("enrolls", enrolls);
                request.getRequestDispatcher(PAGE_URL).forward(request, response);
            } else {
                throw new ServletException();
            }
        } catch (ServletException | IOException e) {
            e.printStackTrace();
            response.sendError(404, "This page does not exists");
        }
    }

    public void deleteEnroll(HttpServletRequest request, HttpServletResponse response) throws IOException {
        User user = (User) request.getSession().getAttribute("user");
        if (request.getMethod().equals("GET") && user.getRole().equals(ADMIN_ROLE)) {
            Integer enrollId = null;
            try {
                enrollId = Integer.parseInt(request.getParameter("id"));
            } catch (NumberFormatException e) {
                throw new IOException("Cannot delete this enroll");
            }
            if (!EnrollDao.getInstance().delete(EnrollDao.getInstance().getById(enrollId))) throw new IOException("Cannot delete this enroll, something went wrong");
            response.sendRedirect("/enrolls");
        } else {
            throw new IOException("Wrong method action");
        }
    }
}
