package com.rayconhealth.rayconhealth.controllers;

import com.rayconhealth.rayconhealth.database.UserDao;
import com.rayconhealth.rayconhealth.facades.AuthenticationFacade;
import com.rayconhealth.rayconhealth.facades.EnrollToDoctorFacade;
import com.rayconhealth.rayconhealth.facades.LoggingFacade;
import com.rayconhealth.rayconhealth.models.User;
import com.rayconhealth.rayconhealth.models.classes.Email;
import com.rayconhealth.rayconhealth.models.classes.Password;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static com.rayconhealth.rayconhealth.database.UserDao.*;
import static com.rayconhealth.rayconhealth.database.UserDao.DEFAULT_USER_CATEGORY;

public class UsersListController {
    private static UsersListController instance = null;
    private static final String PAGE_URL = "/users-list.jsp";
    private static final String ENTITIES_LIST_NAME = "entityList";

    private UsersListController() {}

    public static UsersListController getInstance() {
        if (instance == null) {
            instance = new UsersListController();
        }

        return instance;
    }

    public void users(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            if (request.getMethod().equals("GET")) {
                User user = (User) request.getSession().getAttribute("user");
                if (user.getRole().equals("admin")) {
                    request.setAttribute(ENTITIES_LIST_NAME, UserDao.getInstance().getAll());
                    request.setAttribute("type", "users");
                    request.getRequestDispatcher(PAGE_URL).forward(request, response);
                } else {
                    throw new ServletException();
                }
            } else {
                throw new IOException("Wrong method operation");
            }
        } catch (ServletException e) {
            response.sendError(405, "Page not found");
        }
    }

    public void doctors(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            if (request.getMethod().equals("GET")) {
                User user = (User) request.getSession().getAttribute("user");
                if (user.getRole().equals(ADMIN_ROLE)) {
                    List<User> allDoctors = new ArrayList<>();
                    allDoctors.addAll(UserDao.getInstance().getUsersByRole(DOCTOR_ROLE));
                    allDoctors.addAll(UserDao.getInstance().getUsersByRole("nurse"));

                    allDoctors.forEach(user1 -> user1.setNumberOfPatients(UserDao.getInstance().getDoctorUsers(user1).size()));

                    request.setAttribute(ENTITIES_LIST_NAME, allDoctors);
                    request.setAttribute("type", "doctors");

                    request.getRequestDispatcher(PAGE_URL).forward(request, response);
                } else {
                    throw new ServletException();
                }
            } else {
                throw new IOException("Wrong method operation");
            }
        } catch (ServletException e) {
            response.sendError(405, "Page not found");
        }
    }

    public void patients(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            if (request.getMethod().equals("GET")) {
                User user = (User) request.getSession().getAttribute("user");
                if (user.getRole().equals(DOCTOR_ROLE) || user.getRole().equals("nurse")) {
                    request.setAttribute(ENTITIES_LIST_NAME, UserDao.getInstance().getDoctorUsers(user));
                    request.setAttribute("type", "users");
                    request.setAttribute("myPatients", true);
                    request.getRequestDispatcher(PAGE_URL).forward(request, response);
                } else {
                    throw new ServletException();
                }
            } else {
                throw new IOException("Wrong method operation");
            }
        } catch (ServletException e) {
            response.sendError(405, "Page not found");
        }
    }

    public void admins(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            if (request.getMethod().equals("GET")) {
                User user = (User) request.getSession().getAttribute("user");
                if (user.getRole().equals(ADMIN_ROLE)) {
                    request.setAttribute(ENTITIES_LIST_NAME, UserDao.getInstance().getUsersByRole(ADMIN_ROLE));
                    request.setAttribute("type", "admins");
                    request.getRequestDispatcher(PAGE_URL).forward(request, response);
                } else {
                    throw new ServletException();
                }
            } else {
                throw new IOException("Wrong method operation");
            }
        } catch (ServletException e) {
            response.sendError(405, "Page not found");
        }
    }

    public void myDoctors(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            if (request.getMethod().equals("GET")) {
                User user = (User) request.getSession().getAttribute("user");
                List<User> userDoctors = UserDao.getInstance().getUserDoctors(user);

                userDoctors.forEach(user1 -> user1.setNumberOfPatients(UserDao.getInstance().getDoctorUsers(user1).size()));

                request.setAttribute(ENTITIES_LIST_NAME, userDoctors);
                request.setAttribute("myDoctors", true);
                request.setAttribute("type", "doctors");

                request.getRequestDispatcher(PAGE_URL).forward(request, response);
            } else {
                throw new IOException("Wrong method operation");
            }
        } catch (ServletException e) {
            response.sendError(405, "Page not found");
        }
    }

    public void adminRegister(HttpServletRequest request, HttpServletResponse response) throws IOException {
        User currentUser = (User) request.getSession().getAttribute("user");
        if (request.getMethod().equals("POST") && currentUser.getRole().equals(ADMIN_ROLE)) {
            String name = request.getParameter("name");
            String secondName = request.getParameter("second-name");
            String phone = request.getParameter("phone");
            Email email = Email.generateEmail(request.getParameter("email"));
            Password password = Password.generatePassword(request.getParameter("password"), false);
            String[] birthDayDate = request.getParameter("birth-day").split("-");
            Date birthDay = new Date(
                    Integer.parseInt(birthDayDate[0]) - 1900,
                    Integer.parseInt(birthDayDate[1]) - 1,
                    Integer.parseInt(birthDayDate[2])
            );

            User user = new User(name, secondName, birthDay, email, phone, password, DEFAULT_USER_ROLE, DEFAULT_USER_CATEGORY);

            AuthenticationFacade.adminRegister(user);
            response.sendRedirect("/system");
            LoggingFacade.makeLogEvent(currentUser.getEmail().getValue() + " registered new user: " + user.getEmail().getValue());
        }
    }

    public void deletePatient(HttpServletRequest request, HttpServletResponse response) throws IOException {
        User user = (User) request.getSession().getAttribute("user");
        try {
            if (request.getMethod().equals("GET")) {
                Integer deletingPairUserId = null;
                try {
                    deletingPairUserId = Integer.parseInt(request.getParameter("id"));
                } catch (NumberFormatException e) {
                    throw new IOException("Cannot delete pair with given parameters");
                }
                if (user.getRole().equals(DOCTOR_ROLE) || user.getRole().equals("nurse")) {
                    if (!EnrollToDoctorFacade.deleteUserDoctorPairIfExists(user, UserDao.getInstance().getById(deletingPairUserId))) {
                        throw new IOException("Cannot delete this patient");
                    }
                } else {
                    throw new ServletException();
                }
                response.sendRedirect("/patients-list");
            } else {
                throw new ServletException();
            }
        } catch (ServletException e) {
            response.sendError(404);
        } catch (RuntimeException e) {
            LoggingFacade.makeLogFatal(e.getMessage());
            response.sendError(404);
        }
    }

    public void deleteDoctor(HttpServletRequest request, HttpServletResponse response) throws IOException {
        User user = (User) request.getSession().getAttribute("user");
        try {
            if (request.getMethod().equals("GET")) {
                Integer deletingPairUserId = null;
                try {
                    deletingPairUserId = Integer.parseInt(request.getParameter("id"));
                } catch (NumberFormatException e) {
                    throw new IOException("Cannot delete pair with given parameters");
                }
                if (!EnrollToDoctorFacade.deleteUserDoctorPairIfExists(UserDao.getInstance().getById(deletingPairUserId), user)) {
                    throw new IOException("Cannot delete this doctor");
                }
                response.sendRedirect("/my-doctors");
            } else {
                throw new ServletException();
            }
        } catch (ServletException e) {
            response.sendError(404);
        } catch (RuntimeException e) {
            LoggingFacade.makeLogFatal(e.getMessage());
            response.sendError(404);
        }
    }
}
