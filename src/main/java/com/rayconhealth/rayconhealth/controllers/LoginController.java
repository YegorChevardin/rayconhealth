package com.rayconhealth.rayconhealth.controllers;

import com.rayconhealth.rayconhealth.database.UserDao;
import com.rayconhealth.rayconhealth.facades.AuthenticationFacade;
import com.rayconhealth.rayconhealth.models.User;
import com.rayconhealth.rayconhealth.models.classes.Email;
import com.rayconhealth.rayconhealth.models.classes.Password;
import com.rayconhealth.rayconhealth.models.classes.PasswordHashing;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;

import static com.rayconhealth.rayconhealth.database.UserDao.DEFAULT_USER_CATEGORY;
import static com.rayconhealth.rayconhealth.database.UserDao.DEFAULT_USER_ROLE;

public class LoginController {
    private static final String PAGE_URL = "/login.jsp";
    private static LoginController instance = null;
    private LoginController() {}

    public static LoginController getInstance() {
        if (instance == null) {
            instance = new LoginController();
        }
        return instance;
    }

    public void index(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            if (request.getMethod().equals("GET")) {
                request.getRequestDispatcher(PAGE_URL).forward(request, response);
            } else {
                throw new ServletException();
            }
        } catch (ServletException | IOException e) {
            response.sendError(404, "This page does not exists");
        }
    }

    public void login(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            if (request.getMethod().equals("POST")) {
                Email email = Email.generateEmail(request.getParameter("email"));
                User user = UserDao.getInstance().getUserByEmail(email);

                if (user != null) {
                    AuthenticationFacade.login(request, user);
                    response.sendRedirect("/system");
                } else {
                    throw new SQLException("Wrong email");
                }
            } else {
                throw new ServletException();
            }
        } catch (ServletException e) {
            response.sendError(405, "Wrong method operation");
        } catch (SQLException e) {
            throw new IOException(e.getMessage());
        }
    }
}
