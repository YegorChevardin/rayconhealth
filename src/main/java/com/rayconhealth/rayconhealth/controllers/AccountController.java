package com.rayconhealth.rayconhealth.controllers;

import com.rayconhealth.rayconhealth.database.AppointmentDao;
import com.rayconhealth.rayconhealth.database.HospitalCardDao;
import com.rayconhealth.rayconhealth.database.UserDao;
import com.rayconhealth.rayconhealth.facades.AuthenticationFacade;
import com.rayconhealth.rayconhealth.facades.EnrollToDoctorFacade;
import com.rayconhealth.rayconhealth.facades.LoggingFacade;
import com.rayconhealth.rayconhealth.models.Appointment;
import com.rayconhealth.rayconhealth.models.User;
import com.rayconhealth.rayconhealth.models.classes.Email;
import com.rayconhealth.rayconhealth.models.classes.Password;
import com.rayconhealth.rayconhealth.models.classes.PasswordHashing;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.sql.Date;
import java.util.List;

import static com.rayconhealth.rayconhealth.database.UserDao.*;

public class AccountController {
    private static AccountController instance = null;
    private static final String PAGE_URL = "/account.jsp";

    private AccountController() {
    }

    public static AccountController getInstance() {
        if (instance == null) {
            instance = new AccountController();
        }

        return instance;
    }

    /**
     * main method that makes response to user with view of his account
     *
     * @param request  user http request
     * @param response user http response
     */
    public void index(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            if (!request.getMethod().equals("GET")) throw new IOException("Wrong method operation");

            User user = (User) request.getSession().getAttribute("user");

            request.setAttribute("showUser", user);
            request.setAttribute("sameUser", true);
            request.setAttribute("hospitalCardId", HospitalCardDao.getInstance().getUsersHospitalCard(user).getId());

            if (user.getRole().equals(ADMIN_ROLE)) {
                List<String> categories = UserDao.getInstance().getAllCategories();
                categories.remove("patient");
                request.setAttribute("categories", categories);
            }
            request.getRequestDispatcher(PAGE_URL).forward(request, response);
        } catch (ServletException e) {
            response.sendError(404, "Page does not exists");
        } catch (IOException e) {
            throw new IOException(e.getMessage());
        } catch (RuntimeException e) {
            LoggingFacade.makeLogFatal(e.getMessage());
            response.sendError(404);
        }
    }

    /**
     * method that will prepare view for showing specific user
     *
     * @param request  user http request
     * @param response user http reqponse
     */
    public void getUser(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            if (!request.getMethod().equals("GET")) throw new IOException("Wrong method operation");

            User user = (User) request.getSession().getAttribute("user");
            Integer showUserId = null;
            User showUser = null;
            try {
                showUserId = Integer.valueOf(request.getParameter("id"));
            } catch (NumberFormatException e) {
                throw new ServletException();
            }
            showUser = UserDao.getInstance().getById(showUserId);
            boolean sameUser = user.equalsUser(showUser);

            if (!(sameUser || user.getRole().equals(ADMIN_ROLE) || ((user.getRole().equals(DOCTOR_ROLE) || user.getRole().equals("nurse")) && EnrollToDoctorFacade.isPatientExists(user.getId(), showUserId)))) throw new ServletException();

            if (user.getRole().equals(DOCTOR_ROLE) || user.getRole().equals("nurse")) {
                List<String> appointmentTypes = AppointmentDao.getInstance().getAllAppointmentTypes();
                if (user.getRole().equals("nurse")) {
                    appointmentTypes.remove("operation");
                }
                request.setAttribute("appointmentTypes", appointmentTypes);
            }
            request.setAttribute("showUser", showUser);
            if (user.getRole().equals(ADMIN_ROLE)) {
                List<String> categories = UserDao.getInstance().getAllCategories();
                categories.remove("patient");
                request.setAttribute("categories", categories);
                if (!sameUser) {
                    List<String> roles = UserDao.getInstance().getAllRoles();
                    makeFirstElement(roles, showUser.getRole());
                    request.setAttribute("roles", roles);
                }
            }
            request.setAttribute("sameUser", sameUser);
            request.setAttribute("hospitalCardId", HospitalCardDao.getInstance().getUsersHospitalCard(showUser).getId());

            request.getRequestDispatcher(PAGE_URL).forward(request, response);
        } catch (ServletException e) {
            response.sendError(404, "Page does not exists");
        } catch (RuntimeException e) {
            LoggingFacade.makeLogFatal(e.getMessage());
            response.sendError(404);
        }
    }

    /**
     * method that will move specific element from the list to first position
     *
     * @param arrayList list with elements
     * @param element   element to move to the first position
     * @return prepeared list
     */
    private static List<String> makeFirstElement(List<String> arrayList, String element) {
        arrayList.remove(element);
        arrayList.add(0, element);
        return arrayList;
    }

    /**
     * Method that will update specific user permissions if you are admin
     *
     * @param request  http user request
     * @param response https user response
     */
    public void updatePermissions(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            User user = (User) request.getSession().getAttribute("user");
            if (!(request.getMethod().equals("POST") && user.getRole().equals(ADMIN_ROLE))) throw new ServletException();
            User operationUser = UserDao.getInstance().getUserByEmail(Email.generateEmail(request.getParameter("user-email")));
            String updatingRole = request.getParameter("role");
            String updatingCategory = request.getParameter("category");

            if (updatingCategory == null) updatingCategory = "patient";

            operationUser.setRole(updatingRole);
            operationUser.setCategory(updatingCategory);

            if (!UserDao.getInstance().updateRoleAndCategory(operationUser)) throw new IOException("Cannot update this user: " + operationUser.getId());
            if (operationUser.getRole().equals(ADMIN_ROLE) || operationUser.getRole().equals("user")) {
                UserDao.getInstance().deleteAllDoctorPatients(operationUser);
            }
            response.sendRedirect("/user?id=" + operationUser.getId());
            LoggingFacade.makeLogEvent(user.getEmail().getValue() + " updated permissions of user " + operationUser.getEmail().getValue() + " to role " + operationUser.getRole() + " category of " + operationUser.getCategory());
        } catch (ServletException e) {
            response.sendError(404);
        } catch (RuntimeException e) {
            LoggingFacade.makeLogFatal(e.getMessage());
            response.sendError(404);
        }
    }

    /**
     * Method that will change user account preferences
     *
     * @param request  user http request
     * @param response user https response
     */
    public void editAccount(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            if (!request.getMethod().equals("POST")) throw new IOException("Wrong method operation");
            String action = request.getParameter("edit");
            User user = UserDao.getInstance().getUserByEmail(Email.generateEmail(request.getParameter("userEmail")));
            String oldEmail = user.getEmail().getValue();
            switch (action) {
                case "change":
                    if (!changeInformationAccount(request, user)) throw new IOException("Cannot update this user");
                    LoggingFacade.makeLogEvent(oldEmail + " changed his account information, new email: " + user.getEmail().getValue());
                    break;
                case "delete":
                    if (!deleteAccount(request, user)) throw new IOException("Cannot delete this user account");
                    LoggingFacade.makeLogEvent(user.getEmail().getValue() + " deleted account ");
                    break;
                case "passwordChange":
                    if (!changePasswordAccount(request, user))
                        throw new IOException("Cannot change this user password");
                    break;
                default:
                    throw new ServletException();
            }
            response.sendRedirect("/success");
        } catch (ServletException e) {
            response.sendError(404);
        } catch (RuntimeException e) {
            LoggingFacade.makeLogFatal(e.getMessage());
            response.sendError(404);
        }
    }

    /**
     * Method that will change user password
     *
     * @param request http user request
     * @param user    user, whos password will change
     * @return was password changed or not
     */
    private static boolean changePasswordAccount(HttpServletRequest request, User user) throws IOException, ServletException {
        boolean result = false;

        String currentPassword = request.getParameter("password");
        String newPassword = request.getParameter("newPassword");
        String confirmPassword = request.getParameter("confirmPassword");

        if (!confirmPassword.equals(newPassword)) throw new IOException("new password don't equals confirmed password");
        boolean passwordMatches = new PasswordHashing().authenticate(currentPassword, user.getPassword().getValue());
        if (!passwordMatches) throw new IOException("wrong password!");
        user.setPassword(Password.generatePassword(newPassword, false));
        result = UserDao.getInstance().updatePassword(user);
        AuthenticationFacade.logout(request);

        return result;
    }

    /**
     * Method that will delete user account
     *
     * @param request http user request
     * @param user    user which will be deleted
     * @return was user account deleted or not
     */
    private static boolean deleteAccount(HttpServletRequest request, User user) throws ServletException {
        boolean result = false;

        result = UserDao.getInstance().delete(user);
        User currentUser = (User) request.getSession().getAttribute("user");
        if (result && user.equalsUser(currentUser)) {
            AuthenticationFacade.logout(request);
        }

        return result;
    }

    /**
     * Method that will change specific information in the user account
     *
     * @param request http user request
     * @param user    user account which information will be changed
     * @return was account information changed or not
     */
    private static boolean changeInformationAccount(HttpServletRequest request, User user) throws ServletException {
        boolean result = false;
        String name = request.getParameter("name");
        String secondName = request.getParameter("second-name");
        String[] birthDayDate = request.getParameter("birth-day").split("-");
        Date birthDay = new Date(
                Integer.parseInt(birthDayDate[0]) - 1900,
                Integer.parseInt(birthDayDate[1]) - 1,
                Integer.parseInt(birthDayDate[2])
        );
        String phone = request.getParameter("phone");
        Email email = Email.generateEmail(request.getParameter("email"));

        user.setName(name);
        user.setSecondName(secondName);
        user.setBirthDay(birthDay);
        user.setPhone(phone);
        user.setEmail(email);

        result = UserDao.getInstance().update(user);
        User currentUser = (User) request.getSession().getAttribute("user");
        if (result && user.equalsUser(currentUser)) {
            AuthenticationFacade.logout(request);
        }

        return result;
    }

}
