package com.rayconhealth.rayconhealth.controllers;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

public class ErrorHandlerController {
    private static ErrorHandlerController instance = null;
    private static final String PAGE = "/error.jsp";

    private ErrorHandlerController() {}

    public static ErrorHandlerController getInstance() {
        if (instance == null) {
            instance = new ErrorHandlerController();
        }

        return instance;
    }

    public void index(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String errorMessage = null;
        switch (response.getStatus()) {
            case 404:
                errorMessage = "Page not found";
                break;
            case 409:
                errorMessage = "Inserted data is not valid, please try again";
                break;
            case 405:
                errorMessage = "Wrong method operation";
                break;
            default:
                errorMessage = "Oops, something went wrong, please try again later";
        }
        request.setAttribute("errorMessage", errorMessage);
        request.getRequestDispatcher(PAGE).forward(request, response);
    }
}
