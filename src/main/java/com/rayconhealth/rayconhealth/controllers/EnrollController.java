package com.rayconhealth.rayconhealth.controllers;

import com.google.gson.Gson;
import com.rayconhealth.rayconhealth.database.EnrollDao;
import com.rayconhealth.rayconhealth.database.UserDao;
import com.rayconhealth.rayconhealth.facades.EnrollToDoctorFacade;
import com.rayconhealth.rayconhealth.models.Enroll;
import com.rayconhealth.rayconhealth.models.User;
import com.rayconhealth.rayconhealth.models.classes.Email;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.rmi.ServerException;
import java.util.List;

public class EnrollController {
    private static EnrollController instance = null;
    private static final String PAGE_URL = "/enroll.jsp";
    private static final String SUCCESS_PAGE = "/success";

    private EnrollController() {}

    public static EnrollController getInstance() {
        if (instance == null) {
            instance = new EnrollController();
        }

        return instance;
    }

    public void index(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            User user = (User) request.getSession().getAttribute("user");
            if (request.getMethod().equals("GET") && user.getRole().equals("admin")) {
                Integer id = null;
                try {
                    id = Integer.valueOf(request.getParameter("id"));
                } catch (NumberFormatException e) {
                    throw new ServletException();
                }
                request.setAttribute("enroll", EnrollDao.getInstance().getById(id));
                List<String> categories = UserDao.getInstance().getAllCategories();
                categories.remove("patient");
                request.setAttribute("categories", categories);
                request.getRequestDispatcher(PAGE_URL).forward(request, response);
            } else {
                throw new ServletException();
            }
        } catch (ServletException | IOException e) {
            response.sendError(404, "This page does not exists");
        }
    }

    public void enrollUser(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            User user = (User) request.getSession().getAttribute("user");
            if (request.getMethod().equals("POST") && user.getRole().equals("admin")) {
                String operation = request.getParameter("operation");

                switch (operation) {
                    case "getDoctors":
                        String category = request.getParameter("category");
                        List<User> doctors = UserDao.getInstance().getUsersByCategory(category);
                        Gson result = new Gson();
                        String doctorsList = result.toJson(doctors);
                        response.setContentType("text/html");
                        response.getWriter().write(doctorsList);
                        break;
                    case "enroll":
                        Email doctorEmail = Email.generateEmail(request.getParameter("doctor-email"));
                        User doctor = UserDao.getInstance().getUserByEmail(doctorEmail);
                        User patient = UserDao.getInstance().getUserByEmail(Email.generateEmail(request.getParameter("user-email")));

                        try {
                            int enrollId = Integer.parseInt(request.getParameter("enroll-id"));
                            Enroll enroll = EnrollDao.getInstance().getById(enrollId);
                            EnrollDao.getInstance().delete(enroll);
                        } catch (NumberFormatException ignored) {}

                        if (EnrollToDoctorFacade.enrollUserToDoctor(patient, doctor)) {
                            response.sendRedirect(SUCCESS_PAGE);
                        } else {
                            throw new ServletException("Cannot sing in this user to doctor, because this user was enrolled to this doctor earlier");
                        }
                        break;
                    default:
                        throw new ServletException("Server don't support this operation");
                }
            } else {
                throw new ServletException("Wrong method operation!");
            }
        } catch (ServletException e) {
            throw new ServerException(e.getMessage());
        } catch (IOException e) {
            response.sendError(404);
        }
    }
}
