package com.rayconhealth.rayconhealth.controllers;

import com.rayconhealth.rayconhealth.database.DiagnosDao;
import com.rayconhealth.rayconhealth.database.HospitalCardDao;
import com.rayconhealth.rayconhealth.database.UserDao;
import com.rayconhealth.rayconhealth.facades.EnrollToDoctorFacade;
import com.rayconhealth.rayconhealth.facades.LoggingFacade;
import com.rayconhealth.rayconhealth.models.HospitalCard;
import com.rayconhealth.rayconhealth.models.User;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.sql.SQLException;

import static com.rayconhealth.rayconhealth.database.UserDao.DOCTOR_ROLE;

public class HospitalCardController {
    private static final String PAGE_URL = "/hospital-card.jsp";
    private static HospitalCardController instance = null;

    private HospitalCardController () {}

    public static HospitalCardController getInstance() {
        if (instance == null) {
            instance = new HospitalCardController();
        }

        return instance;
    }

    public void index(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            User user = (User) request.getSession().getAttribute("user");
            if (request.getMethod().equals("GET")) {
                Integer hospitalCardId = null;
                HospitalCard hospitalCard = null;
                try {
                    hospitalCardId = Integer.valueOf(request.getParameter("id"));
                    hospitalCard = HospitalCardDao.instance.getById(hospitalCardId);
                } catch (NumberFormatException e) {
                    hospitalCard = HospitalCardDao.getInstance().getUsersHospitalCard(user);
                }
                if (hospitalCard != null && (user.equalsUser(hospitalCard.getUser()) || EnrollToDoctorFacade.isPatientExists(user.getId(), hospitalCard.getUser().getId()))) {
                    request.setAttribute("hospitalCard", hospitalCard);
                    request.setAttribute("diagnosis", DiagnosDao.getInstance().getDiagnosByHospitalCard(hospitalCard));
                    if (user.getRole().equals(DOCTOR_ROLE)) {
                        request.setAttribute("diagnosStatuses", DiagnosDao.getInstance().getAllStatuses());
                    }
                    request.getRequestDispatcher(PAGE_URL).forward(request, response);
                } else {
                    throw new ServletException();
                }
            } else {
                throw new ServletException();
            }
        } catch (ServletException | IOException e) {
            e.printStackTrace();
            response.sendError(404, "This page does not exists");
        } catch (RuntimeException e) {
            LoggingFacade.makeLogFatal(e.getMessage());
            response.sendError(404);
        }
    }
}
