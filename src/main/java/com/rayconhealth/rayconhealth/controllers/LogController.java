package com.rayconhealth.rayconhealth.controllers;

import com.rayconhealth.rayconhealth.database.LogDao;
import com.rayconhealth.rayconhealth.facades.LoggingFacade;
import com.rayconhealth.rayconhealth.models.User;
import com.rayconhealth.rayconhealth.models.classes.Log;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.rmi.ServerException;

import static com.rayconhealth.rayconhealth.database.UserDao.ADMIN_ROLE;

public class LogController {
    private static final String PAGE_URL = "/log.jsp";
    private static LogController instance = null;

    private LogController() {}

    public static LogController getInstance() {
        if (instance == null) {
            instance = new LogController();
        }
        return instance;
    }

    public void index(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            User user = (User) request.getSession().getAttribute("user");
            if (!request.getMethod().equals("GET") && user.getRole().equals(ADMIN_ROLE)) {
                throw new ServletException();
            }
            Integer id = null;
            try {
                id = Integer.parseInt(request.getParameter("id"));
            } catch (NumberFormatException e) {
                throw new ServletException();
            }
            Log log = LogDao.getInstance().getById(id);
            request.setAttribute("log", log);
            request.getRequestDispatcher(PAGE_URL).forward(request, response);
        } catch (ServletException e) {
            response.sendError(404);
        } catch (RuntimeException e) {
            LoggingFacade.makeLogFatal(e.getMessage());
        }
    }

    public void deleteLog(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            User user = (User) request.getSession().getAttribute("user");
            if (!request.getMethod().equals("GET") && user.getRole().equals(ADMIN_ROLE)) {
                throw new ServletException();
            }
            Integer id = null;
            try {
                id = Integer.parseInt(request.getParameter("id"));
            } catch (NumberFormatException e) {
                throw new ServletException();
            }
            Log log = LogDao.getInstance().getById(id);
            LogDao.getInstance().delete(log);
            response.sendRedirect("/logs");
        } catch (ServletException e) {
            response.sendError(404);
        } catch (RuntimeException e) {
            LoggingFacade.makeLogFatal(e.getMessage());
        }
    }
}
