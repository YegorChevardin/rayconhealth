package com.rayconhealth.rayconhealth.controllers;

import com.rayconhealth.rayconhealth.database.UserDao;
import com.rayconhealth.rayconhealth.facades.AuthenticationFacade;
import com.rayconhealth.rayconhealth.models.User;
import com.rayconhealth.rayconhealth.models.classes.Email;
import com.rayconhealth.rayconhealth.models.classes.Password;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Date;

import static com.rayconhealth.rayconhealth.database.UserDao.DEFAULT_USER_CATEGORY;
import static com.rayconhealth.rayconhealth.database.UserDao.DEFAULT_USER_ROLE;

public class RegistrationController {
    private static final String PAGE_URL = "/register.jsp";
    private static RegistrationController instance = null;
    private RegistrationController() {}

    public static RegistrationController getInstance() {
        if (instance == null) {
            instance = new RegistrationController();
        }
        return instance;
    }

    public void index(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            if (request.getMethod().equals("GET")) {
                request.getRequestDispatcher(PAGE_URL).forward(request, response);
            } else {
                throw new ServletException("Wrong method operation!");
            }
        } catch (ServletException e) {
            response.sendError(404);
        }
    }

    public void register(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try {
            if (request.getMethod().equals("POST")) {
                String name = request.getParameter("name");
                String secondName = request.getParameter("second-name");
                String phone = request.getParameter("phone");
                Email email = Email.generateEmail(request.getParameter("email"));
                Password password = Password.generatePassword(request.getParameter("password"), false);
                String[] birthDayDate = request.getParameter("birth-day").split("-");
                Date birthDay = new Date(
                        Integer.parseInt(birthDayDate[0]) - 1900,
                        Integer.parseInt(birthDayDate[1]) - 1,
                        Integer.parseInt(birthDayDate[2])
                );

                User user = new User(name, secondName, birthDay, email, phone, password, DEFAULT_USER_ROLE, DEFAULT_USER_CATEGORY);

                AuthenticationFacade.register(request, user);
                response.sendRedirect("/system");
            } else {
                throw new ServletException();
            }
        } catch (ServletException | IOException e) {
            response.sendError(405, "Wrong method operation");
        } catch (SQLException e) {
            throw new ServletException("Email already exists", e);
        }
    }
}
