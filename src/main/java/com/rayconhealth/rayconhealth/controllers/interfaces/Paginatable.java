package com.rayconhealth.rayconhealth.controllers.interfaces;

import jakarta.servlet.http.HttpServletRequest;

import static com.rayconhealth.rayconhealth.database.pereferences.DatabasePreferences.PAGINATION_MAX_PAGE;

public interface Paginatable {
    /**
     * Method to prepare request with list for paginated elements
     * @param request http user request get needed data and prepare it
     */
    static Integer getPaginationPage(HttpServletRequest request) {
        Integer page = null;
        try {
            page = Integer.parseInt(request.getParameter("page"));
        } catch (NumberFormatException e) {
            page = 1;
        }
        request.setAttribute("currentPage", page);
        if (page != 1) {
            page--;
            page = page * PAGINATION_MAX_PAGE - 1;
        }
        return page;
    }
}
