<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ page contentType="text/html; UTF-8" %>
<!DOCTYPE html>
<html lang="${sessionScope.userSettings.getLocate()}">
<head>
    <title><my:app-name/> system - hospital card</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Portal - Bootstrap 5 Admin Dashboard Template For Developers">
    <meta name="author" content="Xiaoying Riley at 3rd Wave Media">
    <%@ include file="templates/system-styles.jspf" %>
</head>
<body class="app">
<%@ include file="templates/system-navbar.jspf" %>
<div id="htmlContent" class="app-wrapper">
    <div class="container-xl mb-5 mt-5">
        <h1 class="app-page-title">${user.getName()}'s hospital card</h1>
        <div class="row gy-4 justify-content-center align-items-center">
            <div class="col-12 col-lg-6">
                <div class="app-card app-card-account shadow-sm d-flex flex-column align-items-start">
                    <div class="app-card-header p-3 border-bottom-0">
                        <div class="row align-items-center gx-3">
                            <div class="col-auto">
                                <div class="app-icon-holder">
                                    <img alt="${user.getName()}'s avatar" class="border-radius overflow-hidden" src="./dist/assets/user-picture.png" style="width: 2em; height: 2em;"/>
                                </div><!--//icon-holder-->

                            </div><!--//col-->
                            <div class="col-auto">
                                <h4 class="app-card-title">Hospital card information</h4>
                            </div><!--//col-->
                        </div><!--//row-->
                    </div><!--//app-card-header-->
                    <div class="app-card-body px-4 w-100">
                        <div class="item border-bottom py-3">
                            <div class="row justify-content-between align-items-center">
                                <div class="col-auto">
                                    <div class="item-label"><strong>Card Name</strong></div>
                                    <div class="item-data">${hospitalCard.getName()}</div>
                                </div><!--//col-->
                            </div><!--//row-->
                        </div><!--//item-->
                        <div class="item border-bottom py-3">
                            <div class="row justify-content-between align-items-center">
                                <div class="col-auto">
                                    <div class="item-label"><strong>Created at</strong></div>
                                    <div class="item-data">${hospitalCard.getCreatedAt()}</div>
                                </div><!--//col-->
                            </div><!--//row-->
                        </div><!--//item-->
                        <my:user-information/>
                    </div><!--//app-card-body-->
                </div><!--//app-card-->
            </div><!--//col-->
        </div><!--//col-->
    </div><!--//row-->
    <hr/>
    <c:if test="${user.getRole() == 'doctor'}">
        <div class="container-xl">
            <div class="row g-3 mb-4 align-items-center justify-content-center">
                <div class="col-md-6 text-center">
                    <div id="addDiagnos" class="position-relative mfp-hide alert alert-success mx-auto col-lg-5 border-radius shadow-lg p-5 m-5" role="alert">
                        <h4 class="alert-heading">Adding diagnos</h4>
                        <form action="${pageContext.request.contextPath}/diagnos" method="post" autocomplete="on">
                            <input type="hidden" name="userId" value="${hospitalCard.getUser().getId()}"/>
                            <div class="mb-3">
                                <label for="status">Select diagnos status</label>
                                <select name="status" required id="status" class="form-select">
                                    <c:forEach var="status" items="${diagnosStatuses}">
                                        <option value="${status}">${status}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="name" class="mb-1">Diagnos name</label>
                                <input id="name" placeholder="Please, type here name for diagnos" class="form-control" name="name" required type="text"/>
                            </div>
                            <button type="submit" class="btn btn-success text-white" name="addDiagnos" value="addDiagnos">Add diagnos</button>
                        </form>
                    </div>
                    <button class="mb-3 btn text-white btn-success" id="addDiagnosButton">Add diagnos</button>
                </div>
            </div>
        </div>
    </c:if>
    <div class="container-xl">
        <div class="row g-3 mb-4 align-items-center justify-content-between">
            <div class="col-auto">
                <h1 class="app-page-title mb-0">Diagnosis</h1>
            </div>
        </div><!--//row-->
        <div class="tab-content" id="orders-table-tab-content">
            <div class="tab-pane fade show active" id="orders-all" role="tabpanel" aria-labelledby="orders-all-tab">
                <div class="app-card app-card-orders-table shadow-sm mb-5">
                    <div class="app-card-body">
                        <c:choose>
                            <c:when test="${diagnosis.size() > 0}">
                                <div class="table-responsive">
                                    <table class="table app-table-hover mb-0 text-left">
                                        <thead>
                                        <tr>
                                            <th class="cell">name</th>
                                            <th class="cell">status</th>
                                            <th class="cell">doctor name</th>
                                            <c:if test="${user.getRole() == 'doctor'}">
                                                <th class="cell"></th>
                                            </c:if>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <c:forEach var="diagnos" items="${diagnosis}">
                                            <tr>
                                                <td class="cell"><span class="truncate">${diagnos.getName()}</span>
                                                </td>
                                                <td class="cell">${diagnos.getDiagnosStatus()}</td>
                                                <td class="cell">${diagnos.getDoctor().getName()}</td>
                                                <c:if test="${user.getRole() == 'doctor'}">
                                                    <th class="cell"><a href="${pageContext.request.contextPath}/diagnos?id=${diagnos.getId()}&user_id=${hospitalCard.getUser().getId()}" class="btn-sm app-btn-secondary">delete diagnos</a></th>
                                                </c:if>
                                            </tr>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                </div><!--//table-responsive-->
                            </c:when>
                            <c:otherwise>
                                <h3 class="m-3">No diagnosis yet</h3>
                            </c:otherwise>
                        </c:choose>
                    </div><!--//app-card-body-->
                </div><!--//app-card-->
            </div><!--//tab-pane-->
        </div><!--//tab-content-->
    </div><!--//container-fluid-->
</div><!--//app-wrapper-->
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-md-12 d-flex flex-column align-items-center justify-content-center">
            <button id="hospitalCardPDF" class="mb-3 btn text-white btn-success">Extract to PDF</button>
            <div id="editor" class="d-none"></div>
        </div>
    </div>
</div>
<%@ include file="templates/system-scripts.jspf" %>
</body>
</html>