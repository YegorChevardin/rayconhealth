<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ page contentType="text/html; UTF-8" %>
<!DOCTYPE html>
<html lang="${sessionScope.userSettings.getLocate()}">
<head>
    <title><my:app-name/> system - appointment</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Portal - Bootstrap 5 Admin Dashboard Template For Developers">
    <meta name="author" content="Xiaoying Riley at 3rd Wave Media">
    <%@ include file="templates/system-styles.jspf" %>
</head>
<body class="app">
<%@ include file="templates/system-navbar.jspf" %>
<div class="app-wrapper">
    <div class="app-content pt-3 p-md-3 p-lg-4">
        <div class="container-xl">
            <h1 class="app-page-title">${user.getName()}'s appointment</h1>
            <div class="row gy-4 justify-content-center align-self-center">
                <div class="col-12 col-lg-6">
                    <div class="app-card app-card-account shadow-sm d-flex flex-column align-items-start">
                        <div class="app-card-header p-3 border-bottom-0">
                            <div class="row align-items-center gx-3">
                                <div class="col-auto">
                                    <div class="app-icon-holder">
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-person"
                                             fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd"
                                                  d="M10 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"></path>
                                        </svg>
                                    </div><!--//icon-holder-->

                                </div><!--//col-->
                                <div class="col-auto">
                                    <h4 class="app-card-title">Appointment information</h4>
                                </div><!--//col-->
                                <div class="col-auto">
                                    <c:if test="${user.getRole() == 'doctor' || user.getRole() == 'nurse'}">
                                        <a href="${pageContext.request.contextPath}/delete-appointment?id=${appointment.getId()}" class="btn btn-danger btn-sm text-white">Delete appointment</a>
                                    </c:if>
                                </div>
                            </div><!--//row-->
                        </div><!--//app-card-header-->
                        <div class="app-card-body px-4 w-100">
                            <div class="item border-bottom py-3">
                                <div class="row justify-content-between align-items-center">
                                    <div class="col-auto">
                                        <div class="item-label"><strong>Meeting time</strong></div>
                                        <div class="item-data">${appointment.getMeetingDate()}</div>
                                    </div><!--//col-->
                                </div><!--//row-->
                            </div><!--//item-->
                            <div class="item border-bottom py-3">
                                <div class="row justify-content-between align-items-center">
                                    <div class="col-auto">
                                        <div class="item-label"><strong>Appointment type</strong></div>
                                        <div class="item-data">${appointment.getAppointmentType()}</div>
                                    </div><!--//col-->
                                </div><!--//row-->
                            </div><!--//item-->
                            <div class="item border-bottom py-3">
                                <div class="row justify-content-between align-items-center">
                                    <div class="col-auto">
                                        <div class="item-label"><strong>Doctor</strong></div>
                                        <div class="item-data">${appointment.getDoctor().getName()} ${appointment.getDoctor().getSecondName()} - <span class="text-muted">${appointment.getDoctor().getCategory()}</span></div>
                                    </div><!--//col-->
                                </div><!--//row-->
                            </div><!--//item-->
                            <div class="item border-bottom py-3">
                                <div class="row justify-content-between align-items-center">
                                    <div class="col-auto">
                                        <div class="item-label"><strong>Doctor's phone number</strong></div>
                                        <div class="item-data">${appointment.getDoctor().getPhone()}</div>
                                    </div><!--//col-->
                                </div><!--//row-->
                            </div><!--//item-->
                            <div class="item border-bottom py-3">
                                <div class="row justify-content-between align-items-center">
                                    <div class="col-auto">
                                        <div class="item-label"><strong>Doctor's email</strong></div>
                                        <div class="item-data">${appointment.getDoctor().getEmail().getValue()}</div>
                                    </div><!--//col-->
                                </div><!--//row-->
                            </div><!--//item-->
                            <div class="item border-bottom py-3">
                                <div class="row justify-content-between align-items-center">
                                    <div class="col-auto">
                                        <div class="item-label"><strong>Patient</strong></div>
                                        <div class="item-data">${appointment.getUser().getName()} ${appointment.getUser().getSecondName()}</div>
                                    </div><!--//col-->
                                    <c:if test="${user.equalsUser(appointment.getUser())}">
                                        <div class="col text-right">
                                            <a class="btn-sm app-btn-secondary" href="${pageContext.request.contextPath}/account">Go to your account</a>
                                        </div><!--//col-->
                                    </c:if>
                                </div><!--//row-->
                            </div><!--//item-->
                            <div class="item border-bottom py-3">
                                <div class="row justify-content-between align-items-center">
                                    <div class="col-auto">
                                        <div class="item-label"><strong>Users's phone number</strong></div>
                                        <div class="item-data">${appointment.getUser().getPhone()}</div>
                                    </div><!--//col-->
                                </div><!--//row-->
                            </div><!--//item-->
                            <div class="item border-bottom py-3">
                                <div class="row justify-content-between align-items-center">
                                    <div class="col-auto">
                                        <div class="item-label"><strong>Doctor's email</strong></div>
                                        <div class="item-data">${appointment.getUser().getEmail().getValue()}</div>
                                    </div><!--//col-->
                                </div><!--//row-->
                            </div><!--//item-->
                        </div><!--//app-card-body-->
                        <div class="app-card-footer p-4 mt-auto">
                            <a class="btn app-btn-secondary" href="${pageContext.request.contextPath}/hospital-card?id=${appointment.getHospitalCard().getId()}">Hospital Card</a>
                        </div><!--//app-card-footer-->
                    </div><!--//app-card-->
                </div><!--//col-->
            </div><!--//col-->
        </div><!--//row-->
    </div><!--//container-fluid-->
</div><!--//app-content-->
<%@ include file="templates/system-scripts.jspf" %>
</body>
</html>