<c:if test="${user.equalsUser(hospitalCard.getUser())}">
    <!-- currently usable only in hospital card -->
    <div class="item border-bottom py-3">
        <div class="row justify-content-between align-items-center">
            <div class="col-auto">
                <div class="item-label"><strong>Name</strong></div>
                <div class="item-data">${user.getName()} ${user.getSecondName()}</div>
            </div><!--//col-->
        </div><!--//row-->
    </div><!--//item-->
    <div class="item border-bottom py-3">
        <div class="row justify-content-between align-items-center">
            <div class="col-auto">
                <div class="item-label"><strong>Second name</strong></div>
                <div class="item-data">${user.getSecondName()}</div>
            </div><!--//col-->
        </div><!--//row-->
    </div><!--//item-->
    <div class="item border-bottom py-3">
        <div class="row justify-content-between align-items-center">
            <div class="col-auto">
                <div class="item-label"><strong>Birth day</strong></div>
                <div class="item-data">${user.getBirthDay()}</div>
            </div><!--//col-->
        </div><!--//row-->
    </div><!--//item-->
    <div class="item border-bottom py-3">
        <div class="row justify-content-between align-items-center">
            <div class="col-auto">
                <div class="item-label"><strong>Phone number</strong></div>
                <div class="item-data">${user.getPhone()}</div>
            </div><!--//col-->
        </div><!--//row-->
    </div><!--//item-->
</c:if>