<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<%@ page contentType="text/html; UTF-8" %>
<!DOCTYPE html>
<html lang="${sessionScope.userSettings.getLocate()}">
<head>
    <title><my:app-name/> system - users list</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <%@ include file="templates/system-styles.jspf"%>
</head>
<body class="app">
<%@ include file="templates/system-navbar.jspf"%>
<div class="app-wrapper">
    <div class="app-content pt-3 p-md-3 p-lg-4">
        <div class="container-xl">
            <c:if test="${user.getRole() == 'admin'}">
                <div class="row g-3 mb-4 align-items-center justify-content-between">
                    <div class="col-auto">
                        <div id="createAccount" class="position-relative mfp-hide alert alert-success mx-auto col-lg-5 border-radius shadow-lg p-5 m-5 text-center" role="alert">
                            <h4 class="alert-heading mb-1">Account creating</h4>
                            <form id="authForm" name="register-form" method="post" action="${pageContext.request.contextPath}/users-list" autocomplete="on">
                                <div class="row mb-3">
                                    <label for="name" class="col-md-4 col-form-label text-md-end">Name</label>
                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control" name="name" required>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="second-name" class="col-md-4 col-form-label text-md-end">Second name</label>
                                    <div class="col-md-6">
                                        <input id="second-name" type="text" class="form-control" name="second-name" required>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="birth-day" class="col-md-4 col-form-label text-md-end">Birth day</label>
                                    <div class="col-md-6">
                                        <input id="birth-day" type="date" class="form-control" name="birth-day" required>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="phone-number" class="col-md-4 col-form-label text-md-end">Phone number</label>
                                    <div class="col-md-6">
                                        <input id="phone-number" type="tel" class="form-control" name="phone" required>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="email" class="col-md-4 col-form-label text-md-end">Email</label>
                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control" name="email" required
                                               autocomplete="email">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="password" class="col-md-4 col-form-label text-md-end">Password</label>
                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control" name="password" required
                                               autocomplete="new-password">
                                    </div>
                                </div>
                                <div class="row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button id="submitButton" type="submit" name="submit" value="register" class="mt-1 btn btn-primary text-white">
                                            Register
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <button id="createAccountButton" class="btn btn-success text-white">Create new User</button>
                    </div>
                </div>
            </c:if>
            <div class="row g-3 mb-4 align-items-center justify-content-between">
                <div class="col-auto">
                    <h1 class="app-page-title mb-0">${type} list</h1>
                </div>
                <div class="col-auto">
                    <div class="page-utilities">
                        <div class="row g-2 justify-content-start justify-content-md-end align-items-center">
                            <div class="col-auto">
                                <form id="searchForm" class="table-search-form row gx-1 align-items-center">
                                    <div class="col-auto">
                                        <input type="search" id="search" name="search"
                                               class="form-control search-orders" placeholder="Search by name">
                                    </div>
                                    <div class="col-auto">
                                        <button type="submit" class="btn app-btn-secondary">Search</button>
                                    </div>
                                </form>
                            </div><!--//col-->
                            <div class="col-auto">
                                <c:choose>
                                    <c:when test="${type == 'users'}">
                                        <label>
                                            <select id="sortingSelectUsers" class="form-select w-auto">
                                                <option selected value="default">Default</option>
                                                <option value="birth">Date of birth</option>
                                                <option value="alphabet">Alphabet</option>
                                            </select>
                                        </label>
                                    </c:when>
                                    <c:when test="${type == 'doctors'}">
                                        <label>
                                            <select id="sortingSelectDoctors" class="form-select w-auto">
                                                <option selected value="default">Default</option>
                                                <option value="category">Category</option>
                                                <option value="alphabet">Alphabet</option>
                                                <option value="patients">Number of patients</option>
                                            </select>
                                        </label>
                                    </c:when>
                                    <c:otherwise></c:otherwise>
                                </c:choose>
                            </div>
                        </div><!--//row-->
                    </div><!--//table-utilities-->
                </div><!--//col-auto-->
            </div><!--//row-->
            <div class="tab-content" id="orders-table-tab-content">
                <div class="tab-pane fade show active" id="orders-all" role="tabpanel" aria-labelledby="orders-all-tab">
                    <div class="app-card app-card-orders-table shadow-sm mb-5">
                        <div class="app-card-body">
                            <c:choose>
                                <c:when test="${entityList.size() > 0}">
                                    <div class="table-responsive">
                                        <table class="table app-table-hover mb-0 text-left">
                                            <thead>
                                            <tr>
                                                <th class="cell">name</th>
                                                <th class="cell">second name</th>
                                                <th class="cell">birth day</th>
                                                <th class="cell">email</th>
                                                <th class="cell">phone</th>
                                                <c:if test="${type == 'doctors'}">
                                                    <th class="cell">category</th>
                                                    <th class="cell">number of patients</th>
                                                </c:if>
                                                <c:if test="${user.getRole() != 'user'}">
                                                    <th class="cell"></th>
                                                </c:if>
                                                <c:choose>
                                                    <c:when test="${myDoctors}">
                                                        <th class="cell"></th>
                                                    </c:when>
                                                    <c:when test="${myPatients}">
                                                        <th class="cell"></th>
                                                    </c:when>
                                                </c:choose>
                                            </tr>
                                            </thead>
                                            <tbody id="userElements">
                                            <c:forEach var="entity" items="${entityList}">
                                                <tr data-id="${entity.getId()}" data-category="${entity.getCategory()}" data-patients="${entity.getNumberOfPatients()}" data-name="${entity.getName()}" data-birth="${entity.getBirthDay()}">
                                                    <td class="cell">${entity.getName()}</td>
                                                    <td class="cell">${entity.getSecondName()}</td>
                                                    <td class="cell">${entity.getBirthDay()}</td>
                                                    <td class="cell">${entity.getEmail().getValue()}</td>
                                                    <td class="cell">${entity.getPhone()}</td>
                                                    <c:if test="${type == 'doctors'}">
                                                        <th class="cell">${entity.getCategory()}</th>
                                                        <th class="cell">${entity.getNumberOfPatients()}</th>
                                                    </c:if>
                                                    <c:if test="${user.getRole() != 'user'}">
                                                        <td class="cell"><a class="btn-sm app-btn-secondary" href="${pageContext.request.contextPath}/user?id=${entity.getId()}">View</a></td>
                                                    </c:if>
                                                    <c:choose>
                                                        <c:when test="${myDoctors}">
                                                            <td class="cell"><a class="btn-sm app-btn-secondary" href="${pageContext.request.contextPath}/delete-doctor?id=${entity.getId()}">Delete</a></td>
                                                        </c:when>
                                                        <c:when test="${myPatients}">
                                                            <td class="cell"><a class="btn-sm app-btn-secondary" href="${pageContext.request.contextPath}/delete-patient?id=${entity.getId()}">Delete</a></td>
                                                        </c:when>
                                                    </c:choose>
                                                </tr>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                    </div><!--//table-responsive-->
                                </c:when>
                                <c:otherwise>
                                    <h3 class="m-3">No upcoming patients yet</h3>
                                </c:otherwise>
                            </c:choose>
                        </div><!--//app-card-body-->
                    </div><!--//app-card-->
                </div><!--//tab-pane-->
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->
</div><!--//app-wrapper-->
<%@ include file="templates/system-scripts.jspf"%>
</body>
</html>