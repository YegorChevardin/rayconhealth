<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ page contentType="text/html; UTF-8" %>
<!DOCTYPE html>
<html lang="${sessionScope.userSettings.getLocate()}">
<head>
    <title><my:app-name/> system - welcome</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Portal - Bootstrap 5 Admin Dashboard Template For Developers">
    <meta name="author" content="Xiaoying Riley at 3rd Wave Media">
    <%@ include file="templates/system-styles.jspf" %>
</head>
<body class="app">
<%@ include file="templates/system-navbar.jspf" %>
<div class="app-wrapper">
    <div class="app-content pt-3 p-md-3 p-lg-4">
        <div class="container-xl">
            <h1 class="app-page-title">Hello, ${sessionScope.get('user').getName()}! <span
                    class="text-muted"> - ${sessionScope.get('user').getRole()}</span></h1>
            <div class="row justify-content-around g-4 mb-4">
                <c:choose>
                    <c:when test="${sessionScope.get('user').getRole() == 'admin'}">
                        <div class="col-6 col-lg-3">
                            <div class="app-card app-card-stat shadow-sm h-100">
                                <div class="app-card-body p-3 p-lg-4">
                                    <h4 class="stats-type mb-1">Upcoming Patients</h4>
                                    <div class="stats-figure">${upcomingPatients}</div>
                                </div><!--//app-card-body-->
                                <a class="app-card-link-mask" href="${pageContext.request.contextPath}/enrolls"></a>
                            </div><!--//app-card-->
                        </div>
                        <!--//col-->
                        <div class="col-6 col-lg-3">
                            <div class="app-card app-card-stat shadow-sm h-100">
                                <div class="app-card-body p-3 p-lg-4">
                                    <h4 class="stats-type mb-1">Total doctors</h4>
                                    <div class="stats-figure">${totalDoctors}</div>
                                </div><!--//app-card-body-->
                                <a class="app-card-link-mask" href="${pageContext.request.contextPath}/doctors-list"></a>
                            </div><!--//app-card-->
                        </div>
                        <!--//col-->
                        <div class="col-6 col-lg-3">
                            <div class="app-card app-card-stat shadow-sm h-100">
                                <div class="app-card-body p-3 p-lg-4">
                                    <h4 class="stats-type mb-1">Total admins</h4>
                                    <div class="stats-figure">${totalAdmins}</div>
                                </div><!--//app-card-body-->
                                <a class="app-card-link-mask" href="${pageContext.request.contextPath}/admins-list"></a>
                            </div><!--//app-card-->
                        </div>
                        <!--//col-->
                        <div class="row g-4 mb-4">
                            <div class="col-12 col-lg-4">
                                <div class="app-card app-card-basic d-flex flex-column align-items-start shadow-sm">
                                    <div class="app-card-header p-3 border-bottom-0">
                                        <div class="row align-items-center gx-3">
                                            <div class="col-auto">
                                                <div class="app-icon-holder">
                                                    <svg width="1em" height="1em" viewBox="0 0 16 16"
                                                         class="bi bi-receipt"
                                                         fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd"
                                                              d="M1.92.506a.5.5 0 0 1 .434.14L3 1.293l.646-.647a.5.5 0 0 1 .708 0L5 1.293l.646-.647a.5.5 0 0 1 .708 0L7 1.293l.646-.647a.5.5 0 0 1 .708 0L9 1.293l.646-.647a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .801.13l.5 1A.5.5 0 0 1 15 2v12a.5.5 0 0 1-.053.224l-.5 1a.5.5 0 0 1-.8.13L13 14.707l-.646.647a.5.5 0 0 1-.708 0L11 14.707l-.646.647a.5.5 0 0 1-.708 0L9 14.707l-.646.647a.5.5 0 0 1-.708 0L7 14.707l-.646.647a.5.5 0 0 1-.708 0L5 14.707l-.646.647a.5.5 0 0 1-.708 0L3 14.707l-.646.647a.5.5 0 0 1-.801-.13l-.5-1A.5.5 0 0 1 1 14V2a.5.5 0 0 1 .053-.224l.5-1a.5.5 0 0 1 .367-.27zm.217 1.338L2 2.118v11.764l.137.274.51-.51a.5.5 0 0 1 .707 0l.646.647.646-.646a.5.5 0 0 1 .708 0l.646.646.646-.646a.5.5 0 0 1 .708 0l.646.646.646-.646a.5.5 0 0 1 .708 0l.646.646.646-.646a.5.5 0 0 1 .708 0l.646.646.646-.646a.5.5 0 0 1 .708 0l.509.509.137-.274V2.118l-.137-.274-.51.51a.5.5 0 0 1-.707 0L12 1.707l-.646.647a.5.5 0 0 1-.708 0L10 1.707l-.646.647a.5.5 0 0 1-.708 0L8 1.707l-.646.647a.5.5 0 0 1-.708 0L6 1.707l-.646.647a.5.5 0 0 1-.708 0L4 1.707l-.646.647a.5.5 0 0 1-.708 0l-.509-.51z"></path>
                                                        <path fill-rule="evenodd"
                                                              d="M3 4.5a.5.5 0 0 1 .5-.5h6a.5.5 0 1 1 0 1h-6a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 1 1 0 1h-6a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 1 1 0 1h-6a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5zm8-6a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5z"></path>
                                                    </svg>
                                                </div><!--//icon-holder-->

                                            </div><!--//col-->
                                            <div class="col-auto">
                                                <h4 class="app-card-title">Look to users account</h4>
                                            </div><!--//col-->
                                        </div><!--//row-->
                                    </div><!--//app-card-header-->
                                    <div class="app-card-body px-4">
                                        <div class="intro">
                                            Check all users accounts, sort them and see their meetings and diagnosis
                                        </div>
                                    </div><!--//app-card-body-->
                                    <div class="app-card-footer p-4 mt-auto">
                                        <a class="btn app-btn-secondary"
                                           href="${pageContext.request.contextPath}/users-list">Go
                                            to users list</a>
                                    </div><!--//app-card-footer-->
                                </div><!--//app-card-->
                            </div><!--//col-->
                            <div class="col-12 col-lg-4">
                                <div class="app-card app-card-basic d-flex flex-column align-items-start shadow-sm">
                                    <div class="app-card-header p-3 border-bottom-0">
                                        <div class="row align-items-center gx-3">
                                            <div class="col-auto">
                                                <div class="app-icon-holder">
                                                    <svg width="1em" height="1em" viewBox="0 0 16 16"
                                                         class="bi bi-receipt"
                                                         fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd"
                                                              d="M1.92.506a.5.5 0 0 1 .434.14L3 1.293l.646-.647a.5.5 0 0 1 .708 0L5 1.293l.646-.647a.5.5 0 0 1 .708 0L7 1.293l.646-.647a.5.5 0 0 1 .708 0L9 1.293l.646-.647a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .801.13l.5 1A.5.5 0 0 1 15 2v12a.5.5 0 0 1-.053.224l-.5 1a.5.5 0 0 1-.8.13L13 14.707l-.646.647a.5.5 0 0 1-.708 0L11 14.707l-.646.647a.5.5 0 0 1-.708 0L9 14.707l-.646.647a.5.5 0 0 1-.708 0L7 14.707l-.646.647a.5.5 0 0 1-.708 0L5 14.707l-.646.647a.5.5 0 0 1-.708 0L3 14.707l-.646.647a.5.5 0 0 1-.801-.13l-.5-1A.5.5 0 0 1 1 14V2a.5.5 0 0 1 .053-.224l.5-1a.5.5 0 0 1 .367-.27zm.217 1.338L2 2.118v11.764l.137.274.51-.51a.5.5 0 0 1 .707 0l.646.647.646-.646a.5.5 0 0 1 .708 0l.646.646.646-.646a.5.5 0 0 1 .708 0l.646.646.646-.646a.5.5 0 0 1 .708 0l.646.646.646-.646a.5.5 0 0 1 .708 0l.646.646.646-.646a.5.5 0 0 1 .708 0l.509.509.137-.274V2.118l-.137-.274-.51.51a.5.5 0 0 1-.707 0L12 1.707l-.646.647a.5.5 0 0 1-.708 0L10 1.707l-.646.647a.5.5 0 0 1-.708 0L8 1.707l-.646.647a.5.5 0 0 1-.708 0L6 1.707l-.646.647a.5.5 0 0 1-.708 0L4 1.707l-.646.647a.5.5 0 0 1-.708 0l-.509-.51z"></path>
                                                        <path fill-rule="evenodd"
                                                              d="M3 4.5a.5.5 0 0 1 .5-.5h6a.5.5 0 1 1 0 1h-6a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 1 1 0 1h-6a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 1 1 0 1h-6a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5zm8-6a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5z"></path>
                                                    </svg>
                                                </div><!--//icon-holder-->

                                            </div><!--//col-->
                                            <div class="col-auto">
                                                <h4 class="app-card-title">Doctors list</h4>
                                            </div><!--//col-->
                                        </div><!--//row-->
                                    </div><!--//app-card-header-->
                                    <div class="app-card-body px-4">
                                        <div class="intro">
                                            List of all doctors in your hospital. Look at their patients, sort them
                                        </div>
                                    </div><!--//app-card-body-->
                                    <div class="app-card-footer p-4 mt-auto">
                                        <a class="btn app-btn-secondary"
                                           href="${pageContext.request.contextPath}/doctors-list">Go
                                            to doctors list</a>
                                    </div><!--//app-card-footer-->
                                </div><!--//app-card-->
                            </div><!--//col-->
                            <div class="col-12 col-lg-4">
                                <div class="app-card app-card-basic d-flex flex-column align-items-start shadow-sm">
                                    <div class="app-card-header p-3 border-bottom-0">
                                        <div class="row align-items-center gx-3">
                                            <div class="col-auto">
                                                <div class="app-icon-holder">
                                                    <svg width="1em" height="1em" viewBox="0 0 16 16"
                                                         class="bi bi-receipt"
                                                         fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd"
                                                              d="M1.92.506a.5.5 0 0 1 .434.14L3 1.293l.646-.647a.5.5 0 0 1 .708 0L5 1.293l.646-.647a.5.5 0 0 1 .708 0L7 1.293l.646-.647a.5.5 0 0 1 .708 0L9 1.293l.646-.647a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .801.13l.5 1A.5.5 0 0 1 15 2v12a.5.5 0 0 1-.053.224l-.5 1a.5.5 0 0 1-.8.13L13 14.707l-.646.647a.5.5 0 0 1-.708 0L11 14.707l-.646.647a.5.5 0 0 1-.708 0L9 14.707l-.646.647a.5.5 0 0 1-.708 0L7 14.707l-.646.647a.5.5 0 0 1-.708 0L5 14.707l-.646.647a.5.5 0 0 1-.708 0L3 14.707l-.646.647a.5.5 0 0 1-.801-.13l-.5-1A.5.5 0 0 1 1 14V2a.5.5 0 0 1 .053-.224l.5-1a.5.5 0 0 1 .367-.27zm.217 1.338L2 2.118v11.764l.137.274.51-.51a.5.5 0 0 1 .707 0l.646.647.646-.646a.5.5 0 0 1 .708 0l.646.646.646-.646a.5.5 0 0 1 .708 0l.646.646.646-.646a.5.5 0 0 1 .708 0l.646.646.646-.646a.5.5 0 0 1 .708 0l.646.646.646-.646a.5.5 0 0 1 .708 0l.509.509.137-.274V2.118l-.137-.274-.51.51a.5.5 0 0 1-.707 0L12 1.707l-.646.647a.5.5 0 0 1-.708 0L10 1.707l-.646.647a.5.5 0 0 1-.708 0L8 1.707l-.646.647a.5.5 0 0 1-.708 0L6 1.707l-.646.647a.5.5 0 0 1-.708 0L4 1.707l-.646.647a.5.5 0 0 1-.708 0l-.509-.51z"></path>
                                                        <path fill-rule="evenodd"
                                                              d="M3 4.5a.5.5 0 0 1 .5-.5h6a.5.5 0 1 1 0 1h-6a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 1 1 0 1h-6a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 1 1 0 1h-6a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5zm8-6a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5z"></path>
                                                    </svg>
                                                </div><!--//icon-holder-->

                                            </div><!--//col-->
                                            <div class="col-auto">
                                                <h4 class="app-card-title">List of admins</h4>
                                            </div><!--//col-->
                                        </div><!--//row-->
                                    </div><!--//app-card-header-->
                                    <div class="app-card-body px-4">
                                        <div class="intro">Look at people, who are working with you</div>
                                    </div><!--//app-card-body-->
                                    <div class="app-card-footer p-4 mt-auto">
                                        <a class="btn app-btn-secondary"
                                           href="${pageContext.request.contextPath}/admins-list">Go
                                            to admins list</a>
                                    </div><!--//app-card-footer-->
                                </div><!--//app-card-->
                            </div><!--//col-->
                        </div>
                        <!--//row-->
                    </c:when>
                    <c:when test="${sessionScope.get('user').getRole() == 'doctor' || sessionScope.get('user').getRole() == 'nurse'}">
                        <div class="col-6 col-lg-6">
                            <div class="app-card app-card-stat shadow-sm h-100">
                                <div class="app-card-body p-3 p-lg-4">
                                    <h4 class="stats-type mb-1">Your Patients</h4>
                                    <div class="stats-figure">${doctorPatients}</div>
                                </div><!--//app-card-body-->
                                <a class="app-card-link-mask" href="${pageContext.request.contextPath}/patients-list"></a>
                            </div><!--//app-card-->
                        </div>
                        <!--//col-->
                        <div class="col-6 col-lg-6">
                            <div class="app-card app-card-stat shadow-sm h-100">
                                <div class="app-card-body p-3 p-lg-4">
                                    <h4 class="stats-type mb-1">Your appointments</h4>
                                    <div class="stats-figure">${appointmentsCount}</div>
                                </div><!--//app-card-body-->
                                <a class="app-card-link-mask" href="${pageContext.request.contextPath}/doctor-appointments"></a>
                            </div><!--//app-card-->
                        </div>
                        <!--//col-->
                    </c:when>
                    <c:otherwise>
                        <div class="col-6 col-lg-6">
                            <div class="app-card app-card-stat shadow-sm h-100">
                                <div class="app-card-body p-3 p-lg-4">
                                    <h4 class="stats-type mb-1">Your Doctors</h4>
                                    <div class="stats-figure">${userDoctors}</div>
                                </div><!--//app-card-body-->
                                <a class="app-card-link-mask" href="${pageContext.request.contextPath}/my-doctors"></a>
                            </div><!--//app-card-->
                        </div>
                        <!--//col-->
                        <div class="col-6 col-lg-6">
                            <div class="app-card app-card-stat shadow-sm h-100">
                                <div class="app-card-body p-3 p-lg-4">
                                    <h4 class="stats-type mb-1">Your appointments</h4>
                                    <div class="stats-figure">${userAppointmentsCount}</div>
                                </div><!--//app-card-body-->
                                <a class="app-card-link-mask" href="${pageContext.request.contextPath}/appointments"></a>
                            </div><!--//app-card-->
                        </div>
                        <!--//col-->
                    </c:otherwise>
                </c:choose>
            </div>
        </div><!--//container-fluid-->
        <hr/>
        <div class="container-xl mt-5">
            <div class="row justify-content-center align-items-center">
                <div class="col-md-6 text-center">
                    <h4 class="mb-3">To book an appointment - tell us what hearts you</h4>
                    <form action="${pageContext.request.contextPath}/system" name="doctor-enroll" autocomplete="on"
                          method="post">
                        <div class="mb-3">
                            <textarea name="pain-description" required placeholder="Describe your pain..."
                                      class="form-control" id="pain-textarea" rows="3" cols="2"></textarea>
                        </div>
                        <input type="submit" value="enroll to appointment" name="enroll-to-appointment"
                               class="btn btn-success border-radius text-white"/>
                    </form>
                </div>
            </div>
        </div>
    </div><!--//app-content-->
</div><!--//app-wrapper-->
<%@ include file="templates/system-scripts.jspf" %>
</body>
</html>