<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ page contentType="text/html; UTF-8" %>
<!DOCTYPE html>
<html lang="${sessionScope.userSettings.getLocate()}">
<head>
    <title><my:app-name/> system - welcome</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Portal - Bootstrap 5 Admin Dashboard Template For Developers">
    <meta name="author" content="Xiaoying Riley at 3rd Wave Media">
    <%@ include file="templates/system-styles.jspf" %>
</head>
<body class="app">
<%@ include file="templates/system-navbar.jspf" %>
<div class="app-wrapper">
    <div class="app-content pt-3 p-md-3 p-lg-4">
        <div class="container-xl">
            <h1 class="app-page-title">Hello, ${sessionScope.get('user').getName()}! <span
                    class="text-muted"> - ${sessionScope.get('user').getRole()}</span></h1>
            <div class="row justify-content-center align-items-centerg-4 mb-4">
                <div class="col-12 col-lg-4">
                    <div class="app-card app-card-account shadow-sm d-flex flex-column align-items-start">
                        <div class="app-card-header p-3 border-bottom-0">
                            <div class="row align-items-center gx-3">
                                <div class="col-auto">
                                    <div class="app-icon-holder">
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-person"
                                             fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd"
                                                  d="M10 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"></path>
                                        </svg>
                                    </div><!--//icon-holder-->

                                </div><!--//col-->
                                <div class="col-auto">
                                    <h4 class="app-card-title">Enroll information</h4>
                                </div><!--//col-->
                            </div><!--//row-->
                        </div><!--//app-card-header-->
                        <div class="app-card-body px-4 w-100">
                            <div class="item border-bottom py-3">
                                <div class="row justify-content-between align-items-center">
                                    <div class="col-auto">
                                        <div class="item-label"><strong>Enroll message</strong></div>
                                        <div class="item-data">${enroll.getText()}</div>
                                    </div><!--//col-->
                                </div><!--//row-->
                            </div><!--//item-->
                            <div class="item border-bottom py-3">
                                <div class="row justify-content-between align-items-center">
                                    <div class="col-auto">
                                        <div class="item-label"><strong>User initials</strong></div>
                                        <div class="item-data">${enroll.getUser().getName()} ${enroll.getUser().getSecondName()}</div>
                                    </div><!--//col-->
                                </div><!--//row-->
                            </div><!--//item-->
                            <div class="item border-bottom py-3">
                                <div class="row justify-content-between align-items-center">
                                    <div class="col-auto">
                                        <div class="item-label"><strong>User phone number and email</strong></div>
                                        <div class="item-data">${enroll.getUser().getPhone()}, ${enroll.getUser().getEmail().getValue()}</span></div>
                                    </div><!--//col-->
                                </div><!--//row-->
                            </div><!--//item-->
                        </div><!--//app-card-body-->
                        <hr/>
                        <div class="app-card-footer p-4 mt-auto">
                            <p class="text-muted">To enroll this patient to a doctor - go to his page</p>
                            <a class="btn app-btn-secondary" href="${pageContext.request.contextPath}/user?id=${enroll.getUser().getId()}">Go to user's account</a>
                        </div><!--//app-card-footer-->
                    </div><!--//app-card-->
                </div><!--//col-->
            </div><!--//row-->
        </div><!--//container-fluid-->
        <hr/>
        <div class="container-xl mt-5">
            <div class="row justify-content-center align-items-center">
                <div class="col-md-6 text-center">
                    <h4 class="mb-3">Enroll this patient to a doctor</h4>
                    <form action="${pageContext.request.contextPath}/enroll" name="doctor-enroll" autocomplete="on"
                          method="post">
                        <input type="hidden" name="user-email" value="${enroll.getUser().getEmail().getValue()}">
                        <input type="hidden" name="enroll-id" value="${enroll.getId()}">
                        <div class="mb-3">
                            <label for="doctor-type">Select a type of doctor</label>
                            <select name="doctor-type" required id="doctor-type" class="form-select">
                                <c:forEach var="category" items="${categories}">
                                    <option value="${category}">${category}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="doctor-type">Select a doctor</label>
                            <select disabled id="doctors-select" name="doctor-email" required class="form-select"></select>
                        </div>
                        <button disabled id="enrollSubmit" type="submit" name="operation" value="enroll" class="btn btn-success border-radius text-white">Sing up user</button>
                    </form>
                </div>
            </div>
        </div>
    </div><!--//app-content-->
</div><!--//app-wrapper-->
<%@ include file="templates/system-scripts.jspf" %>
</body>
</html>