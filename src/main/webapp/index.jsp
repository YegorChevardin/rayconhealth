<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<%@ page contentType="text/html; UTF-8" %>
<!DOCTYPE html>
<html lang="${sessionScope.userSettings.getLocate()}">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="Raycon Health - medicare website"/>
        <meta name="author" content="Yegor Chevardin"/>
        <title><my:app-name/> - health care system</title>
        <%@ include file="templates/styles.jspf"%>
    </head>
    <body>
        <%@ include file="templates/navbar.jspf"%>
        <header>
            <button class="d-none" id="cookiePopupButton">confirm cookie</button>
            <div id="cookieAlert" class="position-relative mfp-hide alert alert-success mx-auto col-lg-5 border-radius shadow-lg p-5 m-5 text-center" role="alert">
                <h4 class="alert-heading">Cookie policy</h4>
                <p>Aww yeah, we forgot to mention that we use cookies.</p>
                <hr>
                <p class="mb-0">If you use our application, and you are on our website - we assume that you agree to our cookie policy.</p>
            </div>
            <div class="overlay"></div>
            <video src="./dist/assets/header-video.mp4" playsinline autoplay muted loop></video>
            <!-- The header content -->
            <div class="container h-100">
                <div class="d-flex h-100 text-center align-items-center">
                    <div class="w-100 text-white wow bounceIn">
                        <c:choose>
                            <c:when test="${sessionScope.get('user') != null}">
                                <h1 class="display-5">Hi, ${user.getName()}</h1>
                                <c:choose>
                                    <c:when test="${user.getRole() == 'admin'}">
                                        <h1 class="mb-5">Check, if you have new upcoming patients</h1>
                                        <a href="${pageContext.request.contextPath}/enrolls" class="btn btn-lg btn-success">Check upcoming patients</a>
                                    </c:when>
                                    <c:when test="${user.getRole() == 'doctor' || user.getRole() == 'nurse'}">
                                        <h1 class="mb-5">Check, if you have new appointments</h1>
                                        <a href="${pageContext.request.contextPath}/doctor-appointments" class="btn btn-lg btn-success">Go to your appointments</a>
                                    </c:when>
                                    <c:otherwise>
                                        <h1 class="mb-5">Something hearts?</h1>
                                        <a href="${pageContext.request.contextPath}/system" class="btn btn-lg btn-success">Book and appointment</a>
                                    </c:otherwise>
                                </c:choose>
                            </c:when>
                            <c:otherwise>
                                <h1 class="display-5">Raycon Health</h1>
                                <h1 class="mb-5">Book an appointment, staying at home</h1>
                                <a href="${pageContext.request.contextPath}/login" class="btn btn-lg btn-success">Sing up for a meeting</a>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>
        </header>
        <!-- Icons Grid-->
        <section class="features-icons bg-light text-center">
            <h1 class="mb-5 wow bounceInUp" data-wow-delay="0.5s">What we are providing?</h1>
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3 wow bounceInUp" data-wow-delay="0.5s">
                            <div class="features-icons-icon d-flex"><i class="bi-calendar m-auto text-primary"></i></div>
                            <h3>COVID-19 vaccination</h3>
                            <p class="lead mb-0">Witihn our system you are able to sing up for COVID-19 vaccination, taking a free consultation with doctor before it.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3 wow bounceInUp" data-wow-delay="1s">
                            <div class="features-icons-icon d-flex"><i class="bi-files m-auto text-primary"></i></div>
                            <h3>Diagnosis</h3>
                            <p class="lead mb-0">In our system you are able to see all your diagnosis immediately after your doctor will place it.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="features-icons-item mx-auto mb-0 mb-lg-3 wow bounceInUp" data-wow-delay="1.5s">
                            <div class="features-icons-icon d-flex"><i class="bi-emoji-smile m-auto text-primary"></i></div>
                            <h3>Easy to Use</h3>
                            <p class="lead mb-0">You just need to sing up and press one button!</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Image Showcases-->
        <section class="showcase">
            <div class="container-fluid p-0">
                <div class="row g-0">
                    <div class="col-lg-6 order-lg-2 text-white showcase-img wow bounceInUp" style="background-image: url('./dist/assets/img/bg-showcase-1.webp')"></div>
                    <div class="col-lg-6 order-lg-1 my-auto showcase-text wow bounceInUp" data-wow-delay="0.5s">
                        <h2>Medical assistance to refugees from Ukraine</h2>
                        <p class="lead mb-0">
                            Free emergency medical care for citizens of Ukraine and citizens of other nationalities who arrived from Ukraine after the start of hostilities.
                        </p>
                    </div>
                </div>
                <div class="row g-0">
                    <div class="col-lg-6 text-white showcase-img wow bounceInUp" style="background-image: url('./dist/assets/img/bg-showcase-2.jpg')"></div>
                    <div class="col-lg-6 my-auto showcase-text wow bounceInUp" data-wow-delay="0.5s">
                        <h2>You come first</h2>
                        <p class="lead mb-0">
                            Treatment at Mayo Clinic is a truly human experience. You're cared for as a person first.
                        </p>
                    </div>
                </div>
                <div class="row g-0" data-wow-delay="1s">
                    <div class="col-lg-6 order-lg-2 text-white showcase-img wow bounceInUp" style="background-image: url('./dist/assets/img/bg-showcase-3.jpg')"></div>
                    <div class="col-lg-6 order-lg-1 my-auto showcase-text wow bounceInUp" data-wow-delay="0.5s">
                        <h2>Innovation with impact</h2>
                        <p class="lead mb-0">
                            All of our patient care, education and research are driven to make discoveries that can help heal you.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <!-- Call to Action-->
        <section class="call-to-action text-white text-center wow bounceInUp" id="signup">
            <div class="container position-relative">
                <div class="row justify-content-center">
                    <div class="col-xl-6">
                        <h2 class="mb-4">Ready to get started? Enroll now!</h2>
                        <a href="${pageContext.request.contextPath}/system" class="btn btn-lg btn-success">Sing up for a meeting</a>
                    </div>
                </div>
            </div>
        </section>
        <%@ include file="templates/footer.jspf"%>
        <%@ include file="templates/scripts.jspf"%>
    </body>
</html>
