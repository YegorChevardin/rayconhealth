<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<%@ page contentType="text/html; UTF-8" %>
<!DOCTYPE html>
<html lang="${sessionScope.userSettings.getLocate()}">
<head>
    <title><my:app-name/> system - ${showUser}'s account</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Portal - Bootstrap 5 Admin Dashboard Template For Developers">
    <meta name="author" content="Xiaoying Riley at 3rd Wave Media">
    <%@ include file="templates/system-styles.jspf"%>
</head>
<body class="app">
<%@ include file="templates/system-navbar.jspf"%>
<div class="app-wrapper">
    <div class="app-content pt-3 p-md-3 p-lg-4">
        <div class="container-xl">
            <h1 class="app-page-title">${showUser.getName()}'s account</h1>
            <div class="row gy-4">
                <div class="col-12 col-lg-6">
                    <div class="app-card app-card-account shadow-sm d-flex flex-column align-items-start">
                        <div class="app-card-header p-3 border-bottom-0">
                            <div class="row align-items-center gx-3">
                                <div class="col-auto">
                                    <div class="app-icon-holder">
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-person"
                                             fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd"
                                                  d="M10 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"></path>
                                        </svg>
                                    </div><!--//icon-holder-->

                                </div><!--//col-->
                                <div class="col-auto">
                                    <h4 class="app-card-title">Profile</h4>
                                </div><!--//col-->
                            </div><!--//row-->
                        </div><!--//app-card-header-->
                        <div class="app-card-body px-4 w-100">
                            <div class="item border-bottom py-3">
                                <div class="row justify-content-between align-items-center">
                                    <div class="col-auto">
                                        <div class="item-label mb-2"><strong>Avatar</strong></div>
                                        <div class="item-data"><img class="profile-image" src="./dist/assets/user-picture.png" alt="${showUser.getName()}'s picture"></div>
                                    </div><!--//col-->
                                </div><!--//row-->
                            </div><!--//item-->
                            <div class="item border-bottom py-3">
                                <div class="row justify-content-between align-items-center">
                                    <div class="col-auto">
                                        <div class="item-label"><strong>Name</strong></div>
                                        <div class="item-data">${showUser.getName()} - <span class="text-muted">${showUser.getRole()}, ${showUser.getCategory()}</span></div>
                                    </div><!--//col-->
                                </div><!--//row-->
                            </div><!--//item-->
                            <div class="item border-bottom py-3">
                                <div class="row justify-content-between align-items-center">
                                    <div class="col-auto">
                                        <div class="item-label"><strong>Second name</strong></div>
                                        <div class="item-data">${showUser.getSecondName()}</div>
                                    </div><!--//col-->
                                </div><!--//row-->
                            </div><!--//item-->
                            <div class="item border-bottom py-3">
                                <div class="row justify-content-between align-items-center">
                                    <div class="col-auto">
                                        <div class="item-label"><strong>Birth day</strong></div>
                                        <div class="item-data">${showUser.getBirthDay()}</div>
                                    </div><!--//col-->
                                </div><!--//row-->
                            </div><!--//item-->
                            <div class="item border-bottom py-3">
                                <div class="row justify-content-between align-items-center">
                                    <div class="col-auto">
                                        <div class="item-label"><strong>Phone number</strong></div>
                                        <div class="item-data">${showUser.getPhone()}</div>
                                    </div><!--//col-->
                                </div><!--//row-->
                            </div><!--//item-->
                            <div class="item border-bottom py-3">
                                <div class="row justify-content-between align-items-center">
                                    <div class="col-auto">
                                        <div class="item-label"><strong>Email</strong></div>
                                        <div class="item-data">${showUser.getEmail().getValue()}</div>
                                    </div><!--//col-->
                                </div><!--//row-->
                            </div><!--//item-->
                        </div><!--//app-card-body-->
                        <c:if test="${sameUser || user.getRole() == 'nurse' || user.getRole() == 'doctor'}">
                            <div class="app-card-footer p-4 mt-auto">
                                <a class="btn app-btn-secondary" href="${pageContext.request.contextPath}/hospital-card?id=${hospitalCardId}">Hospital Card</a>
                            </div><!--//app-card-footer-->
                        </c:if>
                    </div><!--//app-card-->
                </div><!--//col-->
                <c:if test="${sameUser || user.getRole() == 'admin'}">
                    <div class="col-12 col-lg-6">
                        <div class="app-card app-card-account shadow-sm d-flex flex-column align-items-start">
                            <div class="app-card-header p-3 border-bottom-0">
                                <div class="row align-items-center gx-3">
                                    <div class="col-auto">
                                        <div class="app-icon-holder">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-sliders"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                      d="M11.5 2a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zM9.05 3a2.5 2.5 0 0 1 4.9 0H16v1h-2.05a2.5 2.5 0 0 1-4.9 0H0V3h9.05zM4.5 7a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zM2.05 8a2.5 2.5 0 0 1 4.9 0H16v1H6.95a2.5 2.5 0 0 1-4.9 0H0V8h2.05zm9.45 4a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zm-2.45 1a2.5 2.5 0 0 1 4.9 0H16v1h-2.05a2.5 2.5 0 0 1-4.9 0H0v-1h9.05z"></path>
                                            </svg>
                                        </div><!--//icon-holder-->
                                    </div><!--//col-->
                                    <div class="col-auto">
                                        <h4 class="app-card-title">Preferences</h4>
                                    </div><!--//col-->
                                </div><!--//row-->
                            </div><!--//app-card-header-->
                            <div id="editAccount" class="position-relative mfp-hide alert alert-success mx-auto col-lg-5 border-radius shadow-lg p-5 m-5 text-center" role="alert">
                                <h4 class="alert-heading mb-1">Account editing</h4>
                                <form method="post" autocomplete="on" action="${pageContext.request.contextPath}/edit-account">
                                    <input type="hidden" name="userEmail" value="${showUser.getEmail().getValue()}"/>
                                    <div class="row mb-3">
                                        <label for="name" class="col-md-4 col-form-label text-md-end">Name</label>
                                        <div class="col-md-6">
                                            <input id="name" type="text" class="form-control" name="name" value="${showUser.getName()}" required>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="second-name" class="col-md-4 col-form-label text-md-end">Second name</label>
                                        <div class="col-md-6">
                                            <input id="second-name" type="text" class="form-control" name="second-name" value="${showUser.getSecondName()}" required>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="birth-day" class="col-md-4 col-form-label text-md-end">Birth day</label>
                                        <div class="col-md-6">
                                            <input id="birth-day" type="date" class="form-control" name="birth-day" value="${showUser.getBirthDay()}" required>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="phone-number" class="col-md-4 col-form-label text-md-end">Phone number</label>
                                        <div class="col-md-6">
                                            <input id="phone-number" type="tel" class="form-control" name="phone" value="${showUser.getPhone()}" required>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="email" class="col-md-4 col-form-label text-md-end">Email</label>
                                        <div class="col-md-6">
                                            <input id="email" type="email" class="form-control" name="email" value="${showUser.getEmail().getValue()}" required
                                                   autocomplete="email">
                                        </div>
                                    </div>
                                    <div class="row mb-0">
                                        <div class="col-md-6 offset-md-4">
                                            <button type="submit" name="edit" value="change" class="btn btn-primary text-white">
                                                Change values
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <hr>
                                <form class="mb-3" autocomplete="off" method="post" action="${pageContext.request.contextPath}/edit-account">
                                    <input type="hidden" name="userEmail" value="${showUser.getEmail().getValue()}"/>
                                    <label for="deleteAccount" class="mb-2">By clicking this button you will immediately delete your account, so be very careful. If you deleted your account accidentally - call administrator</label>
                                    <button class="btn btn-danger text-white" id="deleteAccount" name="edit" value="delete" type="submit">Delete account</button>
                                </form>
                            </div>
                            <div class="app-card-body px-4 w-100">
                                <div class="item border-bottom py-3">
                                    <div class="row justify-content-between align-items-center">
                                        <div class="col-auto">
                                            <div class="item-label"><strong>Edit / Delete account</strong></div>
                                        </div><!--//col-->
                                        <div class="col text-right">
                                            <button class="btn-sm app-btn-secondary" id="editAccountButton">Change</button>
                                        </div><!--//col-->
                                    </div><!--//row-->
                                </div><!--//item-->
                            </div>
                            <c:if test="${sameUser}">
                                <div id="changePassword" class="position-relative mfp-hide alert alert-success mx-auto col-lg-5 border-radius shadow-lg p-5 m-5" role="alert">
                                    <h4 class="alert-heading">Password changing</h4>
                                    <form action="${pageContext.request.contextPath}/edit-account" method="post" autocomplete="off">
                                        <input type="hidden" name="userEmail" value="${showUser.getEmail().getValue()}"/>
                                        <div class="mb-3">
                                            <label for="password" class="mb-1">Old password</label>
                                            <input id="password" placeholder="Please, type here your password" class="form-control" name="password" required type="password"/>
                                        </div>
                                        <div class="mb-3">
                                            <label for="newPassword" class="mb-1">New password</label>
                                            <input id="newPassword" placeholder="Please, type here your NEW password" class="form-control" name="newPassword" required type="password"/>
                                        </div>
                                        <div class="mb-3">
                                            <label for="confirmPassword" class="mb-1">Confirm new password</label>
                                            <input id="confirmPassword" placeholder="Please, confirm your NEW password" class="form-control" name="confirmPassword" required type="password"/>
                                        </div>
                                        <button type="submit" class="btn btn-success text-white" name="edit" value="passwordChange">Change password</button>
                                    </form>
                                    <hr>
                                    <p class="mb-0">If you forgot your password - please, contact the administrator.</p>
                                </div>
                                <div class="app-card-body px-4 w-100">
                                    <div class="item border-bottom py-3">
                                        <div class="row justify-content-between align-items-center">
                                            <div class="col-auto">
                                                <div class="item-label"><strong>Change password</strong></div>
                                            </div><!--//col-->
                                            <div class="col text-right">
                                                <button class="btn-sm app-btn-secondary" id="changePasswordButton">Change</button>
                                            </div><!--//col-->
                                        </div><!--//row-->
                                    </div><!--//item-->
                                </div>
                            </c:if>
                            <div class="app-card-body px-4 w-100">
                                <div class="item border-bottom py-3">
                                    <div class="row justify-content-between align-items-center">
                                        <div class="col-auto">
                                            <div class="item-label"><strong>Account created at ${showUser.getCreatedAt()}</strong></div>
                                        </div><!--//col-->
                                    </div><!--//row-->
                                </div><!--//item-->
                            </div>
                            <div class="app-card-body px-4 w-100">
                                <div class="item border-bottom py-3">
                                    <div class="row justify-content-between align-items-center">
                                        <div class="col-auto">
                                            <div class="item-label"><strong>Account updated at ${showUser.getUpdatedAt()}</strong></div>
                                        </div><!--//col-->
                                    </div><!--//row-->
                                </div><!--//item-->
                            </div>
                        </div>
                    </div><!--//app-card-->
                </c:if>
            </div><!--//col-->
        </div><!--//row-->
        <c:if test="${(user.getRole() == 'doctor' || user.getRole() == 'nurse') && !sameUser}">
            <hr/>
            <div class="container-xl mt-5">
                <div class="row justify-content-center align-items-center">
                    <div class="col-md-6 text-center">
                        <h4 class="mb-3">Make an appointment with this user</h4>
                        <form action="${pageContext.request.contextPath}/appointment" name="doctor-enroll" autocomplete="on"
                              method="post">
                            <input type="hidden" name="user-email" value="${showUser.getEmail().getValue()}">
                            <div class="mb-3">
                                <label for="appointment-type">Select a type of appointment</label>
                                <select name="appointment-type" required id="appointment-type" class="form-select">
                                    <c:forEach var="type" items="${appointmentTypes}">
                                        <option value="${type}">${type}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="date">Make an appointment with this user</label>
                                <input class="form-control" type="date" name="date" id="date" required/>
                            </div>
                            <button id="meetingSubmit" type="submit" name="makeAppointment" value="makeAppointment" class="btn btn-success border-radius text-white">Make an appointment</button>
                        </form>
                    </div>
                </div>
            </div>
        </c:if>
        <c:if test="${user.getRole() == 'admin'}">
            <hr/>
            <div class="container-xl mt-5">
                <div class="row justify-content-center align-items-center">
                    <div class="col-md-6 text-center">
                        <h4 class="mb-3">Enroll this patient to a doctor</h4>
                        <form action="${pageContext.request.contextPath}/enroll" name="doctor-enroll" autocomplete="on"
                              method="post">
                            <input type="hidden" name="user-email" value="${showUser.getEmail().getValue()}">
                            <div class="mb-3">
                                <label for="doctor-type">Select a type of doctor</label>
                                <select name="doctor-type" required id="doctor-type" class="form-select">
                                    <c:forEach var="category" items="${categories}">
                                        <option value="${category}">${category}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="doctor-type">Select a doctor</label>
                                <select disabled id="doctors-select" name="doctor-email" required class="form-select"></select>
                            </div>
                            <button disabled id="enrollSubmit" type="submit" name="operation" value="enroll" class="btn btn-success border-radius text-white">Sing up user</button>
                        </form>
                    </div>
                </div>
            </div>
            <c:if test="${!user.equalsUser(showUser)}">
                <hr/>
                <div class="container-xl mt-5">
                    <div class="row justify-content-center align-items-center">
                        <div class="col-md-6 text-center">
                            <h4 class="mb-3">Change user permissions</h4>
                            <form id="roleForm" action="${pageContext.request.contextPath}/account" name="doctor-enroll" autocomplete="on"
                                  method="post">
                                <input type="hidden" name="user-email" value="${showUser.getEmail().getValue()}">
                                <div class="mb-3">
                                    <label for="role-select">Select a new role for this user</label>
                                    <select form="roleForm" name="role" id="role-select" class="form-select">
                                        <c:forEach items="${roles}" var="role">
                                            <option value="${role}">${role}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label for="category-select">Select a new category for this user</label>
                                    <select form="roleForm" name="category" id="category-select" class="form-select" disabled>
                                        <c:forEach items="${categories}" var="category">
                                            <option value="${category}">${category}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <input type="submit" value="Update permissions" name="enroll-to-appointment"
                                       class="btn btn-success border-radius text-white"/>
                            </form>
                        </div>
                    </div>
                </div>
            </c:if>
        </c:if>
    </div><!--//container-fluid-->
</div><!--//app-content-->
<%@ include file="templates/system-scripts.jspf"%>
</body>
</html>