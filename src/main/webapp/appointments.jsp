<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<%@ page contentType="text/html; UTF-8" %>
<!DOCTYPE html>
<html lang="${sessionScope.userSettings.getLocate()}">
<head>
    <title><my:app-name/> system - your appointments</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Portal - Bootstrap 5 Admin Dashboard Template For Developers">
    <meta name="author" content="Xiaoying Riley at 3rd Wave Media">
    <%@ include file="templates/system-styles.jspf"%>
</head>
<body class="app">
<%@ include file="templates/system-navbar.jspf"%>
<div class="app-wrapper">
    <div class="app-content pt-3 p-md-3 p-lg-4">
        <div class="container-xl">
            <div class="row g-3 mb-4 align-items-center justify-content-between">
                <div class="col-auto">
                    <h1 class="app-page-title mb-0">Appointments <span class="text-muted">(${totalAppointments} in total)</span></h1>
                </div>
            </div><!--//row-->
            <div class="tab-content" id="orders-table-tab-content">
                <div class="tab-pane fade show active" id="orders-all" role="tabpanel" aria-labelledby="orders-all-tab">
                    <div class="app-card app-card-orders-table shadow-sm mb-5">
                        <div class="app-card-body">
                            <c:choose>
                                <c:when test="${appointments.size() > 0}">
                                    <div class="table-responsive">
                                        <table class="table app-table-hover mb-0 text-left">
                                            <thead>
                                            <tr>
                                                <th class="cell">type</th>
                                                <th class="cell">time</th>
                                                <th class="cell">doctor name</th>
                                                <th class="cell">patient name</th>
                                                <th class="cell"></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <c:forEach var="appointment" items="${appointments}">
                                                <tr>
                                                    <td class="cell"><span class="truncate">${appointment.getAppointmentType()}</span>
                                                    </td>
                                                    <td class="cell">${appointment.getMeetingDate()}</td>
                                                    <td class="cell">${appointment.getDoctor().getName()}</td>
                                                    <td class="cell">${appointment.getUser().getName()}</td>
                                                    <td class="cell"><a class="btn-sm app-btn-secondary" href="${pageContext.request.contextPath}/appointment?id=${appointment.getId()}">View</a></td>
                                                </tr>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                    </div><!--//table-responsive-->
                                </c:when>
                                <c:otherwise>
                                    <h3 class="m-3">No appointments yet</h3>
                                </c:otherwise>
                            </c:choose>
                        </div><!--//app-card-body-->
                    </div><!--//app-card-->
                    <c:if test="${totalAppointments > 10}">
                        <nav class="app-pagination">
                            <ul class="pagination justify-content-center">
                                <c:choose>
                                    <c:when test="${currentPage == 1}">
                                        <li class="page-item disabled">
                                            <a class="page-link" href="${pageContext.request.contextPath}/${AppointmentsRelies}?page=1" tabindex="-1" aria-disabled="true">First page</a>
                                        </li>
                                    </c:when>
                                    <c:otherwise>
                                        <li class="page-item">
                                            <a class="page-link" href="${pageContext.request.contextPath}/${AppointmentsRelies}?page=1">First page</a>
                                        </li>
                                    </c:otherwise>
                                </c:choose>
                                <c:if test="${currentPage != 1}">
                                    <li class="page-item"><a class="page-link" href="${pageContext.request.contextPath}/${AppointmentsRelies}?page=${currentPage - 1}">${currentPage - 1}</a></li>
                                </c:if>
                                <li class="page-item active"><a class="page-link" href="${pageContext.request.contextPath}/${AppointmentsRelies}?page=${currentPage}">${currentPage}</a></li>
                                <c:if test="${currentPage + 1 <= totalPages}">
                                    <li class="page-item"><a class="page-link" href="${pageContext.request.contextPath}/${AppointmentsRelies}?page=${currentPage + 1}">${currentPage + 1}</a></li>
                                </c:if>
                                <c:choose>
                                    <c:when test="${currentPage == totalPages}">
                                        <li class="page-item disabled">
                                            <a class="page-link" href="${pageContext.request.contextPath}/${AppointmentsRelies}?page=${totalPages}" tabindex="-1" aria-disabled="true">Last page</a>
                                        </li>
                                    </c:when>
                                    <c:otherwise>
                                        <li class="page-item">
                                            <a class="page-link" href="${pageContext.request.contextPath}/${AppointmentsRelies}?page=${totalPages}">Last page</a>
                                        </li>
                                    </c:otherwise>
                                </c:choose>
                            </ul>
                        </nav><!--//app-pagination-->
                    </c:if>
                </div><!--//tab-pane-->
            </div><!--//tab-content-->
        </div><!--//container-fluid-->
    </div><!--//app-content-->
</div><!--//app-wrapper-->
<%@ include file="templates/system-scripts.jspf"%>
</body>
</html>