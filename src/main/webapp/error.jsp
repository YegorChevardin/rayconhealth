<%@ page isErrorPage="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<%@ page contentType="text/html; UTF-8" %>
<!DOCTYPE html>
<html lang="${sessionScope.userSettings.getLocate()}">
<head>
    <title><my:app-name/> - something went wrong</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Portal - Bootstrap 5 Admin Dashboard Template For Developers">
    <meta name="author" content="Xiaoying Riley at 3rd Wave Media">
    <%@ include file="templates/system-styles.jspf"%>
</head>
<body class="app">
<div class="container mb-5">
    <div class="row">
        <div class="col-12 col-md-11 col-lg-7 col-xl-6 mx-auto">
            <div class="app-card p-5 text-center shadow-sm">
                <h1 class="page-title mb-4">
                    <c:out value="${pageContext.errorData.statusCode}"/>
                </h1>
                <div class="mb-4">
                    <c:if test="${errorMessage != null}">
                        <c:out value="${errorMessage}"/>
                        <hr/>
                    </c:if>
                    <c:out value="${pageContext.errorData.throwable.message}"/>
                </div>
                <a class="btn app-btn-primary" href="${pageContext.request.contextPath}/">Go to home page</a>
            </div>
        </div><!--//col-->
    </div><!--//row-->
</div><!--//container-->
<%@ include file="templates/system-scripts.jspf"%>
</body>
</html>