<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ page contentType="text/html; UTF-8" %>
<!DOCTYPE html>
<html lang="${sessionScope.userSettings.getLocate()}">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <title><my:app-name/> - health care system</title>
    <%@ include file="templates/styles.jspf" %>
</head>
<body>
<%@ include file="templates/navbar.jspf" %>
<div class="container screen-height text-center">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-md-8">
            <div class="card wow bounceInUp">
                <div class="card-header">Login</div>
                <div class="card-body">
                    <form id="authForm" name="login" method="post" action="${pageContext.request.contextPath}/login">
                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end">Email</label>
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" required
                                       autocomplete="email">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end">Password</label>
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required
                                       autocomplete="new-password">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="d-flex align-items-center justify-content-center form-captcha">
                            </div>
                        </div>
                        <div class="row mb-0">
                            <div class="ration">
                                <button id="submitButton" type="submit" name="submit" value="register" class="mt-1 btn btn-primary">
                                    Login
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="mt-3 wow bounceInUp">
                <p>First in our website? - <a href="${pageContext.request.contextPath}/register">register now</a></p>
            </div>
        </div>
    </div>
</div>
<%@ include file="templates/footer.jspf" %>
<%@ include file="templates/scripts.jspf" %>
</body>
</html>
