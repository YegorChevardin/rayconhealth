<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ page contentType="text/html; UTF-8" %>
<!DOCTYPE html>
<html lang="${sessionScope.userSettings.getLocate()}">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <title><my:app-name/> - health care system</title>
    <%@ include file="templates/styles.jspf" %>
</head>
<body>
<%@ include file="templates/navbar.jspf" %>
<div class="container screen-height text-center">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-md-8">
            <div class="card wow bounceInUp">
                <div class="card-header">Registration</div>
                <div class="card-body">
                    <form id="authForm" name="register-form" method="post" action="${pageContext.request.contextPath}/register" autocomplete="on" autofocus>
                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">Name</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" required>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="second-name" class="col-md-4 col-form-label text-md-end">Second name</label>
                            <div class="col-md-6">
                                <input id="second-name" type="text" class="form-control" name="second-name" required>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="birth-day" class="col-md-4 col-form-label text-md-end">Birth day</label>
                            <div class="col-md-6">
                                <input id="birth-day" type="date" class="form-control" name="birth-day" required>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="phone-number" class="col-md-4 col-form-label text-md-end">Phone number</label>
                            <div class="col-md-6">
                                <input id="phone-number" type="tel" class="form-control" name="phone" required>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end">Email</label>
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" required
                                       autocomplete="email">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end">Password</label>
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required
                                       autocomplete="new-password">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="d-flex align-items-center justify-content-center form-captcha">
                            </div>
                        </div>
                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button id="submitButton" type="submit" name="submit" value="register" class="mt-1 btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="mt-3 wow bounceInUp">
                <p>Already have an account? - <a href="${pageContext.request.contextPath}/login">login now</a></p>
            </div>
        </div>
    </div>
</div>
<%@ include file="templates/footer.jspf" %>
<%@ include file="templates/scripts.jspf" %>
</body>
</html>
