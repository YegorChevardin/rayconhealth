$(document).ready(function() {
    $('#createAccountButton').magnificPopup({
        removalDelay: 300,
        mainClass: 'mfp-fade',
        items: {
            src: '#createAccount'
        }
    });
});