'use strict';

/* ===== Enable Bootstrap Popover (on element  ====== */

var popoverTriggerList = [].slice.call(document.querySelectorAll('[data-toggle="popover"]'))
var popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
  return new bootstrap.Popover(popoverTriggerEl)
})

/* ==== Enable Bootstrap Alert ====== */
var alertList = document.querySelectorAll('.alert')
alertList.forEach(function (alert) {
  new bootstrap.Alert(alert)
});


/* ===== Responsive Sidepanel ====== */
const sidePanelToggler = document.getElementById('sidepanel-toggler'); 
const sidePanel = document.getElementById('app-sidepanel');  
const sidePanelDrop = document.getElementById('sidepanel-drop'); 
const sidePanelClose = document.getElementById('sidepanel-close'); 

window.addEventListener('load', function(){
	responsiveSidePanel(); 
});

window.addEventListener('resize', function(){
	responsiveSidePanel(); 
});


function responsiveSidePanel() {
    let w = window.innerWidth;
	if(w >= 1200) {
	    // if larger 
	    //console.log('larger');
		sidePanel.classList.remove('sidepanel-hidden');
		sidePanel.classList.add('sidepanel-visible');
		
	} else {
	    // if smaller
	    //console.log('smaller');
	    sidePanel.classList.remove('sidepanel-visible');
		sidePanel.classList.add('sidepanel-hidden');
	}
};

sidePanelToggler.addEventListener('click', () => {
	if (sidePanel.classList.contains('sidepanel-visible')) {
		console.log('visible');
		sidePanel.classList.remove('sidepanel-visible');
		sidePanel.classList.add('sidepanel-hidden');
		
	} else {
		console.log('hidden');
		sidePanel.classList.remove('sidepanel-hidden');
		sidePanel.classList.add('sidepanel-visible');
	}
});

sidePanelClose.addEventListener('click', (e) => {
	e.preventDefault();
	sidePanelToggler.click();
});

sidePanelDrop.addEventListener('click', (e) => {
	sidePanelToggler.click();
});

// extract to PDF script section start
var doc = new jsPDF();
var specialElementHandlers = {
	'#editor': function (element, renderer) {
		return true;
	}
};
$('#hospitalCardPDF').click(function () {
	doc.fromHTML($('#htmlContent').html(), 15, 15, {
		'width': 700,
		'elementHandlers': specialElementHandlers
	}, function(bla){doc.save('hospitalCard.pdf');});
});

//changing permissions form setup
$(document).ready(function() {
	let roleSelect = $("#role-select");
	if(roleSelect.val() !== "admin" && roleSelect.val() !== "user") $('#category-select').prop("disabled", false);
	roleSelect.on('change', function () {
		let el = $(this);
		if (el.val() !== "admin" && el.val() !== 'user') {
			$('#category-select').prop("disabled", false);
		} else {
			$('#category-select').prop("disabled", true);
		}
	});
});

//edit account page (popup) todo

//get doctors by category (ajax)
$("#doctor-type").change(function() {
	let doctorSelect = $('#doctors-select');
	doctorSelect.find('option').remove()
	$.ajax({
		url: "enroll",
		method: "POST",
		data: {operation: 'getDoctors', category: $('#doctor-type').val()},
		success: function(data, textStatus, jqXHR) {
			if (data !== "[]") {
				let obj = $.parseJSON(data);
				$.each(obj, function (key, value) {
					doctorSelect.append('<option value="' + value.email.value + '">' + value.name + '</option>');
				});
				doctorSelect.prop("disabled", false);
				$('#enrollSubmit').prop("disabled", false);
			} else {
				doctorSelect.prop("disabled", true);
				$('#enrollSubmit').prop("disabled", true);
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			alert("Doctor unavailable");
			$('#enrollSubmit').prop("disabled", true);
		},
		cache: false
	});
});

//Magnific popup section start
$('#editAccountButton').magnificPopup({
	removalDelay: 300,
	mainClass: 'mfp-fade',
	items: {
		src: '#editAccount'
	}
});
$('#changePasswordButton').magnificPopup({
	removalDelay: 300,
	mainClass: 'mfp-fade',
	items: {
		src: '#changePassword'
	}
});
$('#addDiagnosButton').magnificPopup({
	removalDelay: 300,
	mainClass: 'mfp-fade',
	items: {
		src: '#addDiagnos'
	}
});