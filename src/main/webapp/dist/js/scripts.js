/* Animation init section start */
new WOW().init();
/* Animation init section end */
/* Dropdown animation section start */
// Add slideDown animation to Bootstrap dropdown when expanding.
$('.dropdown').on('show.bs.dropdown', function() {
    $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
});

// Add slideUp animation to Bootstrap dropdown when collapsing.
$('.dropdown').on('hide.bs.dropdown', function() {
    $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
});
/* Dropdown animation section end */

/* Navbar animation section start */
var prevScrollpos = window.pageYOffset;

window.onscroll = function() {
    var currentScrollpos = window.pageYOffset;

    if(prevScrollpos > currentScrollpos) {
        document.getElementById('navbar').style.top = "0";
    } else {
        document.getElementById('navbar').style.top = "-5.1em";
    }
    prevScrollpos = currentScrollpos;
}
/* Navbar animation section end */

/* Year section start */
let copyright_year = document.getElementById('year');
let  current_year = new Date().getFullYear();
copyright_year.innerText = current_year;
/* Year section end */

/* Magnific popup button section start */
$('#cookiePopupButton').magnificPopup({
    removalDelay: 300,
    mainClass: 'mfp-fade',
    items: {
        src: '#cookieAlert'
    }
});
$(document).ready(function() {
    if (localStorage.getItem('cookieSeen') !== 'shown') {
        $('#cookiePopupButton').click();
        localStorage.setItem('cookieSeen', 'shown');
    };
});