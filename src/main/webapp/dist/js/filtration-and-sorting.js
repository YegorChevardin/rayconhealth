$(document).ready(function () {
    let doctorSelect = $("#sortingSelectDoctors");
    let userSelect = $("#sortingSelectUsers");

    doctorSelect.change(function() {
        let table = document.querySelector("#userElements");
        let tableChildren = table.children;

        if (doctorSelect.val() === "patients") {
            patientsSort(table, tableChildren);
        } else if (doctorSelect.val() === "category") {
            categorySort(table, tableChildren);
        } else if (doctorSelect.val() === "alphabet") {
            alphabetSort(table, tableChildren);
        } else {
            idSort(table, tableChildren);
        }
    });
    userSelect.change(function() {
        let table = document.querySelector("#userElements");
        let tableChildren = table.children;

        if (userSelect.val() === "birth") {
            birthSort(table, tableChildren);
        } else if (userSelect.val() === "alphabet") {
            alphabetSort(table, tableChildren);
        } else {
            idSort(table, tableChildren);
        }
    });

    /* Searching section start */
    $("#searchForm").on('submit', function(e) {
        e.preventDefault();
        let search = $("#search").val().trim().toLowerCase();
        let tableChildren = document.querySelector("#userElements").children;

        if (search !== "") {
            applySearch(search, tableChildren);
        } else {
            updateElements(tableChildren);
        }
    });
});

function updateElements(tableChildren) {
    for (let i = 0; i < tableChildren.length; i++) {
        tableChildren[i].classList.remove("d-none");
    }
}

function applySearch(searchingString, tableChildren) {
    for (let i = 0; i < tableChildren.length; i++) {
        let name = tableChildren[i].getAttribute("data-name").toLowerCase();
        if (name.search(searchingString) === -1) {
            tableChildren[i].classList.add("d-none");
        } else {
            tableChildren[i].classList.remove("d-none");
        }
    }
}

function insertAfter(element, referenceElement) {
    return referenceElement.parentNode.insertBefore(element, referenceElement.nextSibling);
}

function idSort(table, tableChildren) {
    for (let i = 0; i < tableChildren.length; i++) {
        for (let j = i; j < tableChildren.length; j++) {
            if (+tableChildren[i].getAttribute("data-id") > +tableChildren[j].getAttribute("data-id")) {
                let replaceNode = table.replaceChild(tableChildren[j], tableChildren[i]);
                insertAfter(replaceNode, tableChildren[i]);
            }
        }
    }
}

function patientsSort(table, tableChildren) {
    for (let i = 0; i < tableChildren.length; i++) {
        for (let j = i; j < tableChildren.length; j++) {
            if (+tableChildren[i].getAttribute("data-patients") < +tableChildren[j].getAttribute("data-patients")) {
                let replaceNode = table.replaceChild(tableChildren[j], tableChildren[i]);
                insertAfter(replaceNode, tableChildren[i]);
            }
        }
    }
}

function alphabetSort(table, tableChildren) {
    for (let i = 0; i < tableChildren.length; i++) {
        for (let j = i; j < tableChildren.length; j++) {
            if (tableChildren[i].getAttribute("data-name") > tableChildren[j].getAttribute("data-name")) {
                let replaceNode = table.replaceChild(tableChildren[j], tableChildren[i]);
                insertAfter(replaceNode, tableChildren[i]);
            }
        }
    }
}

function categorySort(table, tableChildren) {
    for (let i = 0; i < tableChildren.length; i++) {
        for (let j = i; j < tableChildren.length; j++) {
            if (tableChildren[i].getAttribute("data-category") > tableChildren[j].getAttribute("data-category")) {
                let replaceNode = table.replaceChild(tableChildren[j], tableChildren[i]);
                insertAfter(replaceNode, tableChildren[i]);
            }
        }
    }
}

function birthSort(table, tableChildren) {
    for (let i = 0; i < tableChildren.length; i++) {
        for (let j = i; j < tableChildren.length; j++) {
            if (tableChildren[i].getAttribute("data-birth") > tableChildren[j].getAttribute("data-birth")) {
                let replaceNode = table.replaceChild(tableChildren[j], tableChildren[i]);
                insertAfter(replaceNode, tableChildren[i]);
            }
        }
    }
}