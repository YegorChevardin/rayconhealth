<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ page contentType="text/html; UTF-8" %>
<!DOCTYPE html>
<html lang="${sessionScope.userSettings.getLocate()}">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <title><my:app-name/> - admin password checker</title>
    <%@ include file="templates/styles.jspf" %>
</head>
<body>
<%@ include file="templates/navbar.jspf" %>
<div class="container screen-height text-center">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-md-8">
            <div class="card wow bounceInUp">
                <div class="card-header">Login</div>
                <div class="card-body">
                    <form name="login" method="post" action="${pageContext.request.contextPath}/logs">
                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end">Password</label>
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required
                                       autocomplete="new-password">
                            </div>
                        </div>
                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" name="submit" value="register" class="btn btn-primary">
                                    Check password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="templates/footer.jspf" %>
<%@ include file="templates/scripts.jspf" %>
</body>
</html>
