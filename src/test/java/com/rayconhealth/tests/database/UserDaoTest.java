package com.rayconhealth.tests.database;

import com.rayconhealth.rayconhealth.database.UserDao;
import com.rayconhealth.rayconhealth.models.User;
import com.rayconhealth.rayconhealth.models.classes.Email;
import com.rayconhealth.rayconhealth.models.classes.Password;
import com.rayconhealth.tests.models.classes.emails.Util;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;

public class UserDaoTest {
    private static final String CONNECTION_URL = "jdbc:derby:memory:testdb;create=true";
    private static final String SHUTDOWN_URL = "jdbc:derby:;shutdown=true";

    private static final String CREATE_USERS_TABLE = "CREATE TABLE users (" +
            " id INT NOT NULL GENERATED ALWAYS AS IDENTITY," +
            " name VARCHAR(20) NOT NULL," +
            " second_name VARCHAR(45) NOT NULL," +
            " birth_day DATE NOT NULL," +
            " phone VARCHAR(45) NOT NULL," +
            " email VARCHAR(255) NOT NULL," +
            " password VARCHAR(128) NOT NULL," +
            " role_id INT NOT NULL," +
            " category_id INT NOT NULL," +
            " created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP," +
            " updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP," +
            " PRIMARY KEY (id))";

    private static final String CREATE_ROLE_TABLE = "CREATE TABLE roles (" +
            "  id INT NOT NULL GENERATED ALWAYS AS IDENTITY," +
            "  name VARCHAR(45) NOT NULL," +
            "  PRIMARY KEY (id))";
    private static final String CREATE_CATEGORY_TABLE = "CREATE TABLE categories (" +
            "  id INT NOT NULL GENERATED ALWAYS AS IDENTITY," +
            "  name VARCHAR(45) NOT NULL," +
            "  PRIMARY KEY (id))";

    private static final String SEED_USER_ROLE = "INSERT INTO roles (name) VALUES ('user'), ('admin'), ('doctor'), ('nurse')";
    private static final String SEED_USER_CATEGORIES = "INSERT INTO categories (name) VALUES ('patient'), ('pediatrician'), ('traumatologist'), ('surgeon'), ('dentist'), ('ophthalmologist')";
    /* need to create insertions for roles and categories */
    private static final String DROP_USERS_TABLE = "DROP TABLE users";
    private static final String DROP_ROLES_TABLE = "DROP TABLE roles";
    private static final String DROP_CATEGORY_TABLE = "DROP TABLE categories";
    private static final String DERBY_LOG_FILE = "derby.log";
    private static Connection con;

    @BeforeAll
    static void globalSetUp() throws SQLException {
        con = DriverManager.getConnection(CONNECTION_URL);
        con.createStatement().executeUpdate(CREATE_ROLE_TABLE);
        con.createStatement().executeUpdate(CREATE_CATEGORY_TABLE);
        con.createStatement().executeUpdate(SEED_USER_ROLE);
        con.createStatement().executeUpdate(SEED_USER_CATEGORIES);
    }

    @AfterAll
    static void globalTearDown() throws SQLException, IOException {
        con.createStatement().executeUpdate(DROP_ROLES_TABLE);
        con.createStatement().executeUpdate(DROP_CATEGORY_TABLE);
        con.close();
        try {
            DriverManager.getConnection(SHUTDOWN_URL);
        } catch (SQLException ex) {
            System.err.println("Derby shutdown");
        }
        Files.delete(Path.of(DERBY_LOG_FILE));
    }

    private final UserDao daoInstance = UserDao.getInstance();

    @BeforeEach
    void setUp() throws SQLException {
        con.createStatement().executeUpdate(CREATE_USERS_TABLE);
    }

    @AfterEach
    void tearDown() throws SQLException {
        con.createStatement().executeUpdate(DROP_USERS_TABLE);
    }

    @Test
    @DisplayName("Create user test")
    public void createUserTest() {
        String phone = String.valueOf((new Random().nextInt(7)+1) * 100);
        String email = Util.generateRandomEmailValue();
        String password = UUID.randomUUID().toString().replaceAll("_", "");
        String secondName = "Chevardin";
        String name = "Yegor";
        User testingUser = new User(
                name,
                secondName,
                new Date(2004, 05, 03),
                Email.generateEmail(email),
                phone,
                Password.generatePassword(password, false),
                "user",
                "patient"
        );
        Assertions.assertTrue(daoInstance.insert(testingUser, con));
    }

    @Test
    @DisplayName("Role test")
    public void roleTest() {
        Assertions.assertEquals("admin", UserDao.getInstance().getRole(UserDao.getInstance().getRoleId("admin", con), con));
    }

    @Test
    @DisplayName("Category test")
    public void categoryTest() {
        Assertions.assertEquals("nurse", UserDao.getInstance().getRole(UserDao.getInstance().getRoleId("nurse", con), con));
    }

    @Test
    @DisplayName("Get all users test")
    public void getAllUsersTest() {
        User user1 = new User(
                "Yegor",
                "Chevardin",
                new Date(2004, 05, 03),
                Email.generateEmail(Util.generateRandomEmailValue()),
                String.valueOf((new Random().nextInt(7)+1) * 100),
                Password.generatePassword(UUID.randomUUID().toString().replaceAll("_", ""), false),
                "user",
                "patient"
        );
        User user2 = new User(
                "Anya",
                "Scheremet",
                new Date(2003, 11, 19),
                Email.generateEmail(Util.generateRandomEmailValue()),
                String.valueOf((new Random().nextInt(7)+1) * 100),
                Password.generatePassword(UUID.randomUUID().toString().replaceAll("_", ""), false),
                "nurse",
                "surgeon"
        );
        daoInstance.insert(user1, con);
        daoInstance.insert(user2, con);

        List<User> userList = daoInstance.getAll(con);

        Assertions.assertTrue(userList.get(0).equalsUser(user1));
        Assertions.assertTrue(userList.get(1).equalsUser(user2));
    }

    @Test
    @DisplayName("Get by id test")
    public void getByIdTest() {
        User user1 = new User(
                "Yegor",
                "Chevardin",
                new Date(2004, 05, 03),
                Email.generateEmail(Util.generateRandomEmailValue()),
                String.valueOf((new Random().nextInt(7)+1) * 100),
                Password.generatePassword(UUID.randomUUID().toString().replaceAll("_", ""), false),
                "user",
                "patient"
        );

        daoInstance.insert(user1, con);
        User resultUser = daoInstance.getById(user1.getId(), con);
        Assertions.assertTrue(user1.equalsUser(resultUser));
    }

    @Test
    @DisplayName("Update user test")
    public void updateUser() {
        User user1 = new User(
                "Yegor",
                "Chevardin",
                new Date(2004, 05, 03),
                Email.generateEmail(Util.generateRandomEmailValue()),
                String.valueOf((new Random().nextInt(7)+1) * 100),
                Password.generatePassword(UUID.randomUUID().toString().replaceAll("_", ""), false),
                "user",
                "patient"
        );

        String newName = "Ilya";
        daoInstance.insert(user1, con);
        user1.setName(newName);
        Assertions.assertTrue(daoInstance.update(user1, con));
        User resultUser = daoInstance.getById(user1.getId(), con);
        Assertions.assertEquals(resultUser.getName(), newName);
    }

    @Test
    @DisplayName("Delete user test")
    public void deleteUserTest() {
        User user1 = new User(
                "Yegor",
                "Chevardin",
                new Date(2004, 05, 03),
                Email.generateEmail(Util.generateRandomEmailValue()),
                String.valueOf((new Random().nextInt(7)+1) * 100),
                Password.generatePassword(UUID.randomUUID().toString().replaceAll("_", ""), false),
                "user",
                "patient"
        );
        daoInstance.insert(user1, con);
        daoInstance.delete(user1, con);
        User user = daoInstance.getById(user1.getId(), con);
        Assertions.assertNull(user);
    }
}
