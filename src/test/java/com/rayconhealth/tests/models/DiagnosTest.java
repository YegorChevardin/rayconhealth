package com.rayconhealth.tests.models;

import com.rayconhealth.rayconhealth.models.Diagnos;
import com.rayconhealth.rayconhealth.models.HospitalCard;
import com.rayconhealth.rayconhealth.models.User;
import com.rayconhealth.rayconhealth.models.classes.Email;
import com.rayconhealth.rayconhealth.models.classes.Password;
import com.rayconhealth.tests.models.classes.emails.Util;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.sql.Date;
import java.util.Random;
import java.util.UUID;

public class DiagnosTest {
    private User doctor;
    private User patient;
    private HospitalCard hospitalCard;
    private Diagnos diagnos;

    public DiagnosTest() {
        String phone = String.valueOf((new Random().nextInt(7)+1) * 100);
        String email = Util.generateRandomEmailValue();
        String password = UUID.randomUUID().toString().replaceAll("_", "");
        String secondName = "Chevardin";
        String name = "Yegor";
        patient = new User(
                name,
                secondName,
                new Date(2004, 05, 03),
                Email.generateEmail(email),
                phone,
                Password.generatePassword(password, false),
                "user",
                "patient"
        );
        doctor = patient;
        hospitalCard = new HospitalCard(patient.getName() + "'s hospital card", patient);
        diagnos = new Diagnos("Some diagnos name", "very difficult status", doctor, hospitalCard);
    }

    @Test
    @DisplayName("Diagnos name and status test")
    public void diagnosNameAndStatusTest() {
        Assertions.assertEquals("Some diagnos name very difficult status", diagnos.getName() + " " + diagnos.getDiagnosStatus());
    }

    @Test
    @DisplayName("Diagnos doctor test")
    public void diagnosDoctorTest() {
        Assertions.assertEquals(doctor, diagnos.getDoctor());
    }

    @Test
    @DisplayName("Diagnos patient test")
    public void diagnosPatientTest() {
        Assertions.assertEquals(patient, diagnos.getHospitalCard().getUser());
    }

    @Test
    @DisplayName("Special diagnos patient test")
    public void specialDiadnosPatientTest() {
        Assertions.assertEquals(diagnos.getHospitalCard().getUser(), diagnos.getDoctor());
    }
}
