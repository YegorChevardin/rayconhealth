package com.rayconhealth.tests.models;

import com.rayconhealth.rayconhealth.models.HospitalCard;
import com.rayconhealth.rayconhealth.models.User;
import com.rayconhealth.rayconhealth.models.classes.Email;
import com.rayconhealth.rayconhealth.models.classes.Password;
import com.rayconhealth.tests.models.classes.emails.Util;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.sql.Date;
import java.util.Random;
import java.util.UUID;

public class HospitalCardTest {
    private HospitalCard hospitalCard;
    private User user;

    public HospitalCardTest() {
        String phone = String.valueOf((new Random().nextInt(7)+1) * 100);
        String email = Util.generateRandomEmailValue();
        String password = UUID.randomUUID().toString().replaceAll("_", "");
        String secondName = "Chevardin";
        String name = "Yegor";
        user = new User(
                name,
                secondName,
                new Date(2004, 05, 03),
                Email.generateEmail(email),
                phone,
                Password.generatePassword(password, false),
                "user",
                "patient"
        );
        this.hospitalCard = new HospitalCard(user.getName() + "'s hospital card", user);
    }

    @Test
    @DisplayName("Hospital card user test")
    public void HospitalCardGenerationTest() {
        Assertions.assertTrue(hospitalCard.getUser().equalsUser(user));
    }
}
