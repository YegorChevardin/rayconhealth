package com.rayconhealth.tests.models;

import com.rayconhealth.rayconhealth.models.User;
import com.rayconhealth.rayconhealth.models.classes.Email;
import com.rayconhealth.rayconhealth.models.classes.Password;
import com.rayconhealth.rayconhealth.models.classes.PasswordHashing;
import com.rayconhealth.tests.models.classes.emails.Util;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.sql.Date;
import java.util.Random;
import java.util.UUID;

public class UserTest {
    private final String email;
    private final String password;
    private final String phone;
    private final User testingUser;

    public UserTest() {
        phone = String.valueOf((new Random().nextInt(7)+1) * 100);
        email = Util.generateRandomEmailValue();
        password = UUID.randomUUID().toString().replaceAll("_", "");
        String secondName = "Chevardin";
        String name = "Yegor";
        testingUser = new User(
                name,
                secondName,
                new Date(2004, 05, 03),
                Email.generateEmail(email),
                phone,
                Password.generatePassword(password, false),
                "user",
                "patient"
        );
    }

    @Test
    @DisplayName("User email test")
    public void userEmailTest() {
        Assertions.assertEquals(email, testingUser.getEmail().getValue());
    }

    @Test
    @DisplayName("User password testing")
    public void userPasswordTest() {
        Assertions.assertTrue(new PasswordHashing().authenticate(password, testingUser.getPassword().getValue()));
    }

    @Test
    @DisplayName("User phone number testing")
    public void userPhoneTest() {
        Assertions.assertEquals(phone, testingUser.getPhone());
    }

    @Test
    @DisplayName("User birth day test")
    public void userBirthDayTest() {
        Assertions.assertEquals(new Date(2004, 05, 03), testingUser.getBirthDay());
    }

    @Test
    @DisplayName("User initials test")
    public void userInitialsTesting() {
        Assertions.assertEquals("Yegor Chevardin", testingUser.getName() + " " + testingUser.getSecondName());
    }

    @Test
    @DisplayName("User role and category test")
    public void userRoleAndCategoryTest() {
        Assertions.assertEquals("user - patient", testingUser.getRole() + " - " + testingUser.getCategory());
    }
}
