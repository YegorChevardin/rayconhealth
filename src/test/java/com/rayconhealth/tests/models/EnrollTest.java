package com.rayconhealth.tests.models;

import com.rayconhealth.rayconhealth.models.Enroll;
import com.rayconhealth.rayconhealth.models.User;
import com.rayconhealth.rayconhealth.models.classes.Email;
import com.rayconhealth.rayconhealth.models.classes.Password;
import com.rayconhealth.tests.models.classes.emails.Util;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.sql.Date;
import java.util.Random;
import java.util.UUID;

public class EnrollTest {
    private User user;
    private Enroll enroll;
    private String enrollText;

    public EnrollTest() {
        String phone = String.valueOf((new Random().nextInt(7)+1) * 100);
        String email = Util.generateRandomEmailValue();
        String password = UUID.randomUUID().toString().replaceAll("_", "");
        String secondName = "Chevardin";
        String name = "Yegor";
        enrollText = UUID.randomUUID().toString().replaceAll("_", "");
        user = new User(
                name,
                secondName,
                new Date(2004, 05, 03),
                Email.generateEmail(email),
                phone,
                Password.generatePassword(password, false),
                "user",
                "patient"
        );
        enroll = new Enroll(
                user,
                enrollText
        );
    }

    @Test
    @DisplayName("Enroll user test")
    public void enrollUserTest() {
        Assertions.assertTrue(enroll.getUser().equalsUser(user));
    }

    @Test
    @DisplayName("Enroll text")
    public void enrollUserNegativeTest() {
        Assertions.assertEquals(enrollText, enroll.getText());
    }
}
