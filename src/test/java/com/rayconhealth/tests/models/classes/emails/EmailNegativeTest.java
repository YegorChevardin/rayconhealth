package com.rayconhealth.tests.models.classes.emails;

import com.rayconhealth.rayconhealth.models.classes.Email;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.rayconhealth.tests.models.classes.emails.Util.generateRandomEmailValueNotValid;

public class EmailNegativeTest {
    @Test
    @DisplayName("Testing negative email value")
    void testNegativeEmailValue() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> Email.generateEmail(generateRandomEmailValueNotValid()));
    }

    @Test
    @DisplayName("Testing negative email matcher")
    void testNegativeEmailMatcher() {
        Assertions.assertFalse(Email.emailMatcher(generateRandomEmailValueNotValid()));
    }
}
