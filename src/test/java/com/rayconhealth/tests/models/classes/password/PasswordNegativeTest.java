package com.rayconhealth.tests.models.classes.password;

import com.rayconhealth.rayconhealth.models.classes.Password;
import com.rayconhealth.rayconhealth.models.classes.PasswordHashing;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class PasswordNegativeTest {
    @Test
    @DisplayName("Testing password generation with negative result")
    public void negativePasswordGenerationTest() {
        String number = "123456789";
        Password password = Password.generatePassword(number, false);
        Assertions.assertFalse(new PasswordHashing().authenticate("12345678", password.getValue()));
    }

    @Test
    @DisplayName("Testing password generation without hashing with negative result")
    public void negativePasswordGenrationWithoutHashingTest() {
        String number = "123456789";
        Password password = Password.generatePassword(number, true);
        Assertions.assertFalse(password.getValue().equals("12345678"));
    }
}
