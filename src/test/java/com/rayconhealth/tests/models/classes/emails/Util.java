package com.rayconhealth.tests.models.classes.emails;

import com.rayconhealth.rayconhealth.models.classes.Email;

import java.util.UUID;

public class Util {
    private Util() {}

    public static String generateRandomEmailValue() {

        return UUID.randomUUID().toString().replaceAll("_", "") + "@" +
                UUID.randomUUID().toString().replaceAll("_", "") +
                "." +
                UUID.randomUUID().toString().replaceAll("_", "")+
                UUID.randomUUID().toString().replaceAll("_", "");
    }

    public static String generateRandomEmailValueNotValid() {
        return UUID.randomUUID().toString().replaceAll("_", "") +
                UUID.randomUUID().toString().replaceAll("_", "") +
                "." +
                UUID.randomUUID().toString().replaceAll("_", "")+
                UUID.randomUUID().toString().replaceAll("_", "");
    }
}
