package com.rayconhealth.tests.models.classes.emails;

import com.rayconhealth.rayconhealth.models.classes.Email;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.rayconhealth.tests.models.classes.emails.Util.generateRandomEmailValue;

public class EmailPositiveTest {
    @Test
    @DisplayName("Testing email's matcher")
    void testEmailMatcher() {
        Assertions.assertTrue(Email.emailMatcher(generateRandomEmailValue()));
    }

    @Test
    @DisplayName("Testing email's value")
    void testEmailValue() {
        Email email = Email.generateEmail(generateRandomEmailValue());
        Assertions.assertTrue(Email.emailMatcher(email.getValue()));
    }
}
