package com.rayconhealth.tests.models.classes.password;

import com.rayconhealth.rayconhealth.models.classes.Password;
import com.rayconhealth.rayconhealth.models.classes.PasswordHashing;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.UUID;

public class PasswordPositiveTest {
    @Test
    @DisplayName("Testing password generation")
    public void passwordGenerationTest() {
        String number = "123456789";
        Password password = Password.generatePassword(number, false);
        Assertions.assertTrue(new PasswordHashing().authenticate(number, password.getValue()));
    }

    @Test
    @DisplayName("Testing password generation without hashing")
    public void passwordGenerationWithoutHashingTest() {
        String number = "123456789";
        Password password = Password.generatePassword(number, true);
        Assertions.assertEquals(password.getValue(), number);
    }

    @Test
    @DisplayName("Testing password hashing system")
    public void testingPasswordHashingSystem() {
        String someString = UUID.randomUUID().toString().replaceAll("_", "");
        PasswordHashing passwordHashing = new PasswordHashing();
        String hashedString = passwordHashing.hash(someString);
        Assertions.assertTrue(passwordHashing.authenticate(someString, hashedString));
    }
}
