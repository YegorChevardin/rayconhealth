package com.rayconhealth.tests.models;

import com.rayconhealth.rayconhealth.models.Appointment;
import com.rayconhealth.rayconhealth.models.HospitalCard;
import com.rayconhealth.rayconhealth.models.User;
import com.rayconhealth.rayconhealth.models.classes.Email;
import com.rayconhealth.rayconhealth.models.classes.Password;
import com.rayconhealth.tests.models.classes.emails.Util;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.sql.Date;
import java.util.Random;
import java.util.UUID;

public class AppointmentTest {
    private Appointment appointment;
    private User user;
    private Date meetingDate;
    private HospitalCard hospitalCard;

    public AppointmentTest() {
        String phone = String.valueOf((new Random().nextInt(7)+1) * 100);
        String email = Util.generateRandomEmailValue();
        String password = UUID.randomUUID().toString().replaceAll("_", "");
        String secondName = "Chevardin";
        String name = "Yegor";
        user = new User(
                name,
                secondName,
                new java.sql.Date(2004, 05, 03),
                Email.generateEmail(email),
                phone,
                Password.generatePassword(password, false),
                "user",
                "patient"
        );

        hospitalCard = new HospitalCard(user.getName() + "'s hospital card", user);

        meetingDate = new Date(2023, 10, 05);
        appointment = new Appointment(
                meetingDate,
                user,
                user,
                "consultation",
                hospitalCard
        );
    }

    @Test
    @DisplayName("Meeting date test")
    public void meetingTimeTest() {
        Assertions.assertEquals(meetingDate, appointment.getMeetingDate());
    }

    @Test
    @DisplayName("Meeting doctor test")
    public void meetingDoctorTest() {
        Assertions.assertEquals(user, appointment.getDoctor());
    }

    @Test
    @DisplayName("Meeting patient test")
    public void meetingPatientTest() {
        Assertions.assertEquals(user, appointment.getUser());
    }

    @Test
    @DisplayName("Appointment special test")
    public void meetingSpecialTest() {
        Assertions.assertEquals(appointment.getUser(), appointment.getDoctor());
    }

    @Test
    @DisplayName("Meeting hospital card test")
    public void meetingHospitalCardTest() {
        Assertions.assertEquals(hospitalCard.getUser(), appointment.getUser());
    }
}
